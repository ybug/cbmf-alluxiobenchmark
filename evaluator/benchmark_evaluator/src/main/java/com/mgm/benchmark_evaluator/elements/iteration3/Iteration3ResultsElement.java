package com.mgm.benchmark_evaluator.elements.iteration3;

import java.util.HashMap;
import java.util.Map;

public class Iteration3ResultsElement {

	private String name;
	private Integer itemId;
	private String unit;
	//scaleFactor
	private Map<Long,Iteration3ResultElement> results = new HashMap<Long,Iteration3ResultElement>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public Map<Long,Iteration3ResultElement> getResults() {
		return results;
	}
	public void setResults(Map<Long,Iteration3ResultElement> results) {
		this.results = results;
	}
	public void addResult(Long scaleFactor,Iteration3ResultElement result){
		this.results.put(scaleFactor,result);
	}
	@Override
	public String toString() {
		return "Iteration3ResultsElement [name=" + name + ", itemId=" + itemId + ", unit=" + unit + ", results=" + results + "]";
	}
}
