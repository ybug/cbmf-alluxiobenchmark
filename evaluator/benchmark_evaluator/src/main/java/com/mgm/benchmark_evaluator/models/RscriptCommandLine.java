package com.mgm.benchmark_evaluator.models;

public class RscriptCommandLine extends CommandLineExecutor{

	private String path;
	private boolean logJarOutput = true;
	
	private final boolean ERROR = true;
	private final boolean SUCCESFUL= !ERROR;
	
	
	public RscriptCommandLine(String path) {
		this.path = path;
	}
	
	@Override
	public String getCommand() {
		return DefaultValues.R_COMMAND+path;
	}

	@Override
	public String getErrorMessage() {
		return StringTemplates.outputExecutorRscriptTitle(path, ERROR);
	}

	@Override
	public String getInfoMessage() {
		return StringTemplates.outputExecutorRscriptTitle(path, SUCCESFUL);
	}

	@Override
	public boolean isLogExecuteOutput() {
		return logJarOutput;
	}
	
	public void setLogJarOutput(boolean logJarOutput) {
		this.logJarOutput = logJarOutput;
	}
}
