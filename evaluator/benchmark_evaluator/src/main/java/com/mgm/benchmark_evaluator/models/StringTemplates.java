package com.mgm.benchmark_evaluator.models;

import java.util.Date;

public class StringTemplates {
	
	public static final String ARGUMENTS_INPUT_PATH_KEY_LONG = "-input";
	public static final String ARGUMENTS_INPUT_PATH_KEY_SHORT = "-i";
	public static final String ARGUMENTS_OUTPUT_PATH_KEY_LONG = "-output";
	public static final String ARGUMENTS_OUTPUT_PATH_KEY_SHORT = "-o";
	public static final String ARGUMENTS_COLOR_KEY_LONG = "-color";
	public static final String ARGUMENTS_COLOR_KEY_SHORT = "-c";
	
	public static final String R_MIN_KEY = "<min>";
	public static final String R_MAX_KEY = "<max>";
	public static final String R_MEAN_KEY = "<mean>";
	public static final String R_OURTPUTPATH_KEY = "<output_path>";
	public static final String R_SCALEFACTOR_KEY = "<scale_factors_as_string>";
	public static final String R_TITLE_KEY = "<title>";
	public static final String R_XTITLE_KEY = "<x_title>";
	public static final String R_YTITLE_KEY = "<y_title>";
	public static final String R_STANDARDDEVIATION_KEY = "<standard_deviation>";
	
	public static final String R2_OURTPUTPATH_KEY = "<output_path>";
	public static final String R2_WITHSTD_KEY = "<with_std>";
	public static final String R2_GRAPHNAMES_KEY = "<graph_names>";
	public static final String R2_PLOTNUM_KEY = "<plot_num>";
	public static final String R2_YTITLE_KEY = "<y_title>";
	public static final String R2_XTITLE_KEY = "<x_title>";
	public static final String R2_TITLETOP_KEY = "<title_top>";
	public static final String R2_TITLEBOTTOM_KEY = "<title_bottom>";
	public static final String R2_GRAPHVALUES_KEY = "<graph_values>";
	public static final String R2_STDVALUES_KEY = "<std_values>";
	public static final String R2_PLOTS_KEY = "<plots>";
	public static final String R2_YRANGE_KEY = "<y_range>";
	
	public static final String R2_Plot_Command = "plotGraph(%s,%s,xName,yName,%s,%s,%s,%s,ownYRange,containsInStringArray(yName,units))";
	public static final String R2_PlotWithColor_Command = "plotGraph(colors,%s,%s,xName,yName,%s,%s,%s,%s,ownYRange,containsInStringArray(yName,units))";

	
	public static final String[] CSV_HEADLINE_MINMAX = new String[]{"Scale Factor","Minimum$1","Maximum$1","Mean$1","Standard Devision$1"};
	public static final String[] CSV_HEADLINE_MEAN = new String[]{"Scale Factor","Mean$1","Standard Devision$1"};
	
	public static String outputStartEvaluationTitle = 
			"\n++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
			"++++++++ Start Benchmark Evaluation +++++++++++++++++++\n"+
			"+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
			new Date().toString()+"\n"+
			"+++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
	
	public static String outputEndEvaluationTitle = 
			"\n#######################################################\n"+
			"######## Finished Benchmark Evaluation ################\n"+
			"#######################################################\n"+
			new Date().toString()+"\n"+
			"#######################################################\n";
	
	public static String outputStartFirstIteration = "###\n### Start Iteration 1 ###\n###";
	
	public static String outputStartIteration2 = "###\n### Start Iteration 2 ###\n###";
	
	public static String outputStartIteration3 = "###\n### Start Iteration 3 ###\n###";
	
	public static String outputStartIteration4 = "###\n### Start Iteration 4 ###\n###";
	
	public static String outputCantCopyFile(String source, String target){
		return "Can't copy file from "+source+" to "+target+".";
	}
	
	public static String outputCantCopyFolder(String source, String target){
		return "Can't copy folder from "+source+" to "+target+".";
	}
	
	public static String outputCantDeleteFolder(String folderPath){
		return "Can't delete folder-> "+folderPath;
	}
	
	public static String outputInputFile(String key, String value){
		return "Name: "+key+" -> "+value;
	}
	
	public static String outputInputPaths = "#### founded input paths ####";
	
	public static String outputCantReadResultsInJsonFile(){
		return "Can't read results JSON file.";
	}
	
	public static String outputTastCaseError(String scenarioName, String testcaseName){
		return "Error test case -> "+scenarioName+" : "+testcaseName;
	}
	
	public static String outputCantWriteFile(String path){
		return "Error can't write -> "+path;
	}
	
	public static String outputCommandLineExecutorLoggerErrorStream(String message){
		return "command line message [error]-> "+message;
	}
	
	public static String ouptutCommandLineTitleErrorStream(){
		return "  --error stream--";
	}
	
	public static String outputCommandLineExecutorLoggerInputStream(String message){
		return "command line message [input]-> "+message;
	}
	
	public static String ouptutCommandLineTitleInputStream(){
		return "  --input stream--";
	}
	
	public static String outputExecutorRscriptTitle(String path, boolean error){
		String statusString = "successful";
		if(error){
			statusString = "error";
		}
		return "-- Finished Rscript :"+path+"->"+statusString+" --";
	}
	
	public static String getTestCaseTitle(String title){
		if(title.equals(title.toLowerCase())){
			return title.toUpperCase();
		}
		String returnString = "";
		for(int index = 0; index < title.length(); index++){
			if(Character.isUpperCase(title.charAt(index)) && (index > 0) && (index < title.length()-1)){
				if(Character.isLowerCase(title.charAt(index-1))){
					returnString += " ";
				}else if(Character.isLowerCase(title.charAt(index+1))){
					returnString += " ";
				}
			}
			returnString += title.charAt(index);
		}
		
		return returnString;
	}
	
	public static String getAlgorithm(String alg){
		String returnString = alg;
		if(alg.equals("wc")){
			returnString = "Word Counter";
		}else if(alg.equals("cp")){
			returnString = "Copy";
		}else if(alg.equals("tera")){
			returnString = "Terasort";
		}else if(alg.equals("tfidf")){
			returnString = "TF-IDF";
		}else if(alg.equals("grouping")){
			returnString = "Grouping";
		}
		return returnString;
	}
	
	public static String getUpperFirstChar(String lowerString){
		if(lowerString.length() < 1){
			return lowerString;
		}
		return (""+lowerString.charAt(0)).toUpperCase()+lowerString.substring(1);
	}
}
