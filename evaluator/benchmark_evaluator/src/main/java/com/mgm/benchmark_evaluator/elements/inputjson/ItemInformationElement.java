package com.mgm.benchmark_evaluator.elements.inputjson;

/**
 * contains the information from one zabbix item 
 * @author bugge
 * @since 04.06.2016
 */
public class ItemInformationElement {
	
	private String name;
	private Integer id;
	private Boolean enable;
	private Integer dataformat;
	private Integer templateId;
	private Integer delay;
	private String unit;
	private String json;
	
	public ItemInformationElement() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public int getDataformat() {
		return dataformat;
	}

	public void setDataformat(int dataformat) {
		this.dataformat = dataformat;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	@Override
	public String toString() {
		return "ItemInformationElement [name=" + name + ", id=" + id + ", enable=" + enable + ", dataformat=" + dataformat + ", templateId="
				+ templateId + ", delay=" + delay + ", unit=" + unit + ", json=" + json + "]";
	}
}
