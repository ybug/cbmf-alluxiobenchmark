#!/bin/bash

echo "#"
echo "### Spark AlluxioMustCacheRDDOffHeap Wordcount"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapWordcountBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapWordcountBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapWordcountBenchmark.sh _part3
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapWordcountBenchmark.sh _part4
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioMustCacheRDDOffHeap Copy"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapCopyBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapCopyBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapCopyBenchmark.sh _part3
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapCopyBenchmark.sh _part4
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioMustCacheRDDOffHeap Terasort"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTeraBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTeraBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTeraBenchmark.sh _part3
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTeraBenchmark.sh _part4
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioMustCacheRDDOffHeap TF-IDF"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTfidfBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTfidfBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTfidfBenchmark.sh _part3
#sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTfidfBenchmark.sh _part4
#sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheRDDOffHeapTfidfBenchmark.sh _part5
