package com.mgm.terasort.extern.eastcirclek;

import org.apache.hadoop.io.Text;
import org.apache.spark.Partitioner;

/**
 * 
 * @author bugge
 * @since 09.06.2016
 */
public class SparkTeraRangePartitioner extends Partitioner{

	private static final long serialVersionUID = 1L;
	private final int numPartitions;
	private final TotalOrderPartitioner underlying;
	
	public SparkTeraRangePartitioner(TotalOrderPartitioner underlying, int partitions){
		this.numPartitions = partitions;
		this.underlying = underlying;
	}

	@Override
	public int getPartition(Object key) {
		Text textKey = (Text)key;
		return this.underlying.getPartition(textKey); 		
	}

	@Override
	public int numPartitions() {
		return this.numPartitions;
	}
}
