package com.mgm.benchmark_evaluator.models;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultAtomElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;

public class R2OperationsMergeQualityAlgoritm {

	private final String resultFolderPath;
	private final String fileNameNumber;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(R2OperationsMergeQualityAlgoritm.class.getName());
	public static boolean WITH_COLOR = true;

	public R2OperationsMergeQualityAlgoritm(String benchmarkName, String resultFolderPath, String iterationName, String computerName, String fileNameNumber) {
		this.resultFolderPath = resultFolderPath;
		this.fileNameNumber = fileNameNumber;
		this.computerName = computerName;
		this.iterationName = iterationName;
		this.benchmarkName = benchmarkName;
	}

	public void write(Entry<Integer,Map<String,Iteration3ResultsElement>> results) {
		
		try {
			writeR(results);
			writeSVG();
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}

	private void writeR(Entry<Integer,Map<String,Iteration3ResultsElement>> results) throws IOException {
		
		List<String> lines = null;
		if(WITH_COLOR){
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT4COLOR_TEMPLATE_PATH);
		}else{
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT4_TEMPLATE_PATH);
		}
		List<String> outputLines = new ArrayList<String>();
		for (String line : lines) {
			line = line
					.replace(StringTemplates.R2_OURTPUTPATH_KEY, getSvgOutputPath(benchmarkName,results))
					.replace(StringTemplates.R2_GRAPHNAMES_KEY, getGraphsNames(results))
					.replace(StringTemplates.R2_PLOTNUM_KEY, getPlotNumber(results))
					.replace(StringTemplates.R2_YRANGE_KEY, getYRange(results))
					.replace(StringTemplates.R2_YTITLE_KEY, "\""+getYTitle(results)+"\"")
					.replace(StringTemplates.R2_XTITLE_KEY, "\""+getXTitle()+"\"")
					.replace(StringTemplates.R2_PLOTS_KEY, getPlots(results))
					;
			outputLines.add(line);
		}
		FileFolderOperations.createFolderIfNotExist(FileFolderOperations.getFolderPathFromFilePath(getRFilePath(benchmarkName)));
		FileFolderOperations.writeFile(getRFilePath(benchmarkName), outputLines);
	}
	
	private String getGraphsNames(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		String graphNames = "";
		for(String graphName : getGraphsNamesList(results)){
			graphNames += "\""+graphName+"\",";
		}
		if(!graphNames.isEmpty()){
			graphNames = graphNames.substring(0,graphNames.length()-1);
		}
		return graphNames;
	}
	
	private String getXTitle(){
		return DefaultValues.R_SCALEFACTOR_TITLE;
	}
	
	private String getYTitle(Entry<Integer,Map<String,Iteration3ResultsElement>> results) {
		
		if(results.getValue().isEmpty()){
			return " ["+DefaultValues.QUALITY_PLOT_UNIT+"]";
		}
		Iteration3ResultsElement result = new ArrayList<Iteration3ResultsElement>(results.getValue().values()).get(0);
		
		if (result.getName().indexOf("_") < 0) {
			return result.getName() + " ["+DefaultValues.QUALITY_PLOT_UNIT+"]";
		}
		return result.getName().substring(result.getName().indexOf("_"), result.getName().length()) + " ["+DefaultValues.QUALITY_PLOT_UNIT+"]";
	}

	
	private String getPlotNumber(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		return Integer.toString(results.getValue().size());
	}
	
	private List<String> getGraphsNamesList(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		List<String> graphNamesList = new ArrayList<String>();
		for(Entry<String,Iteration3ResultsElement> item : results.getValue().entrySet()){
			List<String> newGraphNames = getGraphNamesList(item.getValue());
			for(String newGraphName : newGraphNames){
				if(!graphNamesList.contains(newGraphName)){
					graphNamesList.add(newGraphName);
				}
			}
		}
		Collections.sort(graphNamesList);
		return graphNamesList;
	}
	
	private String getPlots(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		
		String plots = "";
		for( String itemKey : getGraphSeq(results)){
			Iteration3ResultsElement item = results.getValue().get(itemKey);
			String graphValues = "data.frame("+getGraphValues(item)+")";
			String graphSTD = "data.frame("+getSDValues(item)+")";
			String legendSeq = "c("+getLegendSeq(item,results)+")";
			String withSTD = getWithStd();
			String titleTop = "\""+getTitleTop(itemKey)+"\"";
			String titleBottom = "\""+getTitleBottom()+"\"";
			String gaphTemplate;
			if(WITH_COLOR){
				gaphTemplate = StringTemplates.R2_PlotWithColor_Command;
			}else{
				gaphTemplate = StringTemplates.R2_Plot_Command;
			}
			String plot = String.format(gaphTemplate,
					graphValues,graphSTD,withSTD,legendSeq,titleTop,titleBottom);
			plots += plot+"\n";
		}
		
		return plots;
	}
	
	private List<String> getGraphSeq(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		List<String> itemsSeq = new ArrayList<String>();
		
		for(String key : DefaultValues.ALG_SEQ){
			if(results.getValue().containsKey(key)){
				itemsSeq.add(key);
			}
		}
		for(Entry<String,Iteration3ResultsElement> item : results.getValue().entrySet()){
			boolean contains = false;
			for(String key : DefaultValues.ALG_SEQ){
				if(item.getKey().equals(key)){
					contains = true;
					break;
				}
			}
			if(!contains){
				itemsSeq.add(item.getKey());
			}
		}
		
		return itemsSeq;
	}

	private String getLegendSeq(Iteration3ResultsElement item, Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		List<String> existTechNames = getGraphsNamesList(results);
		List<String> techNames = getGraphNamesList(item);
		String legendSeqString = "";
		
		for(String techName : techNames){
			legendSeqString += Integer.toString(existTechNames.indexOf(techName)+1)+",";
		}
		
		if(!legendSeqString.isEmpty()){
			legendSeqString = legendSeqString.substring(0, legendSeqString.length()-1);
		}
		
		return legendSeqString;
	}
	
	private String getYRange(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		return Double.toString(100);
	}
	
	private String getRFilePath(String benchmarkName) {
		String folderPath = Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
		return Paths.get(folderPath, DefaultValues.R_TEMPFOLDERNAME, fileNameNumber + DefaultValues.QUALITYROUND_FILES + DefaultValues.R_ENDING).toString();
	}

	private List<String> getGraphNamesList(Iteration3ResultsElement results) {
		List<String> technologiesKey = new ArrayList<String>();
		List<String> technologieabbrevs = new ArrayList<String>();
		
		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				if (!technologieabbrevs.contains(technology)) {
					technologieabbrevs.add(technology);
				}
			}
		}
		Collections.sort(technologieabbrevs);
		for(String technologieabbrev : technologieabbrevs){
			technologiesKey.add(getGraphNamesScenarioTitle(technologieabbrev));
		}
		
		return technologiesKey;
	}
	
	private String getGraphNamesScenarioTitle(String technology){
		String title = StringTemplates.getTestCaseTitle(technology);
		String[] titleSegments = title.split(" ");
		String returnTitle = "";
		int counter = 0;
		for(String titleSegment : titleSegments){
			if(counter < 2){
				returnTitle += titleSegment+" ";
				counter++;
			}else{
				returnTitle += titleSegment+"\\n";
				counter = 0;
			}
		}
		if(!returnTitle.isEmpty() && !returnTitle.endsWith("n")){
			returnTitle = returnTitle.substring(0,returnTitle.length()-1);
		}else{
			returnTitle = returnTitle.substring(0,returnTitle.length()-2);
		}
		return returnTitle;
	}
	
	private String getTitleTop(String alg) {
		
		String title = StringTemplates.getUpperFirstChar(benchmarkName)
				+" "+StringTemplates.getAlgorithm(alg)+"\\n";
		if(computerName.equals(DefaultValues.SUMMARY_FOLDERNAME)){
			String chartName = getSummaryChartTitle();
			return title+chartName;
		}
		return title+computerName+" ("+DefaultValues.QUALITY_PLOT_TITLE+")";
	}
	
	private String getSummaryChartTitle() {
		String chartName = "";
		if(fileNameNumber.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.TIME_CHARTTITLE;
		}else if(fileNameNumber.equals(Integer.toString(DefaultValues.PERFORMANCE_DEFAULTINDX))){
			chartName=DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}

	private String getWithStd() {
		return "TRUE";
	}

	private String getTitleBottom() {
		return "";
	}

	private String getSvgOutputPath(String benchmarkName,Entry<Integer,Map<String,Iteration3ResultsElement>> results) {
		String folderPath = Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
		return Paths.get(folderPath, getSvgOutputFileName(results)).toString();
	}
	
	private String getSvgOutputFileName(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		String scenario = "_"+benchmarkName.replace(" ", "_").replace("\\n", "_");
		return fileNameNumber+"_"+getYTitle(results).replaceAll("[^a-zA-Z0-9 ]","").replace(" ","_")+"_"+ DefaultValues.QUALITYROUND_FILES +scenario+ DefaultValues.PDF_ENDING;
	}

	private String getGraphValues(Iteration3ResultsElement results) {

		String returnScalFactorString = "sf=c(";

		Map<String, String> technologiesString = new HashMap<String, String>();
		List<String> technologiesKey = new ArrayList<String>();
		List<Long> scaleFactors = new ArrayList<Long>();

		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				if (!technologiesKey.contains(technology)) {
					technologiesKey.add(technology);
				}
			}
			if (!scaleFactors.contains(result.getKey())) {
				scaleFactors.add(result.getKey());
			}
		}
		Collections.sort(scaleFactors);

		for (String technology : technologiesKey) {
			technologiesString.put(technology, technology + "=c(");
		}

		for (Long scaleFactor : scaleFactors) {
			Iteration3ResultElement result = results.getResults().get(scaleFactor);
			List<String> techsNa = new ArrayList<String>(technologiesKey);
			for (Entry<String, Iteration3ResultAtomElement> value : result.getElements().entrySet()) {
				String valueString = technologiesString.get(value.getKey());
				valueString += value.getValue().getQualityInPercentAsString().replace(".", "").replace(",", ".") + ",";
				technologiesString.put(value.getKey(), valueString);
				techsNa.remove(techsNa.indexOf(value.getKey()));
			}
			for (String techNa : techsNa) {
				technologiesString.put(techNa, technologiesString.get(techNa) + "NA,");
			}
			returnScalFactorString += result.getScaleFactorAsString().replace(".", "").replace(",", ".") + ",";
		}

		if (returnScalFactorString.endsWith(",")) {
			returnScalFactorString = returnScalFactorString.substring(0, returnScalFactorString.length() - 1);
			for (Entry<String, String> technologyString : technologiesString.entrySet()) {
				String value = technologyString.getValue();
				value = value.substring(0, value.length() - 1);
				technologiesString.put(technologyString.getKey(), value);
			}
		}
		returnScalFactorString += ")";

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			String value = technologyString.getValue();
			value += ")";
			technologiesString.put(technologyString.getKey(), value);
		}

		for (String key : new TreeSet<String>(technologiesString.keySet())) {
			returnScalFactorString += ", "+technologiesString.get(key);
		}

		return returnScalFactorString;
	}

	private String getSDValues(Iteration3ResultsElement results) {
		String returnString = "";
		Map<String, String> technologiesString = new HashMap<String, String>();
		List<String> technologiesKey = new ArrayList<String>();
		List<Long> scaleFactors = new ArrayList<Long>();

		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				if (!technologiesKey.contains(technology)) {
					technologiesKey.add(technology);
				}
			}
			if (!scaleFactors.contains(result.getKey())) {
				scaleFactors.add(result.getKey());
			}
		}
		Collections.sort(scaleFactors);

		for (String technology : technologiesKey) {
			technologiesString.put(technology, technology + "Sd=c(");
		}

		for (Long scaleFactor : scaleFactors) {
			Iteration3ResultElement result = results.getResults().get(scaleFactor);
			List<String> techsNa = new ArrayList<String>(technologiesKey);
			for (Entry<String, Iteration3ResultAtomElement> value : result.getElements().entrySet()) {
				String valueString = technologiesString.get(value.getKey());
				valueString += value.getValue().getQualityInPercentSTDAsString().replace(".", "").replace(",", ".") + ",";
				technologiesString.put(value.getKey(), valueString);
				techsNa.remove(techsNa.indexOf(value.getKey()));
			}
			for (String techNa : techsNa) {
				technologiesString.put(techNa, technologiesString.get(techNa) + "NA,");
			}
		}

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			String value = technologyString.getValue();
			value = value.substring(0, value.length() - 1);
			technologiesString.put(technologyString.getKey(), value);
		}

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			String value = technologyString.getValue();
			value += ")";
			technologiesString.put(technologyString.getKey(), value);
		}

		for (String key : new TreeSet<String>(technologiesString.keySet())) {
			returnString += technologiesString.get(key)+",";
		}
		
		if(!returnString.equals("")){
			returnString = returnString.substring(0,returnString.length()-1);
		}
		
		return returnString;
	}

	private void writeSVG() {
		RscriptCommandLine rsciptCommand = new RscriptCommandLine(getRFilePath(benchmarkName));
		rsciptCommand.run();
	}
}
