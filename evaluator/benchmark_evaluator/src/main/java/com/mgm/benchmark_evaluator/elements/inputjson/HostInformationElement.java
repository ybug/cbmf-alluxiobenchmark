package com.mgm.benchmark_evaluator.elements.inputjson;

import java.util.ArrayList;
import java.util.List;

/**
 * contains information about the host
 * @author bugge
 * @since 04.02.2016
 */
public class HostInformationElement {
	
	private String name;
	private int id;
	private String host;
	private String ip;
	private int port;
	private boolean enable = false;
	private String jsonHostGet;
	private String jsonHostinterfaceGet;
	private List<Integer> itemsID = new ArrayList<Integer>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public List<Integer> getItemsID() {
		return itemsID;
	}
	public void setItemsID(List<Integer> items) {
		this.itemsID = items;
	}
	public void addItem(Integer item){
		this.itemsID.add(item);
	}
	public String getJsonHostGet() {
		return jsonHostGet;
	}
	public void setJsonHostGet(String jsonHostGet) {
		this.jsonHostGet = jsonHostGet;
	}
	public String getJsonHostinterfaceGet() {
		return jsonHostinterfaceGet;
	}
	public void setJsonHostinterfaceGet(String jsonHostinterfaceGet) {
		this.jsonHostinterfaceGet = jsonHostinterfaceGet;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	@Override
	public String toString() {
		return "HostInformationElement [name=" + name + ", id=" + id + ", host=" + host + ", ip=" + ip + ", port="
				+ port + ", enable=" + enable + ", jsonHostGet=" + jsonHostGet + ", jsonHostinterfaceGet="
				+ jsonHostinterfaceGet + ", itemsID=" + itemsID + "]";
	}
}
