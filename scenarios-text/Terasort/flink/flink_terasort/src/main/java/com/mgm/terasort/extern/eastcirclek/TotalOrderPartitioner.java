/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mgm.terasort.extern.eastcirclek;

//import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream; // eastcirclek
import java.io.ObjectOutputStream; // eastcirclek
//import java.io.PrintStream;
import java.io.Serializable;

import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;

import org.apache.hadoop.mapreduce.MRJobConfig;

import com.mgm.terasort.extern.eastcirclek.total_order_partitioner.TrieBuilder;
import com.mgm.terasort.extern.eastcirclek.total_order_partitioner.TrieNode;

/**
 * eastcirclek : 
 * I extract this class from org.apache.hadoop.examples.terasort.TeraSort 
 * and make little modification to use it for TeraSort on Spark and Flink.
 * - We no longer implement org.apache.hadoop.mapreduce.Partitioner
 *   as we are going to use this class for custom partitioners in 
 *   Spark (org.apache.spark.Partitioner) and 
 *   Flink (org.apache.flink.api.common.functions.Partitioner).
 * - We implement java.io.Serializable because Spark and Flink pass
 *   partitioner objects to tasks by serializing/deserializing the objects.
 * - We remove setConf() from the origial TotalOrderPartitioner
 *   as it assumes that TeraInputFormat.PARTITION_FILENAME is 
 *	 localized to PWD on which a MapReduce task is running.
 **/

/**
 * A partitioner that splits text keys into roughly equal partitions
 * in a global sorted order.
 */
public class TotalOrderPartitioner implements Serializable{

    private Configuration conf;
    private TrieNode trie;
    //private TrieBuilder builder;

    private Path partFile; // eastcirclek

    // eastcirclek
    public TotalOrderPartitioner(Configuration conf,
                                 Path partFile) throws IOException{
        this.conf = conf;
        this.partFile = partFile;
        TrieBuilder builder1 = new TrieBuilder();
        this.trie = builder1.buildTrieFromHDFS(conf, partFile);
    }

    // eastcirclek
    public int getPartition(Text key){
        return trie.findPartition(key);
    }

    // eastcirclek for serialization
    private void writeObject(ObjectOutputStream out) throws IOException{
        out.writeUTF(conf.get("fs.defaultFS"));
        out.writeInt(conf.getInt(MRJobConfig.NUM_REDUCES, 2));
        /**
         * Instead of serializing the trie,
         *  we serialize the filename containing sampling points
         *  so that we can rebuild the trie in each task.
         */
        out.writeUTF(this.partFile.toString());
    }

    // eastcirclek for deserialization
    private void readObject(ObjectInputStream in) throws IOException{
        this.conf = new Configuration();
        conf.set("fs.defaultFS", (String)in.readUTF());
        conf.setInt(MRJobConfig.NUM_REDUCES, (int)in.readInt());
        this.partFile = new Path((String)in.readUTF());
        TrieBuilder builder1 = new TrieBuilder();
        this.trie = builder1.buildTrieFromHDFS(conf, partFile);
    }
}
