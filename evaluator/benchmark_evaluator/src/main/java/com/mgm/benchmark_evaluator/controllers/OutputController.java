package com.mgm.benchmark_evaluator.controllers;

import java.util.Map;
import java.util.Map.Entry;

import com.mgm.benchmark_evaluator.elements.Arguments;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;
import com.mgm.benchmark_evaluator.models.CsvOperationsMean;
import com.mgm.benchmark_evaluator.models.CsvOperationsMinMax;
import com.mgm.benchmark_evaluator.models.CsvOperationsRounds;
import com.mgm.benchmark_evaluator.models.CsvRatioIT3;
import com.mgm.benchmark_evaluator.models.CsvStandardDevicionIT3;
import com.mgm.benchmark_evaluator.models.R2OperationsMean;
import com.mgm.benchmark_evaluator.models.R2OperationsMergeAlgoritm;
import com.mgm.benchmark_evaluator.models.R2OperationsMergeQualityAlgoritm;
import com.mgm.benchmark_evaluator.models.R2OperationsMinMax;
import com.mgm.benchmark_evaluator.models.R2OperationsQualityRounds;
import com.mgm.benchmark_evaluator.models.R2OperationsRounds;
import com.mgm.benchmark_evaluator.models.R2OperationsScenarios;

public class OutputController {

	private final String computerName;
	private final String fileName;
	private final String iterationName;
	private final String benchmarkName;

	public OutputController(String benchmarkName, String iterationName, String computerName, String fileName) {
		this.computerName = computerName;
		this.fileName = fileName;
		this.iterationName = iterationName;
		this.benchmarkName = benchmarkName;
	}

	public void printResults(Iteration1ResultsElement results) {
		CsvOperationsMinMax csvOperations = new CsvOperationsMinMax(benchmarkName,Arguments.getOutputPath(),iterationName, computerName, fileName);
		csvOperations.writeCSV(results);
		
		CsvOperationsMean csvOperations2 = new CsvOperationsMean(benchmarkName,Arguments.getOutputPath(),iterationName, computerName, fileName);
		csvOperations2.writeCSV(results);
		
		CsvOperationsRounds csvOperations3 = new CsvOperationsRounds(benchmarkName,Arguments.getOutputPath(),iterationName, computerName, fileName);
		csvOperations3.writeCSV(results);
		
		R2OperationsMean rOperatins = new R2OperationsMean(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperatins.write(results);
		
		R2OperationsRounds rOperatins2 = new R2OperationsRounds(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperatins2.write(results);
		
		R2OperationsMinMax rOperatins3 = new R2OperationsMinMax(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperatins3.write(results);
		
		R2OperationsQualityRounds rOperatins4 = new R2OperationsQualityRounds(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperatins4.write(results);
		
		
	}
	
	public void printResultsIt3(Iteration3ResultsElement results){
		R2OperationsScenarios rOperatins4 = new R2OperationsScenarios(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperatins4.write(results);
		
		CsvRatioIT3 ratioCSV = new CsvRatioIT3(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		ratioCSV.writeCSV(results);
		
		CsvStandardDevicionIT3 ratioSD = new CsvStandardDevicionIT3(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		ratioSD.writeCSV(results);
	}
	
	public void printResultsIt4(Entry<Integer,Map<String,Iteration3ResultsElement>> results){
		R2OperationsMergeAlgoritm rOperation1 = new R2OperationsMergeAlgoritm(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperation1.write(results);
		R2OperationsMergeQualityAlgoritm rOperation2 = new R2OperationsMergeQualityAlgoritm(benchmarkName, Arguments.getOutputPath(),iterationName, computerName, fileName);
		rOperation2.write(results);
	}
}
