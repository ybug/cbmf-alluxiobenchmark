package com.mgm.spark_terasort;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import org.apache.hadoop.io.Text;

import com.google.common.primitives.Ints;

/**
 * compress the article id (String -> Integer -> 4Byte), copyID (String -> Integer -> 4Byte) and line hash
 * (Integer -> 4Byte) to 12 byte Text
 * (All Integer will converted to unsigned Integer)
 * 
 * @author bugge
 * @since 22.05.2016
 */
public class KeyCompressor {

	private final String START_ARTICLE_TAG = "<";
	private final String END_ARTICLE_TAG = ">";
	private final String ARTICLE_KEY_SPLIT_TAG = "_";
	private final String EMPTY_STRING = "";
	private final int ARTICLEID_POSITION = 0;
	private final int COPYID_POSITION = 1;

	private final String articleLineKey;
	private final String articleLineText;

	public KeyCompressor(Text articleLine) {
		this.articleLineKey = extractArticleKeyFromArticleLine(articleLine.toString());
		this.articleLineText = extractArticleTextFromArticleLine(articleLine.toString());
	}

	public Text compress() {
		byte[] articleID = Ints.toByteArray(convertToUnsignedInt(getArticleID(articleLineKey)));
		byte[] copyID = Ints.toByteArray(convertToUnsignedInt(getCopyID(articleLineKey)));
		byte[] lineHashCode = Ints.toByteArray(convertToUnsignedInt(getTextHashCode(articleLineText)));
		return new Text(mergeKeyParts(articleID, copyID,lineHashCode));
	}

	private String extractArticleKeyFromArticleLine(String articleLine) {
		String[] lineElements = articleLine.split(START_ARTICLE_TAG);
		if (lineElements.length >= 2) {
			return lineElements[1].replace(END_ARTICLE_TAG, EMPTY_STRING);
		} else {
			return lineElements[0].replace(END_ARTICLE_TAG, EMPTY_STRING);
		}
	}

	private String extractArticleTextFromArticleLine(String articleLine) {
		String[] lineElements = articleLine.split(START_ARTICLE_TAG);
		if (lineElements.length >= 2) {
			return lineElements[0];
		} else {
			return EMPTY_STRING;
		}
	}

	private int getArticleID(String articleKey) {
		return Integer.parseInt(articleKey.split(ARTICLE_KEY_SPLIT_TAG)[ARTICLEID_POSITION]);
	}

	private int getCopyID(String articleKey) {
		return Integer.parseInt(articleKey.split(ARTICLE_KEY_SPLIT_TAG)[COPYID_POSITION]);
	}

	private int getTextHashCode(String articleText) {
		return articleText.hashCode();
	}

	private byte[] mergeKeyParts(byte[] firstPart, byte[] secondPart, byte[] thirdPart) {
		byte[] key = new byte[firstPart.length + secondPart.length + thirdPart.length];
		System.arraycopy(firstPart, 0, key, 0, firstPart.length);
		System.arraycopy(secondPart, 0, key, firstPart.length, secondPart.length);
		System.arraycopy(thirdPart, 0, key, firstPart.length + secondPart.length, thirdPart.length);
		return key;
	}

	private int convertToUnsignedInt(int signedInt) {
		return signedInt + Integer.MAX_VALUE + 1;
	}

	public static String bytesToStringUTFNIO(byte[] bytes) {
		CharBuffer cBuffer = ByteBuffer.wrap(bytes).asCharBuffer();
		return cBuffer.toString();
	}
	
	public static byte[] stringToBytesUTFNIO(String str) {
		 char[] buffer = str.toCharArray();
		 byte[] b = new byte[buffer.length << 1];
		 CharBuffer cBuffer = ByteBuffer.wrap(b).asCharBuffer();
		 for(int i = 0; i < buffer.length; i++)
		  cBuffer.put(buffer[i]);
		 return b;
		}
	
	public static boolean sameDocumentType(Text lastKey, Text currentKey){
		if(lastKey == null || currentKey == null){
			return false;
		}
		if(lastKey.getLength() > 4 && currentKey.getLength() > 4){
			for(int index = 0; index < 4; index++){
				if(lastKey.getBytes()[index] != currentKey.getBytes()[index]){
					return false;
				}
			}	
			return true;
		}
		return false;
	}

}
