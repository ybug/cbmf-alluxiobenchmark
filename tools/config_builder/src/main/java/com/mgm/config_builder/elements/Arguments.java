package com.mgm.config_builder.elements;

import com.github.jankroken.commandline.annotations.LongSwitch;
import com.github.jankroken.commandline.annotations.Option;
import com.github.jankroken.commandline.annotations.ShortSwitch;
import com.github.jankroken.commandline.annotations.SingleArgument;
import com.mgm.config_builder.model.DefaultValues;
import com.mgm.config_builder.model.StringTemplates;

public class Arguments {
	
	private String targetFolderPath = DefaultValues.TARGET_CONFIG_FOLDER_PATH;
	private String templateFolderPath = DefaultValues.TEMPLATE_SOURCE_CONFIG_FOLDER_PATH;
	
	@Option
	@LongSwitch(StringTemplates.OUTPUT_FOLDER_ARGUMENT_KEY_LONG)
	@ShortSwitch(StringTemplates.OUTPUT_FOLDER_ARGUMENT_KEY_SHORT)
	@SingleArgument
	public void setTargetFolderPath(String targetFolderPath) {
		this.targetFolderPath = targetFolderPath;
	}

	public String getTemplateFolderPath() {
		return templateFolderPath;
	}

	@Option
	@LongSwitch(StringTemplates.TEMPLATE_FOLDER_ARGUMENT_KEY_LONG)
	@ShortSwitch(StringTemplates.TEMPLATE_FOLDER_ARGUMENT_KEY_SHORT)
	@SingleArgument
	public void setTemplateFolderPath(String templateFolderPath) {
		this.templateFolderPath = templateFolderPath;
	}
	
	public String getTargetFolderPath() {
		return targetFolderPath;
	}
	
}
