package com.mgm.unsorted_evaluation;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.core.fs.FileSystem.WriteMode;
import org.apache.flink.util.Collector;

/**
 * evaluate the reference date with result date
 * (Word Counter)
 * @author bugge
 * @since 13.06.2016
 */
public class Main {
	
	public final static boolean EQUAL = true;
	public final static boolean NOT_EQUAL = false;
	public final static String EQUAL_TEXT = "Equal:";
	public final static String NOT_EQUAL_TEXT = "Not Equal:";
	
	public static void main(String[] args) throws Exception {
		
		final ParameterTool params = ParameterTool.fromArgs(args);
		String referenceInputPath = params.get("inputRef", null);
		String checkInputPath = params.get("inputCheck", null);
		String outputPath = params.get("output", null);

		if (referenceInputPath == null || checkInputPath == null || outputPath == null) {
			System.err.println("input or output not set in the arguments!");
			return;
		}
		// set up the execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> referenceDataSet = env.readTextFile(referenceInputPath);
		DataSet<String> checkDataSet = env.readTextFile(checkInputPath);
		
		//compute compress data set. each string is unique. Tuple2<unique String, number of occurrences>
		DataSet<Tuple2<String, Long>> compressReferenceDataSet = referenceDataSet.map(new CompressDataSet()).groupBy(0).sum(1);

		//compute compress data set. each string is unique. Tuple2<unique String, number of occurrences>
		DataSet<Tuple2<String, Long>> compressCheckDataSet = checkDataSet.map(new CompressDataSet()).groupBy(0).sum(1);
		
		//compare both data sets. merge unique Strings and compare number of occurrences
		compressReferenceDataSet.fullOuterJoin(compressCheckDataSet).where(0).equalTo(0)
				.with(new JoinFunction<Tuple2<String, Long>, Tuple2<String, Long>, Boolean>() {

					private static final long serialVersionUID = 1L;

					// compare the number of elements. return true if equals
					@Override
					public Boolean join(Tuple2<String, Long> first, Tuple2<String, Long> second) throws Exception {
						
						if(second == null || first == null){
							return NOT_EQUAL;
						}
						else if (!first.f1.equals(second.f1)) {
							return NOT_EQUAL;
						}
						return EQUAL;
					}

				}).flatMap(new FlatMapFunction<Boolean, Tuple2<Boolean, Long>>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void flatMap(Boolean value, Collector<Tuple2<Boolean, Long>> out) throws Exception {
						out.collect(new Tuple2<Boolean, Long>(value, 1L));
					}

				}).groupBy(0).sum(1).map(new MapFunction<Tuple2<Boolean,Long>,String>(){

					private static final long serialVersionUID = 1L;

					@Override
					public String map(Tuple2<Boolean, Long> value) throws Exception {
						if(value.f0 == EQUAL){
							return EQUAL_TEXT+Long.toString(value.f1);
						}else {
							return NOT_EQUAL_TEXT+Long.toString(value.f1);
						}
					}
					
				}).writeAsText(outputPath, WriteMode.OVERWRITE);

		// execute program
		env.execute("Flink Unsorted Evaluate");
	}
	
	public static class CompressDataSet implements MapFunction<String, Tuple2<String, Long>> {

		private static final long serialVersionUID = 1L;

		@Override
		public Tuple2<String, Long> map(String value) throws Exception {
			return new Tuple2<String, Long>(value, 1L);
		}
	}
}
