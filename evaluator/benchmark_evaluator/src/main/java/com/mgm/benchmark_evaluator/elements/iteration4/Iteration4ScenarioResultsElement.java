package com.mgm.benchmark_evaluator.elements.iteration4;

import java.util.HashMap;
import java.util.Map;

public class Iteration4ScenarioResultsElement {
	
	private String name;
	
	private final Map<String,Iteration4ComputerResultsElement> computer = new HashMap<String,Iteration4ComputerResultsElement>();
	
	public Iteration4ScenarioResultsElement(String name) {
		this.name = name;
	}
	public Map<String, Iteration4ComputerResultsElement> getComputer() {
		return computer;
	}

	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Iteration4ScenarioResultsElement [name=" + name + ", computer=" + computer + "]";
	}
	
}
