#!/bin/bash

echo "#"
echo "### Flink HDFS Wordcount"
echo "#"

sh Flink/HDFS/startFlinkHDFSWordcountBenchmark.sh _part1
sh Flink/HDFS/startFlinkHDFSWordcountBenchmark.sh _part2
sh Flink/HDFS/startFlinkHDFSWordcountBenchmark.sh _part3
sh Flink/HDFS/startFlinkHDFSWordcountBenchmark.sh _part4
sh Flink/HDFS/startFlinkHDFSWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink HDFS Copy"
echo "#"

sh Flink/HDFS/startFlinkHDFSCopyBenchmark.sh _part1
sh Flink/HDFS/startFlinkHDFSCopyBenchmark.sh _part2
sh Flink/HDFS/startFlinkHDFSCopyBenchmark.sh _part3
sh Flink/HDFS/startFlinkHDFSCopyBenchmark.sh _part4
sh Flink/HDFS/startFlinkHDFSCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink HDFS Terasort"
echo "#"

sh Flink/HDFS/startFlinkHDFSTeraBenchmark.sh _part1
sh Flink/HDFS/startFlinkHDFSTeraBenchmark.sh _part2
sh Flink/HDFS/startFlinkHDFSTeraBenchmark.sh _part3
sh Flink/HDFS/startFlinkHDFSTeraBenchmark.sh _part4
sh Flink/HDFS/startFlinkHDFSTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink HDFS TF-IDF"
echo "#"

sh Flink/HDFS/startFlinkHDFSTfidfBenchmark.sh _part1
sh Flink/HDFS/startFlinkHDFSTfidfBenchmark.sh _part2
sh Flink/HDFS/startFlinkHDFSTfidfBenchmark.sh _part3
#sh Flink/HDFS/startFlinkHDFSTfidfBenchmark.sh _part4
#sh Flink/HDFS/startFlinkHDFSTfidfBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink HDFS Grouping"
echo "#"

sh Flink/HDFS/startFlinkHDFSGroupingBenchmark.sh _part1
sh Flink/HDFS/startFlinkHDFSGroupingBenchmark.sh _part2
sh Flink/HDFS/startFlinkHDFSGroupingBenchmark.sh _part3
sh Flink/HDFS/startFlinkHDFSGroupingBenchmark.sh _part4
sh Flink/HDFS/startFlinkHDFSGroupingBenchmark.sh _part5
