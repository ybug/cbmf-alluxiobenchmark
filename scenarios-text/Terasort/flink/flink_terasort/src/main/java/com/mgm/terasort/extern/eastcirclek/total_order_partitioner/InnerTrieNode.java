package com.mgm.terasort.extern.eastcirclek.total_order_partitioner;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.hadoop.io.Text;

/**
 * An inner trie node that contains 256 children based on the next
 * character.
 */
public class InnerTrieNode extends TrieNode {
    private TrieNode[] child = new TrieNode[256];
  
    InnerTrieNode(int level) {
        super(level);
    }
    public int findPartition(Text key) {
        int level = getLevel();
        if (key.getLength() <= level) {
            return child[0].findPartition(key);
        }
        return child[key.getBytes()[level] & 0xff].findPartition(key);//select with byte by level the correct child
    }
    public void setChild(int idx, TrieNode child) {
        this.child[idx] = child;
    }
    public void print(PrintStream strm) throws IOException {
        for(int ch=0; ch < 256; ++ch) {
            for(int i = 0; i < 2*getLevel(); ++i) {
                strm.print(' ');
            }
            strm.print(ch);
            strm.println(" ->");
            if (child[ch] != null) {
                child[ch].print(strm);
            }
        }
    }
    public TrieNode[] getChildren(){
    	return this.child;
    }
}