#!/bin/bash

echo "#"
echo "### Flink Alluxio Must Cache Wordcount"
echo "#"

sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheWordcountBenchmark.sh _part1
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheWordcountBenchmark.sh _part2
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheWordcountBenchmark.sh _part3
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheWordcountBenchmark.sh _part4
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Must Cache Copy"
echo "#"

sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheCopyBenchmark.sh _part1
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheCopyBenchmark.sh _part2
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheCopyBenchmark.sh _part3
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheCopyBenchmark.sh _part4
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Must Cache Terasort"
echo "#"

sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTeraBenchmark.sh _part1
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTeraBenchmark.sh _part2
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTeraBenchmark.sh _part3
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTeraBenchmark.sh _part4
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Must Cache TF-IDF"
echo "#"

sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTfidfBenchmark.sh _part1
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTfidfBenchmark.sh _part2
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTfidfBenchmark.sh _part3
#sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTfidfBenchmark.sh _part4
#sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheTfidfBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Must Cache Grouping"
echo "#"
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheGroupingBenchmark.sh _part1
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheGroupingBenchmark.sh _part2
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheGroupingBenchmark.sh _part3
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheGroupingBenchmark.sh _part4
sh Flink/Alluxio/MustCache/startFlinkAlluxioMustCacheGroupingBenchmark.sh _part5