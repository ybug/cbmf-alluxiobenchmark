package com.mgm.spark_terasort.extern.eastcirclek.tera_input_format;

import java.util.ArrayList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.util.IndexedSortable;
import org.apache.hadoop.util.QuickSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TextSampler implements IndexedSortable {
	public static Logger LOG = LoggerFactory.getLogger(TextSampler.class);
	
	private ArrayList<Text> records = new ArrayList<Text>();

	public int compare(int i, int j) {
		Text left = records.get(i);
		Text right = records.get(j);
		return left.compareTo(right);
	}

	public void swap(int i, int j) {
		Text left = records.get(i);
		Text right = records.get(j);
		records.set(j, left);
		records.set(i, right);
	}

	public void addKey(Text key) {
		synchronized (this) {
			records.add(new Text(key));
		}
	}

	/**
	 * Find the split points for a given sample. The sample keys are sorted and
	 * down sampled to find even split points for the partitions. The returned
	 * keys should be the start of their respective partitions.
	 * 
	 * @param numPartitions
	 *            the desired number of partitions
	 * @return an array of size numPartitions - 1 that holds the split points
	 */
	public Text[] createPartitions(int numPartitions) {
		
		int numRecords = records.size();
		
		if (numPartitions > numRecords) {
			throw new IllegalArgumentException(
					"Requested more partitions than input keys (" + numPartitions + " > " + numRecords + ")");
		}
		new QuickSort().sort(this, 0, numRecords); 
		float stepSize = numRecords / (float) numPartitions;
		Text[] result = new Text[numPartitions - 1];
		
		for (int i = 1; i < numPartitions; ++i) {
			result[i - 1] = records.get(Math.round(stepSize * i));
		}
		
		return result;
	}
}
