package com.mgm.benchmark_evaluator.elements.iteration4;

import java.util.HashMap;
import java.util.Map;

public class Iteration4BenchmarkResultsElement {
	
	private Map<String,Iteration4ScenarioResultsElement> benchmarks = new HashMap<String,Iteration4ScenarioResultsElement>();

	public Map<String, Iteration4ScenarioResultsElement> getBenchmarks() {
		return benchmarks;
	}

	public void setBenchmarks(Map<String, Iteration4ScenarioResultsElement> benchmarks) {
		this.benchmarks = benchmarks;
	}
	
	
}
