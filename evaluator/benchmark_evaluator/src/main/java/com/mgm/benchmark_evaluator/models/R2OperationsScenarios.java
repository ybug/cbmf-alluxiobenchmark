package com.mgm.benchmark_evaluator.models;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultAtomElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;

public class R2OperationsScenarios {

	private final String resultFolderPath;
	private final String fileNameNumber;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(R2OperationsScenarios.class.getName());
	public static boolean WITH_COLOR = true;

	public R2OperationsScenarios(String benchmarkName, String resultFolderPath, String iterationName, String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileNameNumber = fileName.split("_")[0];
		this.fileName = fileName.split("_")[1].replaceAll("[^a-zA-Z0-9 ]","").replace(" ","_");
		this.computerName = computerName;
		this.iterationName = iterationName;
		this.benchmarkName = benchmarkName;
	}

	public void write(Iteration3ResultsElement results) {

		try {
			writeR(results);
			writeSVG();
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}

	private void writeR(Iteration3ResultsElement results) throws IOException {
		List<String> lines;
		if(WITH_COLOR){
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT3COLOR_TEMPLATE_PATH);
		}else{
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT3_TEMPLATE_PATH);
		}
		
		List<String> outputLines = new ArrayList<String>();
		for (String line : lines) {
			line = line.replace(StringTemplates.R2_OURTPUTPATH_KEY, getSvgOutputPath(benchmarkName))
					.replace(StringTemplates.R2_TITLEBOTTOM_KEY, getTitleBottom()).replace(StringTemplates.R2_YTITLE_KEY, getYTitle(results))
					.replace(StringTemplates.R2_WITHSTD_KEY, getWithStd()).replace(StringTemplates.R2_TITLETOP_KEY, getTitleTop())
					.replace(StringTemplates.R2_GRAPHNAMES_KEY, getGraphNames(results))
					.replace(StringTemplates.R2_GRAPHVALUES_KEY, getGraphValues(results))
					.replace(StringTemplates.R2_STDVALUES_KEY, getSDValues(results));
			outputLines.add(line);
		}
		FileFolderOperations.createFolderIfNotExist(FileFolderOperations.getFolderPathFromFilePath(getRFilePath(benchmarkName)));
		FileFolderOperations.writeFile(getRFilePath(benchmarkName), outputLines);
	}

	private String getRFilePath(String benchmarkName) {
		String folderPath = Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
		return Paths.get(folderPath, DefaultValues.R_TEMPFOLDERNAME, fileNameNumber + DefaultValues.MERGE_FILES + DefaultValues.R_ENDING).toString();
	}

	private String getGraphNames(Iteration3ResultsElement results) {
		
		/*String retunValue = "\"" + DefaultValues.R_SCALEFACTOR_TITLE + "\",";
		List<String> technologiesKey = new ArrayList<String>();
		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				
				if (!technologiesKey.contains(getGraphNamesScenarioTitle(technology))) {
					technologiesKey.add(getGraphNamesScenarioTitle(technology));
				}
			}
		}
		for (String technology : technologiesKey) {

			retunValue += "\"" + technology + "\",";
		}

		if (retunValue.endsWith(",")) {
			retunValue = retunValue.substring(0, retunValue.length() - 1);
		}

		return retunValue;*/
		
		String returnValue = "\"" + DefaultValues.R_SCALEFACTOR_TITLE + "\",";
		Map<String,String> technologiesMap = new HashMap<String,String>();
		List<String> technologiesKey = new ArrayList<String>();
		
		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				if (!technologiesKey.contains(technology)) {
					technologiesKey.add(technology);
				}
			}
		}
		
		for (String technology : technologiesKey) {

			technologiesMap.put(technology,getGraphNamesScenarioTitle(technology));
		}

		for(Entry<String,String> technology: technologiesMap.entrySet()){
			returnValue += "\"" + technology.getValue() + "\",";
		}
		if (returnValue.endsWith(",")) {
			returnValue = returnValue.substring(0, returnValue.length() - 1);
		}
		
		return returnValue;
	}
	
	private String getGraphNamesScenarioTitle(String technology){
		String title = StringTemplates.getTestCaseTitle(technology);
		String[] titleSegments = title.split(" ");
		String returnTitle = "";
		int counter = 0;
		for(String titleSegment : titleSegments){
			if(counter == 0){
				returnTitle += titleSegment+" ";
				counter++;
			}else{
				returnTitle += titleSegment+"\n";
				counter = 0;
			}
		}
		if(!returnTitle.isEmpty()){
			returnTitle = returnTitle.substring(0,returnTitle.length()-1);
		}
		return returnTitle;
	}
	
	private String getTitleTop() {
		
		String[] benchmarkNameSplits = benchmarkName.split("_");
		String title = StringTemplates.getUpperFirstChar(benchmarkNameSplits[0])
				+" "+StringTemplates.getAlgorithm(benchmarkNameSplits[1])+"\\n";
		if(computerName.equals(DefaultValues.COMPUTER_SUMMARYNAME)){
			String chartName = getSummaryChartTitle();
			return title+chartName;
		}
		return title+computerName;
	}
	
	private String getSummaryChartTitle() {
		String chartName = "";
		if(fileNameNumber.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.TIME_CHARTTITLE;
		}else if(fileNameNumber.equals(Integer.toString(DefaultValues.PERFORMANCE_DEFAULTINDX))){
			chartName=DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}

	private String getWithStd() {
		return "TRUE";
	}

	private String getTitleBottom() {
		return "";
	}

	private String getYTitle(Iteration3ResultsElement results) {
		String unit = "";
		if (!results.getUnit().equals("")) {
			unit = " [" + results.getUnit() + "]";
		}
		if (results.getName().indexOf("_") < 0) {
			return results.getName() + unit;
		}
		return results.getName().substring(results.getName().indexOf("_"), results.getName().length()) + unit;
	}

	private String getSvgOutputPath(String benchmarkName) {
		String folderPath = Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
		return Paths.get(folderPath, getSvgOutputFileName()).toString();
	}
	
	private String getSvgOutputFileName(){
		String scenario = "_"+getTitleTop().replace(" ", "_").replace("\\n", "_");
		return fileNameNumber+"_"+fileName+"_" + DefaultValues.MERGE_FILES +scenario+ DefaultValues.PDF_ENDING;
	}

	private String getGraphValues(Iteration3ResultsElement results) {

		String returnScalFactorString = "sf=c(";

		Map<String, String> technologiesString = new HashMap<String, String>();
		List<String> technologiesKey = new ArrayList<String>();
		List<Long> scaleFactors = new ArrayList<Long>();

		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				if (!technologiesKey.contains(technology)) {
					technologiesKey.add(technology);
				}
			}
			if (!scaleFactors.contains(result.getKey())) {
				scaleFactors.add(result.getKey());
			}
		}
		Collections.sort(scaleFactors);

		for (String technology : technologiesKey) {
			technologiesString.put(technology, technology + "=c(");
		}

		for (Long scaleFactor : scaleFactors) {
			Iteration3ResultElement result = results.getResults().get(scaleFactor);
			List<String> techsNa = new ArrayList<String>(technologiesKey);
			for (Entry<String, Iteration3ResultAtomElement> value : result.getElements().entrySet()) {
				String valueString = technologiesString.get(value.getKey());
				valueString += value.getValue().getMeanAsString().replace(".", "").replace(",", ".") + ",";
				technologiesString.put(value.getKey(), valueString);
				techsNa.remove(techsNa.indexOf(value.getKey()));
			}
			for (String techNa : techsNa) {
				technologiesString.put(techNa, technologiesString.get(techNa) + "NA,");
			}
			returnScalFactorString += result.getScaleFactorAsString().replace(".", "").replace(",", ".") + ",";
		}

		if (returnScalFactorString.endsWith(",")) {
			returnScalFactorString = returnScalFactorString.substring(0, returnScalFactorString.length() - 1);
			for (Entry<String, String> technologyString : technologiesString.entrySet()) {
				String value = technologyString.getValue();
				value = value.substring(0, value.length() - 1);
				technologiesString.put(technologyString.getKey(), value);
			}
		}
		returnScalFactorString += ")";

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			String value = technologyString.getValue();
			value += ")";
			technologiesString.put(technologyString.getKey(), value);
		}

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			returnScalFactorString += ", " + technologyString.getValue();
		}

		return returnScalFactorString;
	}

	private String getSDValues(Iteration3ResultsElement results) {
		String returnString = "";
		Map<String, String> technologiesString = new HashMap<String, String>();
		List<String> technologiesKey = new ArrayList<String>();
		List<Long> scaleFactors = new ArrayList<Long>();

		for (Entry<Long, Iteration3ResultElement> result : results.getResults().entrySet()) {

			for (String technology : result.getValue().getElements().keySet()) {
				if (!technologiesKey.contains(technology)) {
					technologiesKey.add(technology);
				}
			}
			if (!scaleFactors.contains(result.getKey())) {
				scaleFactors.add(result.getKey());
			}
		}
		Collections.sort(scaleFactors);

		for (String technology : technologiesKey) {
			technologiesString.put(technology, technology + "Sd=c(");
		}

		for (Long scaleFactor : scaleFactors) {
			Iteration3ResultElement result = results.getResults().get(scaleFactor);
			List<String> techsNa = new ArrayList<String>(technologiesKey);
			for (Entry<String, Iteration3ResultAtomElement> value : result.getElements().entrySet()) {
				String valueString = technologiesString.get(value.getKey());
				valueString += value.getValue().getStandardDeviationAsString().replace(".", "").replace(",", ".") + ",";
				technologiesString.put(value.getKey(), valueString);
				techsNa.remove(techsNa.indexOf(value.getKey()));
			}
			for (String techNa : techsNa) {
				technologiesString.put(techNa, technologiesString.get(techNa) + "NA,");
			}
		}

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			String value = technologyString.getValue();
			value = value.substring(0, value.length() - 1);
			technologiesString.put(technologyString.getKey(), value);
		}

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			String value = technologyString.getValue();
			value += ")";
			technologiesString.put(technologyString.getKey(), value);
		}

		for (Entry<String, String> technologyString : technologiesString.entrySet()) {
			returnString += technologyString.getValue()+",";
		}
		
		if(!returnString.equals("")){
			returnString = returnString.substring(0,returnString.length()-1);
		}
		
		return returnString;
	}

	private void writeSVG() {
		RscriptCommandLine rsciptCommand = new RscriptCommandLine(getRFilePath(benchmarkName));
		rsciptCommand.run();
	}
}
