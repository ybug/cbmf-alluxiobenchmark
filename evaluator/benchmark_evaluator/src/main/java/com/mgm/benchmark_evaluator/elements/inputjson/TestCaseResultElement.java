package com.mgm.benchmark_evaluator.elements.inputjson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * contains for the test case the results
 * @author bugge
 * @since 04.02.2016
 */
public class TestCaseResultElement {
	
	private String name;
	private boolean generatorEnable;
	private boolean generatorError;
	private List<RoundResultElement> rounds = new ArrayList<RoundResultElement>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RoundResultElement> getRounds() {
		Collections.sort(rounds);
		return rounds;
	}
	public void setRounds(List<RoundResultElement> rounds) {
		this.rounds = rounds;
	}
	
	public boolean isGeneratorEnable() {
		return generatorEnable;
	}
	public void setGeneratorEnable(boolean generatorEnable) {
		this.generatorEnable = generatorEnable;
	}
	public boolean isGeneratorError() {
		return generatorError;
	}
	public void setGeneratorError(boolean generatorError) {
		this.generatorError = generatorError;
	}
	@Override
	public String toString() {
		return "TestCaseResultElement [name=" + name + ", generatorEnable=" + generatorEnable + ", generatorError="
				+ generatorError + ", rounds=" + rounds + "]";
	}
}
