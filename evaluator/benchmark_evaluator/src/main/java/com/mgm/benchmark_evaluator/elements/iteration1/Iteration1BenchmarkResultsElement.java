package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.HashMap;
import java.util.Map;

public class Iteration1BenchmarkResultsElement {
	
	private Map<String,Iteration1ScenarioResultsElement> benchmarks = new HashMap<String,Iteration1ScenarioResultsElement>();

	public Map<String, Iteration1ScenarioResultsElement> getBenchmarks() {
		return benchmarks;
	}

	public void setBenchmarks(Map<String, Iteration1ScenarioResultsElement> benchmarks) {
		this.benchmarks = benchmarks;
	}
	
	
}
