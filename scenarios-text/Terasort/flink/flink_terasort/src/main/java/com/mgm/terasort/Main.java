package com.mgm.terasort;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.api.java.hadoop.mapreduce.HadoopOutputFormat;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.security.UserGroupInformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.security.PrivilegedExceptionAction;

import com.mgm.terasort.extern.eastcirclek.OptimizedFlinkTeraPartitioner;
import com.mgm.terasort.extern.eastcirclek.OptimizedText;
import com.mgm.terasort.extern.eastcirclek.TeraInputFormat;
import com.mgm.terasort.extern.eastcirclek.TeraOutputFormat;
import com.mgm.terasort.extern.eastcirclek.TotalOrderPartitioner;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.operators.Order;

/**
 * Skeleton for a Flink Job.
 *
 * For a full example of a Flink Job, see the WordCountJob.java file in the same package/directory or have a look at the website.
 *
 * You can also generate a .jar file that you can submit on your Flink cluster. Just type mvn clean package in the projects root directory. You will
 * find the jar in target/flink-quickstart-0.1-SNAPSHOT-Sample.jar
 *
 */
public class Main {

	public static void main(String[] args) throws Throwable {

		final ParameterTool params = ParameterTool.fromArgs(args);
		String hdfs = params.get("defaultFS",null);
		String inputPath = params.get("input", null);
		String outputFolderPath = params.get("output", null);
		String fileSystemUser = params.get("fsUser","flink");
		int partitions = params.getInt("partitions", 1);

		if (inputPath == null || outputFolderPath == null) {
			System.err.println("input or output not set in the arguments!");
			return;
		}

		JobConf mapRedConf = new JobConf();
		if(hdfs != null){
			mapRedConf.set("fs.defaultFS", hdfs);
		}
		mapRedConf.set("mapreduce.input.fileinputformat.inputdir", inputPath);
		mapRedConf.set("mapreduce.output.fileoutputformat.outputdir", outputFolderPath);
		mapRedConf.setInt("mapreduce.job.reduces", partitions);
		mapRedConf.set("hadoop.job.ugi", fileSystemUser);
		
		Path partitionFile = new Path(outputFolderPath, TeraInputFormat.PARTITION_FILENAME);
		Job jobContext = Job.getInstance(mapRedConf);
		writePartitions(fileSystemUser,partitionFile,jobContext);
		OptimizedFlinkTeraPartitioner partitioner = new OptimizedFlinkTeraPartitioner(new TotalOrderPartitioner(mapRedConf, partitionFile));

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		env.readHadoopFile(new TeraInputFormat(), Text.class, Text.class, inputPath)
				.map(new MapFunction<Tuple2<Text, Text>, Tuple2<OptimizedText, Text>>() {

					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<OptimizedText, Text> map(Tuple2<Text, Text> in) {
						return new Tuple2<OptimizedText, Text>(new OptimizedText(in.f0), in.f1);
					}
				}).partitionCustom(partitioner, 0).sortPartition(0, Order.ASCENDING)
				.map(new MapFunction<Tuple2<OptimizedText, Text>, Tuple2<Text, Text>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<Text, Text> map(Tuple2<OptimizedText, Text> in) {
						return new Tuple2<Text, Text>(in.f0.getText(), in.f1);
					}
				}).output(new HadoopOutputFormat<Text, Text>(new TeraOutputFormat(), jobContext));

		// execute program
		env.execute("Flink Terasort");
		
	
	}
	
	public static void writePartitions(String fileSystemUser,Path partitionFile,Job jobContext) throws IOException, InterruptedException{
		UserGroupInformation ugi = UserGroupInformation.createRemoteUser(fileSystemUser);
		ugi.doAs(new PartitionWriter(partitionFile,jobContext));
	}
	
	public static class PartitionWriter implements PrivilegedExceptionAction<Void>{

		public static Logger LOG = LoggerFactory.getLogger(PartitionWriter.class);
				
		private final Path partitionFile;
		private final Job jobContext;
		
		public PartitionWriter(Path partitionFile,Job jobContext) {
			this.jobContext=jobContext;
			this.partitionFile=partitionFile;
		}
		
		@Override
		public Void run() throws Exception {
			try {
				TeraInputFormat.writePartitionFile(jobContext, partitionFile);
			} catch (Throwable e) {
				LOG.error("An {} occurred.", "error", e);
			}
			return null;
		}
		
	}
}
