package com.mgm.alluxio_lineage_api;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.Lists;

import alluxio.AlluxioURI;
import alluxio.client.lineage.AlluxioLineage;
import alluxio.exception.AlluxioException;
import alluxio.exception.FileDoesNotExistException;
import alluxio.job.CommandLineJob;
import alluxio.job.JobConf;

/**
 * is the lineage API from alluxio for jobs.
 * wrapped the job to build DAG (edge algorithm) 
 * @author bugge
 * @since 04.06.2016
 */
public class Main 
{
	public static final int ARGS_LENGHT = 3;
	public static final String LOG_FILE_PATH = "/tmp/recompute.log";
	
	/**
	 * 
	 * @param args [0]-> input file [1]-> output file [2]-> job command line
	 * @throws IOException 
	 * @throws AlluxioException 
	 * @throws FileDoesNotExistException 
	 */
    public static void main( String[] args ) throws FileDoesNotExistException, AlluxioException, IOException
    {
    
    	if(args.length < ARGS_LENGHT){
    		System.err.println("Not enough arguments! args[0]: input file args[1]: output file args[2] until args[n]: job command line");
    		return;
    	}
    	String inputFile = args[0];
    	String outputFile = args[1];
    	String commandLine = args[2];
    	for(int counter = 3; counter < args.length; counter++) {
    		commandLine += " "+args[counter];
    	}
    	
    	AlluxioLineage tl = AlluxioLineage.get();
    	AlluxioURI input = new AlluxioURI(inputFile);
    	AlluxioURI output = new AlluxioURI(outputFile);
    	
    	List<AlluxioURI> inputFiles = Lists.newArrayList(input);
    	List<AlluxioURI> outputFiles = Lists.newArrayList(output);
    	
     	JobConf conf = new JobConf(LOG_FILE_PATH);
    	CommandLineJob job = new CommandLineJob(commandLine, conf);
    	long lineageId = tl.createLineage(inputFiles, outputFiles, job);
        System.out.println("Alluxio Lineage Job ID-> "+lineageId);
    }
}
