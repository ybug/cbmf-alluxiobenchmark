package com.mgm.comparator_wrapper;

import java.util.List;

import org.apache.log4j.Logger;

import com.mgm.comparator_wrapper.model.ArgumentsElement;
import com.mgm.comparator_wrapper.model.ArgumentsOperations;
import com.mgm.comparator_wrapper.model.CSVFileWriter;
import com.mgm.comparator_wrapper.model.CommandLineExecutor;
import com.mgm.comparator_wrapper.model.HDFSOperations;


public class App {
	
	private final static Logger LOGGER = Logger.getLogger(App.class.getName()); 

	public static void main(String[] args) {
		
		LOGGER.info("start compare");
		ArgumentsElement arguments = ArgumentsOperations.convertArgsToArgumentsElement(args);
		if(arguments.isValid()){
			LOGGER.info("is valid");
			CommandLineExecutor executor = new CommandLineExecutor(arguments.getCommand());
			if(executor.run()){
				LOGGER.error("execute error");
				System.exit(-1);
			}
			List<String> lines = HDFSOperations.getFolderContentAndCleanup(arguments.getHdfsUri(), arguments.getHdfsReportPath());
			if(CSVFileWriter.writeReport(arguments, lines)){
				LOGGER.error("write error");
				System.exit(-1);
			}
		}else {
			LOGGER.error("is not valid");
			System.exit(-1);
		}
	}
}
