package com.mgm.benchmark_evaluator.elements.inputjson;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * contains the results from one round
 * @author bugge
 * @since 04.02.2016
 */
public class RoundResultElement implements Comparable<RoundResultElement>{
	private Timestamp startTime;
	private Timestamp endTime;
	private Boolean executeError;
	private Boolean comparatorError;
	private Boolean comparatorEnable;
	private Boolean cleanupEnable;
	private Boolean cleanupError;
	
	private Map<Integer, HostResultElement> itemsByHost = new HashMap<Integer,HostResultElement>();
	
	public Timestamp getStartTime() {
		return startTime;
	}
	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}
	public Timestamp getEndTime() {
		return endTime;
	}
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}
	public Boolean getExecuteError() {
		return executeError;
	}
	public void setExecuteError(Boolean error) {
		this.executeError = error;
	}
	public Map<Integer, HostResultElement> getItemsByHost() {
		return itemsByHost;
	}
	public void setItemsByHost(Map<Integer, HostResultElement> itemsByHost) {
		this.itemsByHost = itemsByHost;
	}
	
	public Boolean getComparatorError() {
		return comparatorError;
	}
	public void setComparatorError(Boolean comparatorError) {
		this.comparatorError = comparatorError;
	}
	public Boolean getComparatorEnable() {
		return comparatorEnable;
	}
	public void setComparatorEnable(Boolean comparatorEnable) {
		this.comparatorEnable = comparatorEnable;
	}
	public Boolean getCleanupEnable() {
		return cleanupEnable;
	}
	public void setCleanupEnable(Boolean cleanupEnable) {
		this.cleanupEnable = cleanupEnable;
	}
	public Boolean getCleanupError() {
		return cleanupError;
	}
	public void setCleanupError(Boolean cleanupError) {
		this.cleanupError = cleanupError;
	}
	@Override
	public String toString() {
		return "RoundResultElement [startTime=" + startTime + ", endTime=" + endTime + ", executeError=" + executeError
				+ ", comparatorError=" + comparatorError + ", comparatorEnable=" + comparatorEnable + ", cleanupEnable="
				+ cleanupEnable + ", cleanupError=" + cleanupError + ", itemsByHost=" + itemsByHost + "]";
	}
	@Override
    public int compareTo(RoundResultElement another) {
        if (this.getStartTime().getTime() < another.getStartTime().getTime()){
            return -1;
        }else{
            return 1;
        }
    }
}
