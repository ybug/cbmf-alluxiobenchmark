package com.mgm.benchmark_evaluator.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.inputjson.BenchmarkElement;
import com.mgm.benchmark_evaluator.elements.inputjson.HostInformationElement;
import com.mgm.benchmark_evaluator.elements.inputjson.HostResultElement;
import com.mgm.benchmark_evaluator.elements.inputjson.ItemInformationElement;
import com.mgm.benchmark_evaluator.elements.inputjson.ItemResultElement;
import com.mgm.benchmark_evaluator.elements.inputjson.ItemResultsElement;
import com.mgm.benchmark_evaluator.elements.inputjson.RoundResultElement;
import com.mgm.benchmark_evaluator.elements.inputjson.ScenarioResultElement;
import com.mgm.benchmark_evaluator.elements.inputjson.TestCaseResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ScenarioResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1RoundHostsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1RoundsResultElement;
import com.mgm.benchmark_evaluator.models.DefaultValues;
import com.mgm.benchmark_evaluator.models.MathOperations;
import com.mgm.benchmark_evaluator.models.StringTemplates;

/**
 * compute delta t, performance and average from each round + test case metrics 
 * @author bugge
 *
 */
public class Iteration1 {

	private final BenchmarkElement benchmark;
	private final Map<Integer,ItemInformationElement> itemsInformation;
	private final static Logger LOGGER = Logger.getLogger(Iteration1.class.getName());

	public Iteration1(BenchmarkElement benchmark,Map<Integer,ItemInformationElement> itemsInformation) {
		this.benchmark = benchmark;
		this.itemsInformation = itemsInformation;
	}

	public Iteration1ScenarioResultsElement compute() {
		Iteration1ScenarioResultsElement scenarioResults = new Iteration1ScenarioResultsElement(benchmark.getName());
		for (Entry<String, ScenarioResultElement> scenario : benchmark.getBenchmarkResults().getScenarios().entrySet()) {
			for (Entry<String, TestCaseResultElement> testcase : scenario.getValue().getTestCases().entrySet()) {
				computerTestCase(scenario.getValue(), testcase.getValue(), scenarioResults);
			}
		}
		//writeOut(scenarioResults);
		return scenarioResults;
	}

	private void computerTestCase(ScenarioResultElement scenario, TestCaseResultElement testCase, Iteration1ScenarioResultsElement scenarioResults) {
		if (!isOccuredError(testCase)) {
			Iteration1ResultElement time = computeTime(scenario, testCase, scenarioResults);
			computePerformance(scenario, testCase, scenarioResults);
			computeItems(scenario, testCase, scenarioResults,time);
		} else {
			LOGGER.error(StringTemplates.outputTastCaseError(scenario.getName(), testCase.getName()));
		}
	}

	private Iteration1ResultElement computeTime(ScenarioResultElement scenario, TestCaseResultElement testCase, Iteration1ScenarioResultsElement scenarioResults) {
		Iteration1ResultElement result = aggregateTime(testCase);
		result.setScaleFactor(scenario.getDefaultValues().get(DefaultValues.SCALFACTOR_KEY).getValueLong());
		scenarioResults.addResultsToComputer(DefaultValues.COMPUTER_SUMMARYNAME, DefaultValues.TIME_FILENAME, DefaultValues.TIME_UNIT, DefaultValues.TIME_DEFAULTINDX, result);
		return result;
	}
	
	private void computePerformance(ScenarioResultElement scenario, TestCaseResultElement testCase, Iteration1ScenarioResultsElement scenarioResults) {
		Long scaleFactor = scenario.getDefaultValues().get(DefaultValues.SCALFACTOR_KEY).getValueLong();
		Iteration1ResultElement result = aggregatePerformance(testCase,scaleFactor);
		result.setScaleFactor(scaleFactor);
		scenarioResults.addResultsToComputer(DefaultValues.COMPUTER_SUMMARYNAME, DefaultValues.PERFORMANCE_FILENAME, DefaultValues.PERFORMANCE_UNIT, DefaultValues.PERFORMANCE_DEFAULTINDX, result);
	}

	private void computeItems(ScenarioResultElement scenario, TestCaseResultElement testCase, Iteration1ScenarioResultsElement scenarioResults,Iteration1ResultElement time) {
		Iteration1RoundsResultElement roundResults = new Iteration1RoundsResultElement(testCase.getRounds().size());

		for (RoundResultElement round : testCase.getRounds()) {
			// error fehlt (for hosts -> id lesen und raus schreiben)
			for (Entry<Integer, HostResultElement> host : round.getItemsByHost().entrySet()) {
				computeItemsFromHost(host, round,roundResults);
			}
		}
		
		for(Entry<Integer,Iteration1RoundHostsElement> host : roundResults.getHosts().entrySet()){
			for(Entry<Integer, List<Double>> item : host.getValue().getItems().entrySet()){
				List<Long> mesurmentPointNumbers = host.getValue().getMesurmentPointNumbers().get(item.getKey());
				MathOperations math = new MathOperations();
				if(!host.getValue().isError(item.getKey())){
					List<Double> elements = new ArrayList<Double>();
					List<Double> qualitiesInPercent = new ArrayList<Double>();
					for(Double itemRound : item.getValue()){
						math.addDoubleValue(itemRound);
						elements.add(itemRound);
					}
					for(int roundIndex = 0; roundIndex < time.getElements().size(); roundIndex++){
						if(mesurmentPointNumbers.size() > roundIndex){
							qualitiesInPercent.add(getQualityInPercent(mesurmentPointNumbers.get(roundIndex),time.getElements().get(roundIndex)));
						}else{
							qualitiesInPercent.add(new Double(0));
						}
					}
					
					Iteration1ResultElement result = new Iteration1ResultElement();
					result.setMax(math.getMax());
					result.setMean(math.getMean());
					result.setMin(math.getMin());
					result.setElements(elements);
					result.setStandardDeviation(math.getStandardDeviation());
					result.setScaleFactor(scenario.getDefaultValues().get(DefaultValues.SCALFACTOR_KEY).getValueLong());
					result.setQualitiesInPercent(qualitiesInPercent);
					scenarioResults.addResultsToComputer(
							getSummaryHostName(benchmark.getBenchmarkResults().getHostsInformation().get(host.getKey()).getName()),
							benchmark.getBenchmarkResults().getItemsInformation().get(item.getKey()).getName(),
							benchmark.getBenchmarkResults().getItemsInformation().get(item.getKey()).getUnit(),
							item.getKey(),
							result);
				}
			}
		}
	}
	
	private Double getQualityInPercent(long elementNumber, double timeInSeconds){
		return new Double((100 / Math.ceil(timeInSeconds))*(double) elementNumber);
	}
	
	private String getSummaryHostName(String hostName){
		if(hostName.contains(DefaultValues.MASTER_NAME1) || hostName.contains(DefaultValues.MASTER_NAME2)){
			return DefaultValues.MASTER_NAME2;
		}else if (hostName.contains(DefaultValues.WORKER_NAME)){
			String[] nameList = hostName.split(" ");
			String resultName = "";
			boolean found = false;
			for(String name : nameList){
				if(found || name.contains(DefaultValues.WORKER_NAME)){
					resultName += name;
					found = true;
				}
			}
			return resultName;
		}else{
			return hostName;
		}
	}

	/**
	 * Possible data formats:
		0 - numeric float;
		1 - character;
		2 - log;
		3 - numeric unsigned;
		4 - text. 
	 * @param host
	 * @param round
	 */
	private void computeItemsFromHost(Entry<Integer, HostResultElement> host,RoundResultElement round,Iteration1RoundsResultElement roundResults){
		HostInformationElement hostMetha = benchmark.getBenchmarkResults().getHostsInformation().get(host.getKey());
		if(hostMetha.isEnable()){
			for(Entry<Integer, ItemResultsElement> item : round.getItemsByHost().get(host.getKey()).getItems().entrySet()){
				if(itemsInformation.get(item.getKey()).getTemplateId() <= 0){
					continue;
				}
				ItemInformationElement itemMetha = benchmark.getBenchmarkResults().getItemsInformation().get(item.getKey());
				
				if(itemMetha.isEnable() && ((itemMetha.getDataformat() == 0)||(itemMetha.getDataformat() == 3)) && itemMetha.getUnit().equals(DefaultValues.TIME_UNIT)){
					computeTimeItemFromHost(host, roundResults, hostMetha, item, itemMetha);
				}else if(itemMetha.isEnable() && ((itemMetha.getDataformat() == 0)||(itemMetha.getDataformat() == 3))){
					computeNotTimeItemFromHost(host, roundResults, hostMetha, item, itemMetha);
				}
			}
		}
	}

	private void computeNotTimeItemFromHost(Entry<Integer, HostResultElement> host, Iteration1RoundsResultElement roundResults,
			HostInformationElement hostMetha, Entry<Integer, ItemResultsElement> item, ItemInformationElement itemMetha) {
		boolean error = false;
		Long measurementPointNumber = new Long(0); 
		MathOperations mathOP = new MathOperations();
		for(ItemResultElement itemResult : item.getValue().getResults()){
			if(itemResult.getDataFormat() == 0){
				if(itemResult.getValueFloat() == null){
					LOGGER.error("Null in Item -> "+itemMetha.getName()+" in host -> "+hostMetha.getName());
					error = true;
					break;
				}
				measurementPointNumber++;
				mathOP.addFloatValue(itemResult.getValueFloat());
			}else if(itemResult.getDataFormat() == 3){
				if(itemResult.getValueLong() == null){
					LOGGER.error("Null in Item -> "+itemMetha.getName()+" in host -> "+hostMetha.getName());
					error = true;
					break;
				}
				measurementPointNumber++;
				mathOP.addLongValue(itemResult.getValueLong());
			}
		}
		if(error){
			roundResults.addError(host.getKey(), item.getKey());
		}else{
			roundResults.addAverageValue(host.getKey(), item.getKey(), mathOP.getMean(),measurementPointNumber);
		}
	}
	
	private void computeTimeItemFromHost(Entry<Integer, HostResultElement> host, Iteration1RoundsResultElement roundResults,
			HostInformationElement hostMetha, Entry<Integer, ItemResultsElement> item, ItemInformationElement itemMetha){
		boolean error = false;

		if(item.getValue().getResults().isEmpty()){
			roundResults.addError(host.getKey(), item.getKey());
			return ;
		}
		
		ItemResultElement minResult = item.getValue().getResults().get(0);
		ItemResultElement maxResult = item.getValue().getResults().get(item.getValue().getResults().size()-1);
		double time = 0;
		if(minResult.getDataFormat() == 0){
			if(minResult.getValueFloat() == null || maxResult.getValueFloat() == null){
				LOGGER.error("Null in Item -> "+itemMetha.getName()+" in host -> "+hostMetha.getName());
				error = true;
			}else{
				time = maxResult.getValueFloat() - minResult.getValueFloat();
			}
		}else if(minResult.getDataFormat() == 3){
			if(minResult.getValueLong() == null || maxResult.getValueLong() == null){
				LOGGER.error("Null in Item -> "+itemMetha.getName()+" in host -> "+hostMetha.getName());
				error = true;
			}else{
				time = maxResult.getValueLong() - minResult.getValueLong();
			}
		}
		
		if(error){
			roundResults.addError(host.getKey(), item.getKey());
		}else{
			roundResults.addAverageValue(host.getKey(), item.getKey(), time,2);
		}
	}

	private Iteration1ResultElement aggregateTime(TestCaseResultElement testCase) {
		MathOperations math = new MathOperations();
		List<Double> times = new ArrayList<Double>();
		List<Double> qualitiesInPercent = new ArrayList<Double>();
		for (RoundResultElement round : testCase.getRounds()) {
			long usedTime = (round.getEndTime().getTime() - round.getStartTime().getTime())/1000;
			math.addLongValue(usedTime);
			times.add((double)usedTime);
			qualitiesInPercent.add(new Double(100));
		}
		Iteration1ResultElement result = new Iteration1ResultElement();
		result.setMax(math.getMax());
		result.setMean(math.getMean());
		result.setMin(math.getMin());
		result.setElements(times);
		result.setStandardDeviation(math.getStandardDeviation());
		result.setQualitiesInPercent(qualitiesInPercent);
		return result;
	}
	
	private Iteration1ResultElement aggregatePerformance(TestCaseResultElement testCase,Long scaleFactor) {
		MathOperations math = new MathOperations();
		List<Double> performance = new ArrayList<Double>();
		List<Double> qualitiesInPercent = new ArrayList<Double>();
		for (RoundResultElement round : testCase.getRounds()) {
			double runTimeMs = (double) (round.getEndTime().getTime() - round.getStartTime().getTime());
			double runTimeMin = runTimeMs/(double)60000;
			double usedPerformance = (double)scaleFactor/runTimeMin;
			math.addDoubleValue(usedPerformance);
			performance.add((double)usedPerformance);
			qualitiesInPercent.add(new Double(100));
		}
		Iteration1ResultElement result = new Iteration1ResultElement();
		result.setMax(math.getMax());
		result.setMean(math.getMean());
		result.setMin(math.getMin());
		result.setElements(performance);
		result.setStandardDeviation(math.getStandardDeviation());
		result.setQualitiesInPercent(qualitiesInPercent);
		return result;
	}

	/*private void writeOut(Iteration1ScenarioResultsElement scenarioResults) {

		for (Entry<String, Iteration1ComputerResultsElement> computer : scenarioResults.getComputer().entrySet()) {
			for (Entry<String, Iteration1ResultsElement> results : computer.getValue().getResults().entrySet()) {
				OutputController writeOperations = new OutputController(benchmark.getName(), DefaultValues.ITERATION1_NAME, computer.getKey(),
						results.getKey());
				writeOperations.printResults(results.getValue());
			}
		}
	}*/

	private boolean isOccuredError(TestCaseResultElement testCase) {
		boolean error = isError(testCase.isGeneratorEnable(), testCase.isGeneratorError());
		if (error) {
			return error;
		}

		for (RoundResultElement round : testCase.getRounds()) {
			error = round.getExecuteError();
			if(error){
				return error;
			}
			error = isError(round.getCleanupEnable(), round.getCleanupError());
			if (error) {
				return error;
			}
			error = isError(round.getComparatorEnable(), round.getComparatorError());
			if (error) {
				return error;
			}
		}
		return error;
	}

	private boolean isError(Boolean enable, Boolean error) {
		if (enable == null) {
			return false;
		}
		if (enable) {
			if (error == null) {
				return false;
			} else {
				return error;
			}
		}
		return false;
	}
}
