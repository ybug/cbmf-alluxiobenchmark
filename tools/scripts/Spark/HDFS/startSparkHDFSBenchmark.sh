#!/bin/bash

echo "#"
echo "### Spark HDFS Wordcount"
echo "#"

sh Spark/HDFS/startSparkHDFSWordcountBenchmark.sh _part1
sh Spark/HDFS/startSparkHDFSWordcountBenchmark.sh _part2
sh Spark/HDFS/startSparkHDFSWordcountBenchmark.sh _part3
sh Spark/HDFS/startSparkHDFSWordcountBenchmark.sh _part4
sh Spark/HDFS/startSparkHDFSWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark HDFS Copy"
echo "#"

sh Spark/HDFS/startSparkHDFSCopyBenchmark.sh _part1
sh Spark/HDFS/startSparkHDFSCopyBenchmark.sh _part2
sh Spark/HDFS/startSparkHDFSCopyBenchmark.sh _part3
sh Spark/HDFS/startSparkHDFSCopyBenchmark.sh _part4
sh Spark/HDFS/startSparkHDFSCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark HDFS Terasort"
echo "#"

sh Spark/HDFS/startSparkHDFSTeraBenchmark.sh _part1
sh Spark/HDFS/startSparkHDFSTeraBenchmark.sh _part2
sh Spark/HDFS/startSparkHDFSTeraBenchmark.sh _part3
sh Spark/HDFS/startSparkHDFSTeraBenchmark.sh _part4
sh Spark/HDFS/startSparkHDFSTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark HDFS TF-IDF"
echo "#"

sh Spark/HDFS/startSparkHDFSTfidfBenchmark.sh _part1
sh Spark/HDFS/startSparkHDFSTfidfBenchmark.sh _part2
sh Spark/HDFS/startSparkHDFSTfidfBenchmark.sh _part3
#sh Spark/HDFS/startSparkHDFSTfidfBenchmark.sh _part4
#sh Spark/HDFS/startSparkHDFSTfidfBenchmark.sh _part5
