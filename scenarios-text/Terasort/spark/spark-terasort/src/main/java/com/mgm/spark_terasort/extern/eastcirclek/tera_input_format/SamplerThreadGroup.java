package com.mgm.spark_terasort.extern.eastcirclek.tera_input_format;

public class SamplerThreadGroup extends ThreadGroup {

	private Throwable throwable;

	public SamplerThreadGroup(String s) {
		super(s);
	}

	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {
		this.throwable = throwable;
	}

	public Throwable getThrowable() {
		return this.throwable;
	}

}