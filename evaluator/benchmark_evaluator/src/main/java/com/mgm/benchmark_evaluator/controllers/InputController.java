package com.mgm.benchmark_evaluator.controllers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mgm.benchmark_evaluator.elements.Arguments;
import com.mgm.benchmark_evaluator.elements.inputjson.BenchmarkElement;
import com.mgm.benchmark_evaluator.elements.inputjson.BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.models.DefaultValues;
import com.mgm.benchmark_evaluator.models.FileFolderOperations;
import com.mgm.benchmark_evaluator.models.StringTemplates;

public class InputController {

	private final static Logger LOGGER = Logger.getLogger(InputController.class.getName());

	private Arguments arguments;
	private Map<String, String> inputFilePaths = new HashMap<String, String>();

	public InputController(Arguments arguments) {
		this.arguments = arguments;
		collectAllFiles();
	}

	public BenchmarkElement getResult(Entry<String, String> inputPath) {

		return new BenchmarkElement(inputPath.getKey(), readJson(inputPath.getValue()));
	}
	
	public Map<String,String> getInputFilePaths(){
		return inputFilePaths;
	}
	
	private void collectAllFiles() {
		for (String folder : FileFolderOperations.getFoldersFromPath(arguments.getInputPath())) {
			String resultFilePath = null;
			String source = null;
			for (String file : FileFolderOperations.getFilesFromPath(folder)) {
				if (FileFolderOperations.getFileNameFromPath(file).equals(DefaultValues.INPUTFILENAME_RESULTJSON)) {
					resultFilePath = file;
				}
				if (FileFolderOperations.getFileNameFromPath(file).contains(DefaultValues.INPUTFILENAME_CONFIGNAME)
						&& FileFolderOperations.getFileNameFromPath(file).contains(DefaultValues.INPUTFILENAME_CONFIGENDIG)) {
					source = FileFolderOperations.getFileNameFromPath(file).replace(DefaultValues.INPUTFILENAME_CONFIGNAME, "")
							.replace(DefaultValues.INPUTFILENAME_CONFIGENDIG, "");
				}
			}
			if (resultFilePath != null && source != null) {
				inputFilePaths.put(source, resultFilePath);
			}
		}
		printInputPaths();
	}

	private void printInputPaths() {
		LOGGER.info(StringTemplates.outputInputPaths);
		for (Entry<String, String> inputPath : inputFilePaths.entrySet()) {
			LOGGER.info(StringTemplates.outputInputFile(inputPath.getKey(), inputPath.getValue()));
		}
	}

	private BenchmarkResultsElement readJson(String path) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(new File(path), BenchmarkResultsElement.class);
		} catch (JsonGenerationException e) {
			LOGGER.error(StringTemplates.outputCantReadResultsInJsonFile(), e);
		} catch (JsonMappingException e) {
			LOGGER.error(StringTemplates.outputCantReadResultsInJsonFile(), e);
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantReadResultsInJsonFile(), e);
		}
		return null;
	}
}
