package com.mgm.benchmark_evaluator.models;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;
import com.opencsv.CSVWriter;

public class CsvOperationsMinMax {

	private final String resultFolderPath;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(CsvOperationsMinMax.class.getName()); 

	public CsvOperationsMinMax(String benchmarkName,String resultFolderPath,String iterationName,String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileName = fileName;
		this.computerName = computerName;
		this.iterationName = iterationName;	
		this.benchmarkName = benchmarkName;
	}
	
	public void writeCSV(Iteration1ResultsElement results){
		String folderPath = getOutputFolderPath();
		FileFolderOperations.createFolderIfNotExist(folderPath);
		String path = getOutputPath();
		CSVWriter csvWriter;
		//LOGGER.info("Write to -> "+path);
		try {
			csvWriter = new CSVWriter(new FileWriter(path),
					DefaultValues.CSV_SEPERATOR,CSVWriter.NO_QUOTE_CHARACTER);
			List<String[]> data = toStringArray(results.getResults(),results.getUnit());
			csvWriter.writeAll(data);
			csvWriter.close();
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantWriteFile(path),e);
			e.printStackTrace();
		}
	}

	private String getOutputPath() {
		String scenario = "_"+getTitleScenario().replace(" ", "_");
		return Paths.get(getOutputFolderPath(), fileName.replaceAll("[^a-zA-Z0-9.-]", "_") + DefaultValues.MIN_MAX_FILES +scenario+ DefaultValues.CSV_ENDING).toString();
	}
	
	private String getTitleScenario() {
		
		String[] benchmarkNameSplits = benchmarkName.split("_");
		String title = StringTemplates.getUpperFirstChar(benchmarkNameSplits[0])
				+" "+StringTemplates.getAlgorithm(benchmarkNameSplits[1])+"_";
		if(computerName.equals(DefaultValues.COMPUTER_SUMMARYNAME)){
			String chartName = getSummaryScenarioTitle();
			return title+chartName;
		}
		return title+computerName;
	}
	
	private String getSummaryScenarioTitle() {
		String chartName = "";
		if(fileName.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.TIME_CHARTTITLE;
		}else if(fileName.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}

	private String getOutputFolderPath() {
		return Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
	}
	
	private List<String[]> toStringArray(List<Iteration1ResultElement> elements,String unit) {
		List<String[]> records = new ArrayList<String[]>();
		// add header record
		records.add(addToHeadLineUnit(addToHeadLineStandardDeviationInPercent(StringTemplates.CSV_HEADLINE_MINMAX),unit));
		
		getCorectSequence(elements);
		
		Iterator<Iteration1ResultElement> it = elements.iterator();
		while (it.hasNext()) {
			Iteration1ResultElement element = it.next();
			records.add(new String[] { element.getScaleFactorAsString(), element.getMinAsString(), element.getMaxAsString(),
					element.getMeanAsString(), element.getStandardDeviationAsString(), element.getStandardDeviationAsStringInPercent() });
		}
		return records;
	}
	
	private String[] addToHeadLineStandardDeviationInPercent(String[] headLine){
		String[] newHeadLine = new String[headLine.length+1];
		int index = 0;
		for(; index < headLine.length; index++){
			newHeadLine[index] = headLine[index];
		}
		newHeadLine[index] = DefaultValues.CSV_HEADLINE_STANDARD_DEVISION_PERCENT;
		return newHeadLine;
	}
	
	private void getCorectSequence(List<Iteration1ResultElement> elements){
		Collections.sort(elements, new Comparator<Iteration1ResultElement>() {
            @Override
            public int compare(Iteration1ResultElement lhs, Iteration1ResultElement rhs) {
                return lhs.getScaleFactor() < rhs.getScaleFactor() ? -1 : (lhs.getScaleFactor() < rhs.getScaleFactor() ) ? 1 : 0;
            }
        });
	}
	
	private String[] addToHeadLineUnit(String[] rawHeadLines, String unit){
	
		
		if(unit.isEmpty()){
			for(int index = 0; index < rawHeadLines.length; index++){
				rawHeadLines[index] = rawHeadLines[index].replace(DefaultValues.UNIT_PATTERN1, "");
			}
		}else {
			for(int index = 0; index < rawHeadLines.length; index++){
				rawHeadLines[index] = rawHeadLines[index].replace(DefaultValues.UNIT_PATTERN1, " ["+unit+"]");
			}
		}
		for(int index = 0; index < rawHeadLines.length; index++){
			rawHeadLines[index] = rawHeadLines[index].replace(DefaultValues.UNIT_PATTERN2, " ["+DefaultValues.UNIT_PERCENT+"]");
		}
		return rawHeadLines;
	}
 }