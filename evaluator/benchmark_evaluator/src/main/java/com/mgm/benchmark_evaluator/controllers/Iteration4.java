package com.mgm.benchmark_evaluator.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ComputerResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ScenarioResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration4.Iteration4BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration4.Iteration4ComputerResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration4.Iteration4ScenarioResultsElement;
import com.mgm.benchmark_evaluator.models.DefaultValues;

/**
 * Summary Algorithm -> only separated about Cluster Computing Frameworks
 * @author bugge
 *
 */
public class Iteration4 {

	private final Iteration3BenchmarkResultsElement benchmarksResultsIt3;
	
	private final static Logger LOGGER = Logger.getLogger(Iteration4.class.getName());
	
	public Iteration4(Iteration3BenchmarkResultsElement benchmarksResultsIt3) {
		this.benchmarksResultsIt3 = benchmarksResultsIt3;
	}
	
	public Iteration4BenchmarkResultsElement compute() {
		return mergeBenchmarks();
	}
	
	private Iteration4BenchmarkResultsElement mergeBenchmarks(){
		Iteration4BenchmarkResultsElement results = new Iteration4BenchmarkResultsElement();
		
		for(Entry<String,List<Iteration3ScenarioResultsElement>> mergedBenchmarks : getBenchmarksFromSameTechnologies().entrySet()){
			results.getBenchmarks().put(mergedBenchmarks.getKey(), getIt4Benchmark(mergedBenchmarks.getValue(),mergedBenchmarks.getKey()));
		}
		writeOut(results);
		return results;
	}
	
	private Map<String,List<Iteration3ScenarioResultsElement>> getBenchmarksFromSameTechnologies(){
		Map<String,List<Iteration3ScenarioResultsElement>> benchmarkFromSameTechnologies = new HashMap<String, List<Iteration3ScenarioResultsElement>>();
		
		for(Entry<String,Iteration3ScenarioResultsElement> benchmarkIt3 : benchmarksResultsIt3.getBenchmarks().entrySet()){
			String technologie = benchmarkIt3.getKey().split("_")[0];
			if(benchmarkFromSameTechnologies.containsKey(technologie)){
				benchmarkFromSameTechnologies.get(technologie).add(benchmarkIt3.getValue());
			}else{
				List<Iteration3ScenarioResultsElement> element = new ArrayList<Iteration3ScenarioResultsElement>();
				element.add(benchmarkIt3.getValue());
				benchmarkFromSameTechnologies.put(technologie, element);
			}
		}
		return benchmarkFromSameTechnologies;
	}
	
	private Iteration4ScenarioResultsElement getIt4Benchmark(List<Iteration3ScenarioResultsElement> it3Benchmarks, String techName){
		Iteration4ScenarioResultsElement it4Scenarios = new Iteration4ScenarioResultsElement(techName);
		for(Iteration3ScenarioResultsElement it3Benchmark : it3Benchmarks ){
			String algName = it3Benchmark.getName().split("_")[1];
			for(Entry<String,Iteration3ComputerResultsElement> it3Computer : it3Benchmark.getComputer().entrySet()){
				Iteration4ComputerResultsElement it4Computer = getIt4Computer(it4Scenarios, it3Computer);
				for(Entry<Integer,Iteration3ResultsElement> it3Item : it3Computer.getValue().getResults().entrySet()){
					if(it4Computer.getResults().containsKey(it3Item.getKey())){
						it4Computer.getResults().get(it3Item.getKey()).put(algName, it3Item.getValue());
					}else{
						Map<String,Iteration3ResultsElement> it4Item = new HashMap<String,Iteration3ResultsElement>();
						it4Item.put(algName, it3Item.getValue());
						it4Computer.getResults().put(it3Item.getKey(), it4Item);
					}
				}
				it4Scenarios.getComputer().put(it3Computer.getKey(), it4Computer);
			}
		}
		return it4Scenarios;
	}

	private Iteration4ComputerResultsElement getIt4Computer(Iteration4ScenarioResultsElement it4Scenarios,
			Entry<String, Iteration3ComputerResultsElement> it3Computer) {
		Iteration4ComputerResultsElement it4Computer;
		if(it4Scenarios.getComputer().containsKey(it3Computer.getKey())){
			it4Computer = it4Scenarios.getComputer().get(it3Computer.getKey());
		}else{
			it4Computer = new Iteration4ComputerResultsElement(it3Computer.getKey());
		}
		return it4Computer;
	}
	
	private void writeOut(Iteration4BenchmarkResultsElement it4Benchmaks){
		long counter = 1;
		for(Entry<String,Iteration4ScenarioResultsElement> it4Benchmark : it4Benchmaks.getBenchmarks().entrySet()){
			for(Entry<String,Iteration4ComputerResultsElement> it4Computer : it4Benchmark.getValue().getComputer().entrySet()){
				for(Entry<Integer,Map<String,Iteration3ResultsElement>> it4Items : it4Computer.getValue().getResults().entrySet()){
					OutputController writeOperations = new OutputController(it4Benchmark.getKey(), DefaultValues.ITERATION4_NAME, it4Computer.getKey(),
							Integer.toString(it4Items.getKey()));
					writeOperations.printResultsIt4(it4Items);	
				}
			}
			LOGGER.info(it4Benchmark.getKey()+" | "+counter+" \\ "+it4Benchmaks.getBenchmarks().size());
			counter++;
		}
	}
	
}
