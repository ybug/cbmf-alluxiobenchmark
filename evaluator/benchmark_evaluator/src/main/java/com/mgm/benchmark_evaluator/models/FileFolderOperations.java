package com.mgm.benchmark_evaluator.models;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 * contains all folder or file operations
 * 
 * @author bugge
 * @since 03.03.2016
 */
public class FileFolderOperations {

	private final static Logger LOGGER = Logger.getLogger(FileFolderOperations.class.getName());

	/**
	 * check to exist file
	 * 
	 * @param path
	 *            -> path to the file
	 * @return true if exist the file
	 */
	public static boolean existFile(String path) {
		File file = new File(path);
		if (file.exists() && !file.isDirectory()) {
			return true;
		}
		return false;
	}
	
	/**
	 * check to exist file or folder
	 * @param path -> path to the file or folder
	 * @return true if exist the file or folder
	 */
	public static boolean existFileOrFolder(String path){
		File file = new File(path);
		return file.exists();
	}

	/**
	 * create folder
	 * 
	 * @param folderPath
	 *            -> path to the folder
	 */
	public static void creatFolder(String folderPath) {
		File resultFolder = new File(folderPath);
		resultFolder.mkdirs();
	}

	/**
	 * copy the source file to the target path
	 * 
	 * @param sourcePath
	 *            -> is the path to the source file
	 * @param targetPath
	 *            -> file path to copy the source file
	 */
	public static void copyFile(String sourcePath, String targetPath) {
		try {
			Files.copy(Paths.get(sourcePath), Paths.get(targetPath));
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantCopyFile(sourcePath, targetPath), e);
		}
	}
	
	public static void copyFolder(String sourcePath, String targetPath){
		try {
			FileUtils.copyDirectory(new File(sourcePath), new File(targetPath));
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantCopyFolder(sourcePath, targetPath), e);
		}
	}

	/**
	 * extract the file name from the file path
	 * 
	 * @param filePath
	 *            -> is the path to extract the file name
	 * @return the extracted file name
	 */
	public static String getFileNameFromPath(String filePath) {
		File file = new File(filePath);
		return file.getName();
	}
	
	/**
	 * extract the folder path from the file path
	 * 
	 * @param filePath
	 *            -> is the path to extract the file name
	 * @return the extracted file name
	 */
	public static String getFolderPathFromFilePath(String filePath) {
		File file = new File(filePath);
		return file.getParent();
	}

	/**
	 * delete file
	 * 
	 * @param filePath -> is the file to delete
	 * @throws IOException 
	 */
	public static void deleteFile(String filePath) throws IOException {
		Files.delete(Paths.get(filePath));
	}
	
	/**
	 * create folder if not exist
	 * @param folderPath -> folder path to create
	 */
	public static void createFolderIfNotExist(String folderPath){
		if(!FileFolderOperations.existFileOrFolder(folderPath)){
			FileFolderOperations.creatFolder(folderPath);
		}
	}
	
	/**
	 * 
	 * @param path -> path of the file or folder
	 * @return true if a directory
	 */
	public static boolean isPathDirectory(String path){
		File folder = new File(path);
		return folder.isDirectory();
	}
	
	/**
	 * 
	 * @param path -> is the path to the file
	 * @return get the file from this path
	 */
	public static File getFileFromPath(String path){
		return new File(path);
	}
	
	/**
	 * delete folder recursively
	 * @param folderPath -> is the path to the folder to delete 
	 */
	public static void deleteFolder(String folderPath){
		Path directory = Paths.get(folderPath);
		if(new File(folderPath).exists() && new File(folderPath).isDirectory()){
			try {
				Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
					   @Override
					   public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						   Files.delete(file);
						   return FileVisitResult.CONTINUE;
					   }

					   @Override
					   public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
						   Files.delete(dir);
						   return FileVisitResult.CONTINUE;
					   }

				   });
			} catch (IOException e) {
				LOGGER.error(StringTemplates.outputCantDeleteFolder(folderPath), e);
			}
		}
	}
	
	public static List<String> getFoldersFromPath(String folderPath){
		List<String> foundedFolders = new ArrayList<String>();
		File folder = new File(folderPath);
		File[] files = folder.listFiles();
		for(File file : files){
			if(file.isDirectory()){
				foundedFolders.add(file.getAbsolutePath());
			}
		}
		return foundedFolders;
	}
	
	public static List<String> getFilesFromPath(String folderPath){
		List<String> foundedFiles = new ArrayList<String>();
		File folder = new File(folderPath);
		File[] files = folder.listFiles();
		for(File file : files){
			if(file.isFile()){
				foundedFiles.add(file.getAbsolutePath());
			}
		}
		return foundedFiles;
	}
	
	public static List<String> readFile(String filePath) throws IOException {
		List<String> fileLines = new ArrayList<String>();
		File file = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(file));

		String line = null;
		while ((line = br.readLine()) != null) {
			fileLines.add(line);
		}

		br.close();
		return fileLines;
	}

	public static void writeFile(String filePath, List<String> lines) throws IOException {
		FileOutputStream outputStream = new FileOutputStream(filePath);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
		BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

		for (String line : lines) {
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		}

		bufferedWriter.close();
	}
}
