package com.mgm.text_extractor;

import org.sweble.wikitext.engine.PageId;
import org.sweble.wikitext.engine.PageTitle;
import org.sweble.wikitext.engine.WtEngineImpl;
import org.sweble.wikitext.engine.config.WikiConfig;
import org.sweble.wikitext.engine.nodes.EngProcessedPage;
import org.sweble.wikitext.engine.utils.DefaultConfigEnWp;

public class WikiParser {
	
	public static String run(String wikiText) throws Exception
	{
		String EMPTY_PAGE_TITLE="test";
		
		// Set-up a simple wiki configuration
		WikiConfig config = DefaultConfigEnWp.generate();
		
		final int wrapCol = 80;
		
		// Instantiate a compiler for wiki pages
		WtEngineImpl engine = new WtEngineImpl(config);
		
		// Retrieve a page
		PageTitle pageTitle = PageTitle.make(config, EMPTY_PAGE_TITLE);
		
		PageId pageId = new PageId(pageTitle, -1);
		
		// Compile the retrieved page
		EngProcessedPage cp = engine.postprocess(pageId, wikiText, null);
		
		TextConverter p = new TextConverter(config, wrapCol);
		return (String) p.go(cp.getPage());
	}
}
