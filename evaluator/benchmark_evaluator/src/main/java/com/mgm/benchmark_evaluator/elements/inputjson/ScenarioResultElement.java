package com.mgm.benchmark_evaluator.elements.inputjson;

import java.util.HashMap;
import java.util.Map;

/**
 * contain the result from the scenario
 * @author bugge
 * @since 04.02.2016
 */
public class ScenarioResultElement {
	
	private String name;
	private Map<String,TestCaseResultElement> testCases = new HashMap<String,TestCaseResultElement>();
	private Map<String,ScenarioDefaultValueResultElement> defaultValues = new HashMap<String, ScenarioDefaultValueResultElement>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Map<String,TestCaseResultElement> getTestCases() {
		return testCases;
	}
	public void setTestCases(Map<String,TestCaseResultElement> testCases) {
		this.testCases = testCases;
	}
	
	public Map<String, ScenarioDefaultValueResultElement> getDefaultValues() {
		return defaultValues;
	}
	public void setDefaultValues(Map<String, ScenarioDefaultValueResultElement> defaultValues) {
		this.defaultValues = defaultValues;
	}
	@Override
	public String toString() {
		return "ScenarioResultElement [name=" + name + ", testCases=" + testCases + ", defaultValues=" + defaultValues
				+ "]";
	}
}
