package com.mgm.benchmark_evaluator;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.mgm.benchmark_evaluator.controllers.CompressorController;
import com.mgm.benchmark_evaluator.controllers.InputController;
import com.mgm.benchmark_evaluator.elements.Arguments;
import com.mgm.benchmark_evaluator.models.FileFolderOperations;
import com.mgm.benchmark_evaluator.models.R2OperationsMean;
import com.mgm.benchmark_evaluator.models.R2OperationsMergeAlgoritm;
import com.mgm.benchmark_evaluator.models.R2OperationsMergeQualityAlgoritm;
import com.mgm.benchmark_evaluator.models.R2OperationsMinMax;
import com.mgm.benchmark_evaluator.models.R2OperationsQualityRounds;
import com.mgm.benchmark_evaluator.models.R2OperationsRounds;
import com.mgm.benchmark_evaluator.models.R2OperationsScenarios;
import com.mgm.benchmark_evaluator.models.StringTemplates;

/**
 * Hello world!
 *
 */
public class Main 
{
	private final static Logger LOGGER = Logger.getLogger(Main.class.getName()); 
	
    public static void main( String[] args )
    {
    	
    	Arguments arguments = new Arguments();
		try {
			arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
		} catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
			LOGGER.error(e);
			LOGGER.error(StringTemplates.outputEndEvaluationTitle);
			System.exit(1);
		}
		
		setInGraphsColor(arguments);

		LOGGER.info(StringTemplates.outputStartEvaluationTitle);
		FileFolderOperations.deleteFolder(Arguments.getOutputPath());
		CompressorController compressor = new CompressorController(new InputController(arguments));
		compressor.compress();
		
		LOGGER.info(StringTemplates.outputEndEvaluationTitle);
    }
    
    public static void setInGraphsColor(Arguments arguments){
    	R2OperationsScenarios.WITH_COLOR = arguments.isWithColor();
    	R2OperationsMergeAlgoritm.WITH_COLOR = arguments.isWithColor();
    	R2OperationsMinMax.WITH_COLOR = arguments.isWithColor();
    	R2OperationsRounds.WITH_COLOR = arguments.isWithColor();
    	R2OperationsMean.WITH_COLOR = arguments.isWithColor();
    	R2OperationsQualityRounds.WITH_COLOR = arguments.isWithColor();
    	R2OperationsMergeQualityAlgoritm.WITH_COLOR = arguments.isWithColor();
    }
}
