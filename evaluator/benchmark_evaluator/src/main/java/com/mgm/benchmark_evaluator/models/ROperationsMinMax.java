package com.mgm.benchmark_evaluator.models;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;

public class ROperationsMinMax {
	
	private final String resultFolderPath;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final static Logger LOGGER = Logger.getLogger(ROperationsMinMax.class.getName()); 

	public ROperationsMinMax(String resultFolderPath,String iterationName,String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileName = fileName.split("_")[0];
		this.computerName = computerName;
		this.iterationName = iterationName;	
	}

	public void write(Iteration1ResultsElement results,String benchmarkName){
		
		try {
			writeR(results,benchmarkName);
			writeSVG(benchmarkName);
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}
	
	private void writeR(Iteration1ResultsElement results,String benchmarkName) throws IOException{
		List<String> lines = FileFolderOperations.readFile(DefaultValues.R_PLOT1_TEMPLATE_MINMAX_PATH);
		List<String> outputLines = new ArrayList<String>();
		for(String line: lines){
			line = line.replaceAll(StringTemplates.R_MIN_KEY, getMinValues(results))
					.replaceAll(StringTemplates.R_MAX_KEY, getMaxValues(results))
					.replaceAll(StringTemplates.R_MEAN_KEY, getMeanValues(results))
					.replaceAll(StringTemplates.R_STANDARDDEVIATION_KEY, getSDValues(results))
					.replace(StringTemplates.R_OURTPUTPATH_KEY, getSvgOutputPath(benchmarkName))
					.replaceAll(StringTemplates.R_SCALEFACTOR_KEY, getScaleFactors(results))
					.replace(StringTemplates.R_TITLE_KEY, getTitle(results))
					.replaceAll(StringTemplates.R_XTITLE_KEY, getXTitle())
					.replaceAll(StringTemplates.R_YTITLE_KEY, getYTitle(results));
			outputLines.add(line);
		}
		FileFolderOperations.createFolderIfNotExist(FileFolderOperations.getFolderPathFromFilePath(getRFilePath(benchmarkName)));
		FileFolderOperations.writeFile(getRFilePath(benchmarkName), outputLines);
	}
	
	private String getRFilePath(String benchmarkName){
		String folderPath = Paths.get(resultFolderPath,iterationName, benchmarkName,computerName).toString();
		return Paths.get(folderPath,DefaultValues.R_TEMPFOLDERNAME,fileName+DefaultValues.MIN_MAX_FILES+ DefaultValues.R_ENDING).toString();
	}
	
	private String getYTitle(Iteration1ResultsElement results){
		return results.getUnit();
	}
	
	private String getXTitle(){
		return DefaultValues.R_SCALEFACTOR_TITLE;
	}
	
	private String getTitle(Iteration1ResultsElement results){
		if(results.getName().indexOf("_") < 0){
			return results.getName();
		}
		return results.getName().substring(results.getName().indexOf("_"), results.getName().length());
	}
	
	private String getScaleFactors(Iteration1ResultsElement results){
		String returnString = "";
		for(Iteration1ResultElement result : results.getResults()){
			returnString += "\""+result.getScaleFactorAsString()+"\",";
		}
		if(returnString.endsWith(",")){
			returnString = returnString.substring(0, returnString.length()-1);
		}
		return returnString;
	}
	
	private String getSvgOutputPath(String benchmarkName){
		String folderPath = Paths.get(resultFolderPath,iterationName, benchmarkName,computerName).toString();
		return Paths.get(folderPath,fileName+DefaultValues.MIN_MAX_FILES+ DefaultValues.PDF_ENDING).toString();
	}
	
	private String getMeanValues(Iteration1ResultsElement results){
		String returnString = "";
		for(Iteration1ResultElement result : results.getResults()){
			returnString += result.getMeanAsString().replace(".", "").replace(",", ".")+",";
		}
		if(returnString.endsWith(",")){
			returnString = returnString.substring(0, returnString.length()-1);
		}
		return returnString;
	}
	
	private String getSDValues(Iteration1ResultsElement results){
		String returnString = "";
		for(Iteration1ResultElement result : results.getResults()){
			returnString += result.getStandardDeviationAsString().replace(".", "").replace(",", ".")+",";
		}
		if(returnString.endsWith(",")){
			returnString = returnString.substring(0, returnString.length()-1);
		}
		return returnString;
	}
	
	private String getMaxValues(Iteration1ResultsElement results){
		String returnString = "";
		for(Iteration1ResultElement result : results.getResults()){
			returnString += result.getMaxAsString().replace(".", "").replace(",", ".")+",";
		}
		if(returnString.endsWith(",")){
			returnString = returnString.substring(0, returnString.length()-1);
		}
		return returnString;
	}
	
	private String getMinValues(Iteration1ResultsElement results){
		String returnString = "";
		for(Iteration1ResultElement result : results.getResults()){
			returnString += result.getMinAsString().replace(".", "").replace(",", ".")+",";
		}
		if(returnString.endsWith(",")){
			returnString = returnString.substring(0, returnString.length()-1);
		}
		return returnString;
	}
	
	private void writeSVG(String benchmarkName){
		RscriptCommandLine rsciptCommand = new RscriptCommandLine(getRFilePath(benchmarkName));
		rsciptCommand.run();
	}
}
