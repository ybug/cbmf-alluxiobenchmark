package com.mgm.config_builder.controller;

import java.util.HashMap;
import java.util.Map;

import com.mgm.config_builder.elements.PropertiesElement;
import com.mgm.config_builder.model.FileOperations;
import com.mgm.config_builder.model.PropertiesConverter;
import com.mgm.config_builder.model.PropertiesOperations;
import com.mgm.config_builder.model.StringTemplates;

public class PropertiesManager {
	private PropertiesElement properties = null;
	private final PropertiesOperations propertiesOperations;
	private final String propertiesPath;
	private final String[] args;

	/**
	 * 
	 * @param propertiesPath -> is the path to the properties file 
	 */
	public PropertiesManager(String propertiesPath,String[] args) {
		this.propertiesOperations = new PropertiesOperations(propertiesPath);
		this.propertiesPath = propertiesPath;
		this.args = args;
	}

	/**
	 * load the properties file and convert this to PropertiesElement
	 * @return true if occur a error
	 */
	public boolean loadAndConvertPropertiesFromFile() {
		if(loadPropertiesFromFile()){
			return true; 
		}
		convertProperties();
		return false;
	}

	/**
	 * load from file the properties
	 * @return true if error
	 */
	private boolean loadPropertiesFromFile() {
		return this.propertiesOperations.loadProperties();
	}
	
	/**
	 * convert the loaded properties to PropertiesElement
	 */
	private void convertProperties() {
		PropertiesConverter converter = new PropertiesConverter(this.propertiesPath);
		converter.setPropertiesMap(getKeyValuesFromProperties());
		converter.convert(this.args);
		this.properties = converter.getPropertiesElement();
	}

	/**
	 * 
	 * @return all key value pairs from the properties file
	 */
	private Map<String, String> getKeyValuesFromProperties() {
		Map<String, String> keyValues = new HashMap<String, String>();
		keyValues.put(StringTemplates.ABSOLUT_PATH_KEY, FileOperations.getLocationPath());
		for(Object key : this.propertiesOperations.getAllKeys()){
			keyValues.put((String) key, this.propertiesOperations.getProperty((String) key, ""));
		}
		return keyValues;
	}

	/**
	 * 
	 * @return the founded properties
	 */
	public PropertiesElement getProperties() {
		return this.properties;
	}
}
