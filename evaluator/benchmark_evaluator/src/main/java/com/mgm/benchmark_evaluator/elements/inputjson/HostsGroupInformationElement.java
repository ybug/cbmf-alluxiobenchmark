package com.mgm.benchmark_evaluator.elements.inputjson;

/**
 * contains the host group information. this information get from zabbix.
 * @author bugge
 *
 */
public class HostsGroupInformationElement {
	
	private String name;
	private Integer id;
	private String json;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getJson() {
		return json;
	}
	public void setJson(String json) {
		this.json = json;
	}
	@Override
	public String toString() {
		return "HostsGroupInformationElement [name=" + name + ", id=" + id + ", json=" + json + "]";
	}
	
}
