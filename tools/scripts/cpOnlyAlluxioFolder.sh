#!/bin/bash


#alluxio://10.199.200.2:19998/benchmark/flink/cache/results/cp_flink/1/target/
#/home/ybugge/benchmark/benchmark/generator/text/1/
SOURCE_DIR=$1
TARGET_DIR=$2
ALLUXIO_DIR_EX=$3

#SOURCE_DIR="/home/ybugge/benchmark/benchmark/generator/text/1/"
#TARGET_DIR="alluxio://10.199.200.2:19998/benchmark/flink/cache/results/cp_flink/1/source/"
#ALLUXIO_DIR_EX="/opt/alluxio/alluxio-1.1.0"

if [[ "$SOURCE_DIR" == */ ]]
then
   SOURCE_DIR=${SOURCE_DIR%?}

fi

if [[ "$TARGET_DIR" == */ ]]
then
   TARGET_DIR=${TARGET_DIR%?}

fi

if [[ "ALLUXIO_DIR_EX" == */ ]]
then
   ALLUXIO_DIR_EX=${ALLUXIO_DIR_EX?}

fi

SOURCE_DIR=$SOURCE_DIR"/*"
TARGET_DIR=$TARGET_DIR"/"
ALLUXIO_DIR_EX=$ALLUXIO_DIR_EX/bin/alluxio

for SOURCE_FILE in $SOURCE_DIR
do
	SOURCE_FILE_SEP_POS_temp=$(echo $SOURCE_FILE | grep -b -o / | tail -1| cut -d'/' -f 1)
	SOURCE_FILE_SEP_POS=$((${SOURCE_FILE_SEP_POS_temp%?}+1))
	SOURCE_FILE_NAME=$(echo $SOURCE_FILE | cut -c$SOURCE_FILE_SEP_POS- | cut -c2-)
	TARGET_DIR_FILEPATH=$TARGET_DIR$SOURCE_FILE_NAME
	echo $ALLUXIO_DIR_EX " fs copyFromLocal "$SOURCE_FILE" "$TARGET_DIR_FILEPATH
	$ALLUXIO_DIR_EX fs copyFromLocal $SOURCE_FILE $TARGET_DIR_FILEPATH
done
