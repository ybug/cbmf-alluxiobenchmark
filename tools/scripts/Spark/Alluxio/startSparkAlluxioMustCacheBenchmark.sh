#!/bin/bash

echo "#"
echo "### Spark AlluxioMustCache Wordcount"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheWordcountBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheWordcountBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheWordcountBenchmark.sh _part3
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheWordcountBenchmark.sh _part4
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioMustCache Copy"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheCopyBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheCopyBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheCopyBenchmark.sh _part3
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheCopyBenchmark.sh _part4
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioMustCache Terasort"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTeraBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTeraBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTeraBenchmark.sh _part3
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTeraBenchmark.sh _part4
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioMustCache TF-IDF"
echo "#"

sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTfidfBenchmark.sh _part1
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTfidfBenchmark.sh _part2
sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTfidfBenchmark.sh _part3
#sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTfidfBenchmark.sh _part4
#sh Spark/Alluxio/MustCache/startSparkAlluxioMustCacheTfidfBenchmark.sh _part5
