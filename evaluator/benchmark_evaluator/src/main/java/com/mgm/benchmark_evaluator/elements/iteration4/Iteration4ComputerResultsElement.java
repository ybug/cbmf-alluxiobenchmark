package com.mgm.benchmark_evaluator.elements.iteration4;

import java.util.HashMap;
import java.util.Map;

import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;

public class Iteration4ComputerResultsElement {

	private final String name;
	//algorithm name
	private Map<Integer,Map<String,Iteration3ResultsElement>> results = new HashMap<Integer,Map<String,Iteration3ResultsElement>>();
	
	public Iteration4ComputerResultsElement(String name) {
		this.name = name;
	}
	
	public void setResults(Map<Integer,Map<String,Iteration3ResultsElement>> results){
		this.results = results;
	}

	public String getName() {
		return name;
	}

	public Map<Integer, Map<String,Iteration3ResultsElement>> getResults() {
		return results;
	}

	@Override
	public String toString() {
		return "Iteration4ComputerResultsElement [name=" + name + ", results=" + results + "]";
	}
}
