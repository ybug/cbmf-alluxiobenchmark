package com.mgm.config_builder.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.config_builder.elements.PropertiesElement;
import com.mgm.config_builder.model.FileOperations;

public class ConfigManager {
	
	private final static Logger LOGGER = Logger.getLogger(ConfigManager.class.getName()); 

	private final PropertiesElement properties;
	
	public ConfigManager(PropertiesElement properties) {
		this.properties = properties;
	}
	
	public boolean build(){
		boolean error = false;
		FileOperations fs = new FileOperations();
		for(String templatePath: fs.getTempaltesPaths(properties.getTemplateFolderPath())){
			if(templatePath.contains(properties.getReplaceCopyNumberString())){
				for(int copyCounter = 1; copyCounter <= properties.getNumberOfCopies(); copyCounter++){
					try {
						List<String> fileContent = fillTemplateLines(getCopyTemlpateLines(fs.readFile(templatePath),copyCounter));
						String targetFilePath = fs.getTargetFilePath(templatePath, properties.getTargetFolderPath())
								.replace(properties.getReplaceCopyNumberString(), Integer.toString(copyCounter));
						fs.createFolderIfNotExist(properties.getTargetFolderPath());
						fs.writeFile(targetFilePath, fileContent);
						LOGGER.info("write configuration to -> "+targetFilePath);
						
					} catch (IOException e) {
						error = true;
						LOGGER.error("Cant read file: "+templatePath,e);
					}
				}
			}else{
				try {
					List<String> fileContent = fillTemplateLines(fs.readFile(templatePath));
					String targetFilePath = fs.getTargetFilePath(templatePath, properties.getTargetFolderPath());
					fs.createFolderIfNotExist(properties.getTargetFolderPath());
					fs.writeFile(targetFilePath, fileContent);
					LOGGER.info("write configuration to -> "+targetFilePath);
					
				} catch (IOException e) {
					error = true;
					LOGGER.error("Cant read file: "+templatePath,e);
				}
			}
		}
		return error;
	}
	
	private List<String> getCopyTemlpateLines(List<String> templateLines, int copyNumber){
		List<String> outputLines = new ArrayList<String>();
		for(String line: templateLines){
			outputLines.add(line.replaceAll(properties.getReplaceCopyNumberString(), Integer.toString(copyNumber)));
		}
		return outputLines;
	}
	
	private List<String> fillTemplateLines(List<String> templateLines){
		List<String> outputLines = new ArrayList<String>();
		for(String line: templateLines){
			outputLines.add(fillTemplateLine(line));
		}
		return outputLines;
	}
	
	private String fillTemplateLine(String templateLine){
		String resultLine = templateLine;
		for(Entry<String,String> property : properties.getProperties().entrySet()){
			resultLine = resultLine.replaceAll(property.getKey(), property.getValue());
		}
		return resultLine;
	}
}
