package com.mgm.config_builder.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {

	public List<String> getTempaltesPaths(String path) {
		List<String> templatesPaths = new ArrayList<String>();
		File templateFolder = new File(path);
		if (templateFolder.isDirectory()) {
			for (File file : templateFolder.listFiles()) {
				if (file.isFile() && file.getName().contains(DefaultValues.TEMPLATE_FILE_NAME_PATTERN)) {
					templatesPaths.add(file.getAbsolutePath());
				}
			}
		}
		return templatesPaths;
	}
	
	public static String getLocationPath(){
		URL location = FileOperations.class.getProtectionDomain().getCodeSource().getLocation();
		String filePath = location.getFile();
		String folderPath = filePath.substring(0, filePath.indexOf("cbmf"));
        return folderPath;
	}

	public List<String> readFile(String filePath) throws IOException {
		List<String> fileLines = new ArrayList<String>();
		File file = new File(filePath);
		BufferedReader br = new BufferedReader(new FileReader(file));

		String line = null;
		while ((line = br.readLine()) != null) {
			fileLines.add(line);
		}

		br.close();
		return fileLines;
	}

	public void writeFile(String filePath, List<String> lines) throws IOException {
		FileOutputStream outputStream = new FileOutputStream(filePath);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
		BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

		for (String line : lines) {
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		}

		bufferedWriter.close();
	}

	public void createFolderIfNotExist(String folderPath) {
		File folder = new File(folderPath);
		if (!folder.exists()) {
			folder.mkdirs();
		}
	}

	public String getTargetFilePath(String templatFilePath, String targetFolderPath) {

		File file = new File(templatFilePath);
		String targetFileName = file.getName().replaceAll(DefaultValues.TEMPLATE_FILE_NAME_PATTERN, "");
		return Paths.get(targetFolderPath, targetFileName).toString();
	}
}
