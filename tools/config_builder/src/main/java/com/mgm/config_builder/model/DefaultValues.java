package com.mgm.config_builder.model;

public class DefaultValues {
	
	public static final String PROPERTIES_FILE_PATH = "src/main/resources/config.properties";
	public static final String TARGET_CONFIG_FOLDER_PATH = "config-files/";
	public static final String TEMPLATE_SOURCE_CONFIG_FOLDER_PATH = "../templates/benchmark_config/";
	
	public static final String TEMPLATE_FILE_NAME_PATTERN = "-template";
	public static final int NUMBER_OF_COPIES = 1;
	public static final String REPLACE_COPYNUMBER_STRING = "1";
	
}
