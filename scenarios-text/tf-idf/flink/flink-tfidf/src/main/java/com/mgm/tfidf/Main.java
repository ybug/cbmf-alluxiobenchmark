package com.mgm.tfidf;

import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.util.Collector;

/**
 * tf-idf for flink
 *
 */
public class Main {

	public static void main(String[] args) throws Exception {
		final ParameterTool params = ParameterTool.fromArgs(args);
		String inputPath = params.get("input", null);
		String outputFolderPath = params.get("output", null);

		if (inputPath == null || outputFolderPath == null) {
			System.err.println("input or output not set in the arguments!");
			return;
		}

		// set up the execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		// read text file
		DataSet<String> text = env.readTextFile(inputPath);

		// split text -> (document, word, 1)
		DataSet<Tuple3<String, String, Integer>> docWordNumbers = text.flatMap(new FlatMapFunction<String, Tuple3<String, String, Integer>>() {
			private static final long serialVersionUID = 1L;

			@Override
			public void flatMap(String line, Collector<Tuple3<String, String, Integer>> out) {
				// document name is on the end of the line in this format -> <document name>
				String[] lineElements = line.split("<");
				if (lineElements.length >= 2 && !lineElements[0].equals("")) {
					String docID = lineElements[1].replace(">", ""); // clean the document name
					String docContent = lineElements[0].toLowerCase().replaceAll("[^a-zA-Z ]", "");// clean the text and remove unneeded charts
					for (String word : docContent.split(" ")) { // split text to words
						if (!word.equals("")) {
							out.collect(new Tuple3<String, String, Integer>(docID, word, 1));
						}
					}
				}
			}
		});
		// number of documents -> for idf (number of documents)
		Long docNumber = docWordNumbers.map(new MapFunction<Tuple3<String, String, Integer>, String>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String map(Tuple3<String, String, Integer> docWordNumber) {
				return docWordNumber.f0;

			}
		}).distinct().count();

		// number of words in document -> to normalize tf (document, number of words)
		DataSet<Tuple2<String, Integer>> numberOfWordsInDocuments = docWordNumbers
				.map(new MapFunction<Tuple3<String, String, Integer>, Tuple2<String, Integer>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple2<String, Integer> map(Tuple3<String, String, Integer> docWordNumber) {
						return new Tuple2<String, Integer>(docWordNumber.f0, docWordNumber.f2);

					}
				}).groupBy(0).sum(1);

		// tf -> (document, word, tf) tf = (number of terms in document) / (number of all words in document)
		DataSet<Tuple3<String, String, Double>> tf = docWordNumbers.groupBy(0, 1).sum(2).join(numberOfWordsInDocuments).where(0).equalTo(0)
				.map(new MapFunction<Tuple2<Tuple3<String, String, Integer>, Tuple2<String, Integer>>, Tuple3<String, String, Double>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Tuple3<String, String, Double> map(Tuple2<Tuple3<String, String, Integer>, Tuple2<String, Integer>> input) {
						Tuple3<String, String, Integer> docWordNumber = input.f0;
						Tuple2<String, Integer> numberOfWordsInDocument = input.f1;
						Double tf = ((double) docWordNumber.f2 / (double) numberOfWordsInDocument.f1);
						return new Tuple3<String, String, Double>(docWordNumber.f0, docWordNumber.f1, tf);
					}
				});

		// idf -> (word, idf) idf = 1+log_e( document number in corpus / number of documents contains word)
		DataSet<Tuple2<String, Double>> idf = docWordNumbers.map(new MapFunction<Tuple3<String, String, Integer>, Tuple2<String, String>>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Tuple2<String, String> map(Tuple3<String, String, Integer> input) {
				// document,word
				return new Tuple2<String, String>(input.f0, input.f1);
			}
		}).distinct().map(new MapFunction<Tuple2<String, String>, Tuple2<String, Integer>>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Tuple2<String, Integer> map(Tuple2<String, String> input) {
				// word,1
				return new Tuple2<String, Integer>(input.f1, 1);
			}
		}).groupBy(0).sum(1).map(new IdfComputer(docNumber));

		// tf-idf -> (document, word, tf-idf) -> tf-idf = tf * idf
		/*
		 * DataSet<Tuple3<String, String, Double>> tfidf = tf.join(idf).where(1).equalTo(0).map( new MapFunction<Tuple2<Tuple3<String, String,
		 * Double>, Tuple2<String, Double>>, Tuple3<String, String, Double>>() { private static final long serialVersionUID = 1L;
		 * 
		 * @Override public Tuple3<String, String, Double> map( Tuple2<Tuple3<String, String, Double>, Tuple2<String, Double>> value) throws Exception
		 * {
		 * 
		 * Tuple3<String, String, Double> tf = value.f0; Tuple2<String, Double> idf = value.f1; 
		 * return new Tuple3<String, String, Double>(tf.f0,
		 * tf.f1, tf.f2 * idf.f1); }
		 * 
		 * });//.sortPartition(0, Order.ASCENDING);
		 */
		DataSet<Tuple3<String, String, Double>> tfidf = tf.leftOuterJoin(idf).where(1).equalTo(0)
				.with(new FlatJoinFunction<Tuple3<String, String, Double>, Tuple2<String, Double>, Tuple3<String, String, Double>>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void join(Tuple3<String, String, Double> tf, Tuple2<String, Double> idf,
							Collector<Tuple3<String, String, Double>> out) throws Exception {
						if(idf != null){
							out.collect(new Tuple3<String, String, Double>(tf.f0, tf.f1, tf.f2 * idf.f1));
						}
					}
				});

		// write out results in csv file
		tfidf.map(new MapFunction<Tuple3<String, String, Double>, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String map(Tuple3<String, String, Double> value) throws Exception {
				return value.f0 + ", " + value.f1 + ", " + Double.toString(value.f2);
			}

		}).writeAsText(outputFolderPath);

		// execute program
		env.execute("Flink TF-IDF");
	}

	/**
	 * compute idf
	 * 
	 * @author bugge
	 * @since 10.05.2016
	 */
	public static class IdfComputer implements MapFunction<Tuple2<String, Integer>, Tuple2<String, Double>> {
		private static final long serialVersionUID = 1L;

		private Long numberOfDocuments = 0L;

		public IdfComputer() {

		}

		/**
		 * 
		 * @param numberOfDocuments
		 *            ->is the number of documents in corpus
		 */
		public IdfComputer(Long numberOfDocuments) {
			this.numberOfDocuments = numberOfDocuments;
		}

		@Override
		public Tuple2<String, Double> map(Tuple2<String, Integer> value) throws Exception {
			if (numberOfDocuments == 0L) {
				return new Tuple2<String, Double>(value.f0, 0.0);
			}
			return new Tuple2<String, Double>(value.f0, idf(value.f1, numberOfDocuments));
		}

		/**
		 * compute idf like 1+log_e( document number in corpus / number of documents contains word)
		 * 
		 * @param numberOFDocumentsContainsWord
		 *            -> document number in corpus
		 * @param maxNumberDocuments
		 *            -> number of documents contains word
		 * @return idf
		 */
		private Double idf(Integer numberOFDocumentsContainsWord, Long maxNumberDocuments) {
			if (numberOFDocumentsContainsWord > 0) {
				return new Double(1 + Math.log((double) maxNumberDocuments / (double) numberOFDocumentsContainsWord));
			} else {
				return new Double(1);
			}
		}
	}
}
