package com.mgm.benchmark_evaluator.models;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultAtomElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;
import com.opencsv.CSVWriter;

public class CsvStandardDevicionIT3 {

	private final String resultFolderPath;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(CsvStandardDevicionIT3.class.getName());

	public CsvStandardDevicionIT3(String benchmarkName, String resultFolderPath, String iterationName, String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileName = fileName.split("_")[0];
		this.computerName = computerName;
		this.iterationName = iterationName;
		this.benchmarkName = benchmarkName;
	}

	public void writeCSV(Iteration3ResultsElement results) {

		createFolder();
		CSVWriter csvWriter;

		try {
			csvWriter = new CSVWriter(new FileWriter(getOutputPath()), DefaultValues.CSV_SEPERATOR, CSVWriter.NO_QUOTE_CHARACTER);
			List<String[]> data = getSDs(results.getResults());
			csvWriter.writeAll(data);
			csvWriter.close();
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantWriteFile(getOutputPath()), e);
			e.printStackTrace();
		}
	}

	private String getOutputPath() {
		String scenario = "_" + getTitleScenario().replace(" ", "_");
		return Paths.get(getOutputFolderPath(), fileName + DefaultValues.STANDARDDEVISION_FILES + scenario + DefaultValues.CSV_ENDING).toString();
	}

	private String getTitleScenario() {

		String[] benchmarkNameSplits = benchmarkName.split("_");
		String title = StringTemplates.getUpperFirstChar(benchmarkNameSplits[0]) + " " + StringTemplates.getAlgorithm(benchmarkNameSplits[1]) + "_";
		if (computerName.equals(DefaultValues.COMPUTER_SUMMARYNAME)) {
			String chartName = getSummaryScenarioTitle();
			return title + chartName;
		}
		return title + computerName;
	}

	private String getSummaryScenarioTitle() {
		String chartName = "";
		if (fileName.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))) {
			chartName = DefaultValues.TIME_CHARTTITLE;
		} else if (fileName.equals(Integer.toString(DefaultValues.PERFORMANCE_DEFAULTINDX))) {
			chartName = DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}

	private void createFolder() {
		FileFolderOperations.createFolderIfNotExist(getOutputFolderPath());
	}

	private String getOutputFolderPath() {
		return Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
	}

	private List<String[]> getSDs(Map<Long, Iteration3ResultElement> elements) {

		List<String> technologies = getTechnologies(elements);
		List<String[]> csvContent = getHeadLine(technologies);
		
		csvContent.addAll(getSD(elements,technologies));
		return csvContent;
	}
	
	
	private List<String[]> getSD(Map<Long, Iteration3ResultElement> elements,
			List<String> technologies) {
		List<String[]> cells = new ArrayList<String[]>();
		for (Long elementScaleFactor : getSortedScaleFactor(elements)) {

			String[] row = new String[technologies.size() + 1]; // +1 -> Area for scale factor
			row[0] = Long.toString(elementScaleFactor); //set scale factor on row 1
			int rowIndex = 1;
			for(String technology : technologies){
				Iteration3ResultElement element = elements.get(elementScaleFactor);
				if(element.getElements().containsKey(technology)){
					row[rowIndex] = element.getElements().get(technology).getStandardDeviationAsStringInPercent();
				}else{
					row[rowIndex] = "-";
				}
				
				rowIndex++;
			}
			cells.add(row);
		}

		return cells;
	}

	private List<String[]> getHeadLine(List<String> technologies) {
		List<String[]> headLine = new ArrayList<String[]>();
		String[] headLineStrings = new String[technologies.size() + 1];
		headLineStrings[0] = DefaultValues.R_SCALEFACTOR_TITLE;
		for (int index = 0; index < technologies.size(); index++) {
			headLineStrings[(index + 1)] = StringTemplates.getTestCaseTitle(technologies.get(index));
		}
		headLine.add(headLineStrings);
		return headLine;
	}

	private List<Long> getSortedScaleFactor(Map<Long, Iteration3ResultElement> elements) {
		List<Long> sortedKeys = new ArrayList<Long>(elements.keySet());
		Collections.sort(sortedKeys);
		return sortedKeys;
	}

	private List<String> getTechnologies(Map<Long, Iteration3ResultElement> elements) {
		List<String> technologies = new ArrayList<String>();
		for (Entry<Long, Iteration3ResultElement> element : elements.entrySet()) {
			for (Entry<String, Iteration3ResultAtomElement> t_element : element.getValue().getElements().entrySet()) {
				if (!technologies.contains(t_element.getKey())) {
					technologies.add(t_element.getKey());
				}
			}
		}
		return technologies;
	}
}