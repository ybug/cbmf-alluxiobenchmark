package com.mgm.config_builder.elements;

import java.util.HashMap;
import java.util.Map;

import com.mgm.config_builder.model.DefaultValues;

public class PropertiesElement {
	
	private Map<String,String> properties = new HashMap<String,String>();
	private String propertiesPath = null;
	private int numberOfCopies = DefaultValues.NUMBER_OF_COPIES;
	private String replaceCopyNumberString = DefaultValues.REPLACE_COPYNUMBER_STRING;
	private String targetFolderPath = DefaultValues.TARGET_CONFIG_FOLDER_PATH;
	private String templateFolderPath = DefaultValues.TEMPLATE_SOURCE_CONFIG_FOLDER_PATH;

	public PropertiesElement(String propertiesPath) {
		this.propertiesPath = propertiesPath;
	}

	public String getPropertiesPath() {
		return propertiesPath;
	}

	public String getTargetFolderPath() {
		return targetFolderPath;
	}
	
	public void setTargetFolderPath(String targetFolderPath) {
		this.targetFolderPath = targetFolderPath;
	}

	public String getTemplateFolderPath() {
		return templateFolderPath;
	}

	public void setTemplateFolderPath(String templateFolderPath) {
		this.templateFolderPath = templateFolderPath;
	}
	
	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
	
	public void addProperty(String key, String value){
		this.properties.put("<"+key+">", value);
	}
	
	public int getNumberOfCopies() {
		return numberOfCopies;
	}

	public void setNumberOfCopies(int numberOfCopies) {
		this.numberOfCopies = numberOfCopies;
	}
	
	public String getReplaceCopyNumberString() {
		return replaceCopyNumberString;
	}

	public void setReplaceCopyNumberString(String replaceCopyNumberString) {
		this.replaceCopyNumberString = replaceCopyNumberString;
	}

	@Override
	public String toString() {
		return "PropertiesElement [properties=" + properties + ", propertiesPath=" + propertiesPath + ", numberOfCopies=" + numberOfCopies
				+ ", replaceCopyNumberString=" + replaceCopyNumberString + ", targetFolderPath=" + targetFolderPath + ", templateFolderPath="
				+ templateFolderPath + "]";
	}
}
