package com.mgm.benchmark_evaluator.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

public abstract class CommandLineExecutor {
	
	private final static Logger LOGGER = Logger.getLogger(CommandLineExecutor.class.getName());
	
	private Timestamp startTimeStamp = null;
	private Timestamp endTimeStamp = null;
	
	public abstract String getCommand();
	public abstract String getErrorMessage();
	public abstract String getInfoMessage();
	public abstract boolean isLogExecuteOutput();
	
	public CommandLineExecutor(){
		
	}
	
	public boolean run(){
		Process process;
		boolean executeInCommandError = false;
		
		try {
			startTimeStamp = new Timestamp(new Date().getTime());
			process = Runtime.getRuntime().exec(getCommand());
			logOutputFromCommandInputStream(process);
			executeInCommandError = logOutputFromCommandErrorStream(process);
			//logOutputFromCommandErrorStream(process);
			process.waitFor();
			endTimeStamp = new Timestamp(new Date().getTime());
			if(process.exitValue() != 0){
				executeInCommandError = true;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			LOGGER.error(getErrorMessage());
			return true;
		}
		if(executeInCommandError){
			LOGGER.error(getErrorMessage());
			return true;
		}
		//LOGGER.info(getInfoMessage());
		return executeInCommandError;
	}
	
	/**
	 * write in the log the output from the execute command
	 * (only input stream) 
	 * 
	 * @param process
	 *            -> is the process from the execute command
	 * @throws IOException
	 */
	private void logOutputFromCommandInputStream(Process process) throws IOException {
		if(isLogExecuteOutput()){
			//LOGGER.info(StringTemplates.ouptutCommandLineTitleInputStream());
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			
			while ((reader.readLine()) != null) {
				//LOGGER.info(StringTemplates.outputCommandLineExecutorLoggerInputStream(line));
			}
		}
	}
	
	/**
	 * write in the log the output from the execute command
	 * (only error stream) 
	 * 
	 * @param process
	 *            -> is the process from the execute command
	 * @return if true, then occur error
	 * @throws IOException
	 */
	private boolean logOutputFromCommandErrorStream(Process process) throws IOException {
		if(isLogExecuteOutput()){
			//LOGGER.info(StringTemplates.ouptutCommandLineTitleErrorStream());
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		String line = "";
		boolean error = false;
		while ((line = reader.readLine()) != null) {
			if(isLogExecuteOutput()){
				LOGGER.error(StringTemplates.outputCommandLineExecutorLoggerErrorStream(line));
			}
			if(!line.startsWith("SLF4J:")){
				error = true;
			}
		}
		return error;
	}
	
	public Timestamp getStartTimeStamp() {
		return startTimeStamp;
	}
	public Timestamp getEndTimeStamp() {
		return endTimeStamp;
	}
}
