package com.mgm.benchmark_evaluator.elements.iteration3;

import java.util.HashMap;
import java.util.Map;

public class Iteration3BenchmarkResultsElement {
	
	private Map<String,Iteration3ScenarioResultsElement> benchmarks = new HashMap<String,Iteration3ScenarioResultsElement>();

	public Map<String, Iteration3ScenarioResultsElement> getBenchmarks() {
		return benchmarks;
	}

	public void setBenchmarks(Map<String, Iteration3ScenarioResultsElement> benchmarks) {
		this.benchmarks = benchmarks;
	}
	
	
}
