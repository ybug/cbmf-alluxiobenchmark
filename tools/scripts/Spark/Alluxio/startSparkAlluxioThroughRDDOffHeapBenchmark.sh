#!/bin/bash

echo "#"
echo "### Spark AlluxioThroughRDDOffHeap Wordcount"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapWordcountBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapWordcountBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapWordcountBenchmark.sh _part3
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapWordcountBenchmark.sh _part4
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioThroughRDDOffHeap Copy"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapCopyBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapCopyBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapCopyBenchmark.sh _part3
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapCopyBenchmark.sh _part4
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioThroughRDDOffHeap Terasort"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTeraBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTeraBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTeraBenchmark.sh _part3
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTeraBenchmark.sh _part4
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioThroughRDDOffHeap TF-IDF"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTfidfBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTfidfBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTfidfBenchmark.sh _part3
#sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTfidfBenchmark.sh _part4
#sh Spark/Alluxio/Through/startSparkAlluxioThroughRDDOffHeapTfidfBenchmark.sh _part5
