package com.mgm.spark_terasort;

import java.io.IOException;
import java.security.PrivilegedExceptionAction;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.mgm.terasort.extern.eastcirclek.SparkTeraRangePartitioner;
import com.mgm.terasort.extern.eastcirclek.TeraInputFormat;
import com.mgm.terasort.extern.eastcirclek.TeraOutputFormat;
import com.mgm.terasort.extern.eastcirclek.TotalOrderPartitioner;

import org.apache.hadoop.io.Text;

/**
 * terasort implement in Spark 
 * @author bugge
 * @since 08.06.2016
 */
public class Main {
	private static final String APP_NAME = "com.mgm.spark-terasort";
	private static JavaSparkContext context;

	public static void main(String[] args) throws Throwable {
		Arguments arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);

		JobConf mapRedConf = new JobConf();
		if(!arguments.getHdfs().equals("")){
			mapRedConf.set("fs.defaultFS", arguments.getHdfs());	
		}
		mapRedConf.set("mapreduce.input.fileinputformat.inputdir", arguments.getInputPath());
		mapRedConf.set("mapreduce.output.fileoutputformat.outputdir", arguments.getOutputPath());
		mapRedConf.setInt("mapreduce.job.reduces", arguments.getPartitionNumber());
		mapRedConf.set("hadoop.job.ugi", arguments.getFsUser());
		
		Path partitionFile = new Path(arguments.getOutputPath(), TeraInputFormat.PARTITION_FILENAME);
		Job jobContext = Job.getInstance(mapRedConf);
		
		writePartitions(arguments.getFsUser(),partitionFile, jobContext);
		
		@SuppressWarnings("rawtypes")
		Class[] registerKryoClasses = new Class[1];
		registerKryoClasses[0] = Text.class;
		
		StorageLevel storageLevel = StorageLevel.MEMORY_ONLY();
		if(arguments.isOffHeap()){
			storageLevel = StorageLevel.OFF_HEAP();
    	}
		
		SparkConf conf = new SparkConf().setAppName(APP_NAME).registerKryoClasses(registerKryoClasses);
		context = new JavaSparkContext(conf);
		
		JavaPairRDD<Text, Text> inputRDD = context.
		newAPIHadoopFile(arguments.getInputPath(), TeraInputFormat.class, Text.class, Text.class, mapRedConf);
		SparkTeraRangePartitioner partitioner = new SparkTeraRangePartitioner(new TotalOrderPartitioner(mapRedConf, partitionFile),
		          arguments.getPartitionNumber());
		inputRDD.repartitionAndSortWithinPartitions(partitioner).persist(storageLevel)
		.saveAsNewAPIHadoopFile(arguments.getOutputPath(), Text.class, Text.class, TeraOutputFormat.class);
	}
	

	public static void writePartitions(String fileSystemUser,Path partitionFile,Job jobContext) throws IOException, InterruptedException{
		UserGroupInformation ugi = UserGroupInformation.createRemoteUser(fileSystemUser);
		ugi.doAs(new PartitionWriter(partitionFile,jobContext));
	}
	
	public static class PartitionWriter implements PrivilegedExceptionAction<Void>{

		public static Logger LOG = LoggerFactory.getLogger(PartitionWriter.class);
				
		private final Path partitionFile;
		private final Job jobContext;
		
		public PartitionWriter(Path partitionFile,Job jobContext) {
			this.jobContext=jobContext;
			this.partitionFile=partitionFile;
		}
		
		public Void run() throws Exception {
			try {
				TeraInputFormat.writePartitionFile(jobContext, partitionFile);
			} catch (Throwable e) {
				LOG.error("An {} occurred.", "error", e);
			}
			return null;
		}
	}
}
