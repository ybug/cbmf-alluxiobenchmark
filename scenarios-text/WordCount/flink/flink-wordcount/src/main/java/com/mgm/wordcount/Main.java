package com.mgm.wordcount;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.util.Collector;

/**
 * word counter with flink 
 *
 */
public class Main {
	
	public static void main(String[] args) throws Exception {
		final ParameterTool params = ParameterTool.fromArgs(args);
		String inputPath = params.get("input",null);
		String outputFolderPath = params.get("output", null);
		
		if (inputPath == null || outputFolderPath == null) {
			System.err.println("input or output not set in the arguments!");
			return;
		}
		
		// set up the execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

		DataSet<String> text = env.readTextFile(inputPath);
		text
		.map(new MapFunction<String, String>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String map(String input) throws Exception {

				return input.toLowerCase().replaceAll("[^a-zA-Z ]", "");
			}
		})
		.flatMap(new FlatMapFunction<String, Tuple2<String, Integer>>() {
			private static final long serialVersionUID = 1L;

			@Override
			public void flatMap(String line, Collector<Tuple2<String, Integer>> out) {
				for (String word : line.split(" ")) {
					if(!word.equals("")){
						out.collect(new Tuple2<String, Integer>(word, 1));
					}
				}
			}
		})
		.groupBy(0)
		.sum(1)
		.map(new MapFunction<Tuple2<String,Integer>,String>(){

			private static final long serialVersionUID = 1L;

			@Override
			public String map(Tuple2<String, Integer> value) throws Exception {
				return value.f0+","+value.f1.toString();
			}
			
		})
		.writeAsText(outputFolderPath);

		// execute program
		env.execute("Flink WordCount");
	}
}
