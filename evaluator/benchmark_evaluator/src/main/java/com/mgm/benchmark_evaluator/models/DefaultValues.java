package com.mgm.benchmark_evaluator.models;

public class DefaultValues {

	public static final String INPUT_PATH = "../input/";
	
	public static final String OUTPUT_PATH = "../output/";
	
	
	public static final String INPUTFILENAME_RESULTJSON = "result.json";
	public static final String INPUTFILENAME_CONFIGNAME = "config";
	public static final String INPUTFILENAME_CONFIGENDIG = ".properties";
	
	public static final String OUTPUTFOLDERNAME_ITERATION1 = "base";
	
	public static final char CSV_SEPERATOR = ';';
	public static final String CSV_ENDING = ".csv";
	public static final String PDF_ENDING = ".pdf";
	public static final String R_ENDING = ".R";
	
	public static final String MIN_MAX_FILES = "_min_max";
	public static final String MEAN_FILES = "_mean";
	public static final String ROUND_FILES = "_round";
	public static final String QUALITYROUND_FILES = "_qualityround";
	public static final String MERGE_FILES = "_merge";
	public static final String RATIO_FILES = "_ratio";
	public static final String STANDARDDEVISION_FILES = "_sd";
	
	public static final String SUMMARY_FOLDERNAME = "summary";
	public static final String TIME_FILENAME = "time";
	public static final String PERFORMANCE_FILENAME ="performance";
	
	public static final String SCALFACTOR_KEY = "scale_factor";
	public static final String COMPUTER_SUMMARYNAME = "Summary";
	public static final String TIME_UNIT = "s";
	public static final String PERFORMANCE_UNIT = "1/min";
	public static final int TIME_DEFAULTINDX = -1;
	public static final int PERFORMANCE_DEFAULTINDX = -2;
	
	public static final String PERFORMANCE_CHARTTITLE = "Performance";
	public static final String TIME_CHARTTITLE = "Runtime";
	
	public static final String ITERATION1_NAME = "iteration_1";
	public static final String ITERATION2_NAME = "iteration_2";
	public static final String ITERATION3_NAME = "iteration_3";
	public static final String ITERATION4_NAME = "iteration_4";
	
	public static final String BENCHMARK_PART_NAME1 = "test";
	public static final String BENCHMARK_PART_NAME2 = "part";
	
	public static final String UNIT_PATTERN1 = "$1";
	public static final String UNIT_PATTERN2 = "$2";
	public static final String UNIT_PERCENT = "%";
	
	public static final String MASTER_NAME1 = "Zabbix server";
	public static final String MASTER_NAME2 = "Master";
	public static final String WORKER_NAME = "Worker";
	
	public static final String CSV_HEADLINE_STANDARD_DEVISION_PERCENT = "Standard Devision$2";
	
	public static final String R_PLOT1_TEMPLATE_MINMAX_PATH = "plot1.R_template";
	public static final String R_PLOT1_TEMPLATE_MEAN_PATH = "plot2.R_template";
	public static final String R_PLOT3_TEMPLATE_PATH = "plot3.R_template";
	public static final String R_PLOT3COLOR_TEMPLATE_PATH = "plot3_color.R_template";
	public static final String R_PLOT4_TEMPLATE_PATH = "plot4.R_template";
	public static final String R_PLOT4COLOR_TEMPLATE_PATH = "plot4_color.R_template";
	public static final String R_SCALEFACTOR_TITLE = "scale factor";
	public static final String R_MEAN_TITLE = "mean";
	public static final String R_MIN_TITLE = "min";
	public static final String R_MAX_TITLE = "max";
	public static final String R_TEMPFOLDERNAME = "temp";
	public static final String R_COMMAND = "Rscript ";
	
	public static final int R_MAX_COLUMNNUMBER = 4;
	
	public static final String QUALITY_PLOT_UNIT = "%";
	public static final String QUALITY_PLOT_TITLE = "Quality";
	
	public static final String REF_TECHNOLOGY = "hdfs";
	
	public static final String[] ALG_SEQ = new String[]{"wc","cp","tera","tfidf"};
}
