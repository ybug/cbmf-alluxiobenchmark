package com.mgm.spark_tfidf;

import com.github.jankroken.commandline.annotations.LongSwitch;
import com.github.jankroken.commandline.annotations.Option;
import com.github.jankroken.commandline.annotations.Required;
import com.github.jankroken.commandline.annotations.ShortSwitch;
import com.github.jankroken.commandline.annotations.SingleArgument;
import com.github.jankroken.commandline.annotations.Toggle;

public class Arguments {
	private String inputPath = null;
	private String outputPath = null;
	private boolean offHeap = false;

	public String getInputPath() {
		return inputPath;
	}

	@Option
	@LongSwitch("input")
	@ShortSwitch("i")
	@SingleArgument
	@Required
	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
	}

	public String getOutputPath() {
		return outputPath;
	}

	@Option
	@LongSwitch("output")
	@ShortSwitch("o")
	@SingleArgument
	@Required
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}
	
	public boolean isOffHeap() {
		return offHeap;
	}
	
	@Option
    @LongSwitch("offheap")
    @ShortSwitch("oh")
    @Toggle(true)
	public void setOffHeap(boolean offHeap) {
		this.offHeap = offHeap;
	}
}
