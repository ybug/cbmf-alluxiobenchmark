#!/bin/bash
echo "####################################"
echo "############# CONFIG ###############"
echo "####################################"
echo ""
echo "#### set configuration ####"
echo ""
vim tools/config_builder/src/main/resources/config.properties

echo ""
echo "#### build configurations ####"
echo ""
cd tools/config_builder/
java -jar target/config_builder-0.0.1-SNAPSHOT-jar-with-dependencies.jar -i ../templates/benchmark_config/ -o ../../../cbmf/src/main/resources/
java -jar target/config_builder-0.0.1-SNAPSHOT-jar-with-dependencies.jar -i ../templates/opertions/ -o ../scripts/
java -jar target/config_builder-0.0.1-SNAPSHOT-jar-with-dependencies.jar -i ../templates/generator_skript/ -o ../../
cd ../../
