package com.mgm.spark_cp;

import java.lang.reflect.InvocationTargetException;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.storage.StorageLevel;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;


/**
 * copy in spark
 * @author bugge
 * @since 07.06.2016
 */
public class Main 
{
	
	private static final String APP_NAME = "com.mgm.spark-copy";
	private static JavaSparkContext context;
	
    public static void main( String[] args ) throws IllegalAccessException, InstantiationException, InvocationTargetException
    {
    	Arguments arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
    	
    	StorageLevel storageLevel = StorageLevel.MEMORY_ONLY();
		if(arguments.isOffHeap()){
			storageLevel = StorageLevel.OFF_HEAP();
    	}
    	
    	SparkConf conf = new SparkConf().setAppName(APP_NAME);
		context = new JavaSparkContext(conf);
		context.textFile(arguments.getInputPath()).persist(storageLevel).saveAsTextFile(arguments.getOutputPath());
    }
}
