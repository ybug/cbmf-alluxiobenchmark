package com.mgm.spark_unsorted_evaluation;

import java.lang.reflect.InvocationTargetException;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.google.common.base.Optional;

import scala.Tuple2;

/**
 * Hello world!
 *
 */
public class Main 
{
	private static final String APP_NAME = "com.mgm.spark-unsorted-evaluation";
	private static JavaSparkContext context;
	
	public final static boolean EQUAL = true;
	public final static boolean NOT_EQUAL = false;
	public final static String EQUAL_TEXT = "Equal:";
	public final static String NOT_EQUAL_TEXT = "Not Equal:";
	
    public static void main( String[] args ) throws IllegalAccessException, InstantiationException, InvocationTargetException
    {
    	Arguments arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
    	SparkConf conf = new SparkConf().setAppName(APP_NAME);
    	context = new JavaSparkContext(conf);
    	
    	JavaRDD<String> referenceDataSet = context.textFile(arguments.getInputRefPath());
    	JavaRDD<String> checkDataSet = context.textFile(arguments.getInputRefPath());
    	
    	JavaPairRDD<String, Long> compressReferenceDataSet = referenceDataSet.mapToPair(new TransformToCompressDataSet()).reduceByKey(new CompressDataSetInteger());
    	JavaPairRDD<String, Long> compressCheckDataSet = checkDataSet.mapToPair(new TransformToCompressDataSet()).reduceByKey(new CompressDataSetInteger());
    	
    	compressReferenceDataSet
    	.fullOuterJoin(compressCheckDataSet)
    	.mapToPair(new PairFunction<Tuple2<String,Tuple2<Optional<Long>,Optional<Long>>>, Boolean,Long>() {

			private static final long serialVersionUID = 1L;

			public Tuple2<Boolean,Long> call(Tuple2<String, Tuple2<Optional<Long>, Optional<Long>>> v1) throws Exception {
				
				Boolean isEqual = EQUAL;
				Long first = v1._2._1.get();
				Long second = v1._2._1.get();
				if(second == null || first == null){
					isEqual = NOT_EQUAL;
				}
				if (!first .equals(second)) {
					isEqual =  NOT_EQUAL;
				}
				return new Tuple2<Boolean,Long>(isEqual,1L);
			}
		})
    	.reduceByKey(new CompressDataSetLong())
    	.map(new Function<Tuple2<Boolean,Long>,String>(){

			private static final long serialVersionUID = 1L;

			public String call(Tuple2<Boolean, Long> value) throws Exception {
				if(value._1() == EQUAL){
					return EQUAL_TEXT+Long.toString(value._2());
				}else {
					return NOT_EQUAL_TEXT+Long.toString(value._2());
				}
			}
    		
    	})
    	.saveAsTextFile(arguments.getOutputPath());
    
    }
    
    public static class TransformToCompressDataSet implements PairFunction<String,String,Long>{
    	private static final long serialVersionUID = 1L;
		//convert Tuple2<String,Integer> to JavaPairRDD<String,Integer> 
		public Tuple2<String, Long> call(String value) throws Exception {
			return new Tuple2<String, Long>(value, 1L);
		}
    }
    
    public static class CompressDataSetInteger implements Function2<Long,Long,Long>{
    	private static final long serialVersionUID = 1L;
		//add all occurrence from each word (add all values from same key)
		public Long call(Long v1, Long v2) throws Exception {
			return v1+v2;
		}
    }
    
    public static class CompressDataSetLong implements Function2<Long,Long,Long>{
    	private static final long serialVersionUID = 1L;
		//add all occurrence from each word (add all values from same key)
		public Long call(Long v1, Long v2) throws Exception {
			return v1+v2;
		}
    }
}
