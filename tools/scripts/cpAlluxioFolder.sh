#!/bin/bash

ALLUXIO_USER=$1
SOURCE_DIR=$2
TARGET_DIR=$3
ALLUXIO_DIR_EX=$4
echo $TARGET_DIR
if [[ "$SOURCE_DIR" == */ ]]
then
   SOURCE_DIR=${SOURCE_DIR%?}

fi

if [[ "$TARGET_DIR" == */ ]]
then
   TARGET_DIR=${TARGET_DIR%?}

fi

if [[ "$SOURCE_DIR" == hdfs://* ]]
then
  SOURCE_DIR_SEP_POS1=$(echo $SOURCE_DIR  | grep -b -o / | head -3 | tail -1 | cut -d':' -f 1)
  SOURCE_DIR=${SOURCE_DIR:$SOURCE_DIR_SEP_POS1} 

fi

if [[ "$TARGET_DIR" == alluxio://* ]]
then
  TARGET_DIR_SEP_POS1=$(echo $TARGET_DIR  | grep -b -o / | head -3 | tail -1 | cut -d':' -f 1)
  TARGET_DIR=${TARGET_DIR:$TARGET_DIR_SEP_POS1} 
fi

TARGET_DIR=/alluxio$TARGET_DIR

SOURCE_DIR_SEP_POS_temp=$(echo $SOURCE_DIR | grep -b -o / | tail -1 | cut -d'/' -f 1)
SOURCE_DIR_SEP_POS=$((${SOURCE_DIR_SEP_POS_temp%?}+1))

TARGET_DIR_SEP_POS_temp=$(echo $TARGET_DIR | grep -b -o / | tail -1| cut -d'/' -f 1)
TARGET_DIR_SEP_POS=$((${TARGET_DIR_SEP_POS_temp%?}+1))

SOURCE_DIR_NAME=$(echo $SOURCE_DIR | cut -c$SOURCE_DIR_SEP_POS- | cut -c2-)
SOURCE_DIR_PARENT=$(echo $SOURCE_DIR | cut -c-$SOURCE_DIR_SEP_POS) 

TARGET_DIR_NAME=$(echo $TARGET_DIR | cut -c$TARGET_DIR_SEP_POS- | cut -c2-)
TARGET_DIR_PARENT=$(echo $TARGET_DIR | cut -c-$TARGET_DIR_SEP_POS)

echo "mkdir: "$TARGET_DIR_PARENT
HADOOP_USER_NAME=$ALLUXIO_USER hdfs dfs -mkdir -p $TARGET_DIR_PARENT
echo ""
echo "cp: "$SOURCE_DIR" to-> "$TARGET_DIR_PARENT
HADOOP_USER_NAME=$ALLUXIO_USER hdfs dfs -cp $SOURCE_DIR $TARGET_DIR_PARENT
echo ""
echo "mv: "$TARGET_DIR_PARENT$SOURCE_DIR_NAME" -> "$TARGET_DIR
HADOOP_USER_NAME=$ALLUXIO_USER hdfs dfs -mv $TARGET_DIR_PARENT$SOURCE_DIR_NAME $TARGET_DIR


TARGET_DIR=${TARGET_DIR:8}
echo ""
echo "load "$ALLUXIO_DIR_EX" -> "$TARGET_DIR
$ALLUXIO_DIR_EX/bin/alluxio fs load $TARGET_DIR

