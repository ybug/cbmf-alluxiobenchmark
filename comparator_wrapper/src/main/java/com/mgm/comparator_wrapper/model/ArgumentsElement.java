package com.mgm.comparator_wrapper.model;


public class ArgumentsElement {
	
	public static final int MIN_ARGS_NUMBER = 6;
	
	private String refPath = null;
	private String resultPath = null;
	private String reportPath = null;
	private String command = null;
	private String hdfsUri = null;
	private String hdfsReportPath = null;
	
	public String getRefPath() {
		return refPath;
	}

	public void setRefPath(String refPath) {
		this.refPath = refPath;
	}
	public String getResultPath() {
		return resultPath;
	}

	public void setResultPath(String resultPath) {
		this.resultPath = resultPath;
	}
	public String getReportPath() {
		return reportPath;
	}

	public void setReportPath(String reportPath) {
		this.reportPath = reportPath;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
	
	public String getHdfsUri() {
		return hdfsUri;
	}

	public void setHdfsUri(String hdfsUri) {
		this.hdfsUri = hdfsUri;
	}

	public String getHdfsReportPath() {
		return hdfsReportPath;
	}

	public void setHdfsReportPath(String hdfsReportPath) {
		this.hdfsReportPath = hdfsReportPath;
	}

	public boolean isValid(){
		if(refPath == null || reportPath == null || resultPath == null || command == null || hdfsReportPath == null || hdfsUri == null){
			return false;
		}
		return true;
	}
}