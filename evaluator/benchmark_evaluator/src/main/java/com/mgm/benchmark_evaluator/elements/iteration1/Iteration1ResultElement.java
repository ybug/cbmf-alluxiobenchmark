package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.mgm.benchmark_evaluator.models.MathOperations;

public class Iteration1ResultElement implements Comparable<Iteration1ResultElement>{

	private double min;
	private double max;
	private double mean;
	private double standardDeviation;
	private long scaleFactor;
	private double qualityInPercent;
	private double qualityInPercentSTD;
	private List<Double> qualitiesInPercent = new ArrayList<Double>(); 
	private List<Double> elements = new ArrayList<Double>();
	
	
	public double getQualityInPercentSTD() {
		return qualityInPercentSTD;
	}

	public void setQualityInPercentSTD(double qualityInPercentSTD) {
		this.qualityInPercentSTD = qualityInPercentSTD;
	}

	public double getQualityInPercent() {
		return qualityInPercent;
	}

	public void setQualityInPercent(double qualityInPercent) {
		this.qualityInPercent = qualityInPercent;
	}

	public double computMeanQualityInPercentByElements(){
		MathOperations math = new MathOperations();
		for(Double quality : qualitiesInPercent){
			math.addDoubleValue(quality);
		}
		return math.getMean();
	}
	
	public double computStandadDeviationQualityInPercentByElements(){
		MathOperations math = new MathOperations();
		for(Double quality : qualitiesInPercent){
			math.addDoubleValue(quality);
		}
		return math.getStandardDeviation();
	}
	
	public double getMin() {
		return min;
	}
	public String getMinAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", min);
	}
	
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public String getMaxAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", max);
	}
	public void setMax(double max) {
		this.max = max;
	}
	public double getMean() {
		return mean;
	}
	public String getMeanAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", mean);
	}
	public void setMean(double mean) {
		this.mean = mean;
	}
	public double getStandardDeviation() {
		return standardDeviation;
	}
	public String getStandardDeviationAsString() {
		return String.format(Locale.GERMAN, "%1$,.4f", standardDeviation);
	}
	public String getStandardDeviationAsStringInPercent(){
		return String.format(Locale.GERMAN, "%1$,.4f",(((double) 100 / this.mean)*this.standardDeviation));
	}
	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}
	public long getScaleFactor() {
		return scaleFactor;
	}
	public String getScaleFactorAsString() {
		return Long.toString(scaleFactor);
	}
	public void setScaleFactor(long scaleFactor) {
		this.scaleFactor = scaleFactor;
	}
	public List<Double> getElements() {
		return elements;
	}
	
	public List<String> getQualitiesInPercentAsStrings(){
		List<String> elementsAsString = new ArrayList<String>();
		
		for(Double element: qualitiesInPercent){
			elementsAsString.add(String.format(Locale.GERMAN, "%1$,.4f", element));
		}
		
		return elementsAsString;
	} 
	
	public List<String> getElementsAsStrings(){
		List<String> elementsAsString = new ArrayList<String>();
		
		for(Double element: elements){
			elementsAsString.add(String.format(Locale.GERMAN, "%1$,.4f", element));
		}
		
		return elementsAsString;
	} 
	
	public void setElements(List<Double> elements) {
		this.elements = elements;
	}
	public boolean isEmpty(){
		return elements.isEmpty();
	}
	public List<Double> getQualitiesInPercent() {
		return qualitiesInPercent;
	}
	public void addQualityInPercent(double qualityInPercent) {
		this.qualitiesInPercent.add(qualityInPercent);
	}
	
	public void setQualitiesInPercent(List<Double> qualities){
		this.qualitiesInPercent = qualities;
		MathOperations math = new MathOperations();
		for(Double qualitie : qualities){
			math.addDoubleValue(qualitie);
		}
		setQualityInPercent(math.getMean());
		setQualityInPercentSTD(math.getStandardDeviation());
	}
	@Override
    public int compareTo(Iteration1ResultElement another) {
        if (this.getScaleFactor()<another.getScaleFactor()){
            return -1;
        }else{
            return 1;
        }
    }
}
