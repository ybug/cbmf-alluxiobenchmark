package com.mgm.wordcount;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.storage.StorageLevel;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;

import scala.Tuple2;

/**
 * word count with spark
 * @author bugge
 * @sicne 7.06.2016
 */
public class Main {

	private static final String APP_NAME = "com.mgm.spark-wordcounter";
	private static JavaSparkContext context;
	
	public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException {
		
		Arguments arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
		
		StorageLevel storageLevel = StorageLevel.MEMORY_ONLY();
		if(arguments.isOffHeap()){
			storageLevel = StorageLevel.OFF_HEAP();
    	}
		
		SparkConf conf = new SparkConf().setAppName(APP_NAME);
		context = new JavaSparkContext(conf);
		
		context.textFile(arguments.getInputPath()).persist(storageLevel)
		.map(new Function<String,String>(){

			private static final long serialVersionUID = 1L;
			//filter undesirable characters and set text to lowercase 
			public String call(String value) throws Exception {
				return value.toLowerCase().replaceAll("[^a-zA-Z ]", "");
			}
			
		}).persist(storageLevel)
		.flatMapToPair(new PairFlatMapFunction<String,String,Integer>(){

			private static final long serialVersionUID = 1L;
			//split line to words and delete empty words
			public Iterable<Tuple2<String,Integer>> call(String line) throws Exception {
				List<Tuple2<String,Integer>> out = new ArrayList<Tuple2<String,Integer>>();
				for (String word : line.split(" ")) {
					if(!word.equals("")){
						out.add(new Tuple2<String, Integer>(word, 1));
					}
				}
				return out;
			}
			
		}).persist(storageLevel)
		.reduceByKey(new Function2<Integer,Integer,Integer>(){

			private static final long serialVersionUID = 1L;
			//add all occurrence from each word (add all values from same key)
			public Integer call(Integer v1, Integer v2) throws Exception {
				return v1+v2;
			}
			
		}).persist(storageLevel)
		.map(new Function<Tuple2<String,Integer>,String>(){

			private static final long serialVersionUID = 1L;

			public String call(Tuple2<String, Integer> input) throws Exception {
				return input._1()+","+input._2().toString();
			}
			
		}).persist(storageLevel)
		.saveAsTextFile(arguments.getOutputPath());
		
	}
}
