#!/bin/bash

echo "#"
echo "### Flink Alluxio Only Wordcount"
echo "#"

sh Flink/Alluxio/Only/startFlinkAlluxioWordcountBenchmark.sh _part1
sh Flink/Alluxio/Only/startFlinkAlluxioWordcountBenchmark.sh _part2
sh Flink/Alluxio/Only/startFlinkAlluxioWordcountBenchmark.sh _part3
sh Flink/Alluxio/Only/startFlinkAlluxioWordcountBenchmark.sh _part4
sh Flink/Alluxio/Only/startFlinkAlluxioWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Only Copy"
echo "#"

sh Flink/Alluxio/Only/startFlinkAlluxioCopyBenchmark.sh _part1
sh Flink/Alluxio/Only/startFlinkAlluxioCopyBenchmark.sh _part2
sh Flink/Alluxio/Only/startFlinkAlluxioCopyBenchmark.sh _part3
sh Flink/Alluxio/Only/startFlinkAlluxioCopyBenchmark.sh _part4
sh Flink/Alluxio/Only/startFlinkAlluxioCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Only Terasort"
echo "#"

sh Flink/Alluxio/Only/startFlinkAlluxioTeraBenchmark.sh _part1
sh Flink/Alluxio/Only/startFlinkAlluxioTeraBenchmark.sh _part2
sh Flink/Alluxio/Only/startFlinkAlluxioTeraBenchmark.sh _part3
sh Flink/Alluxio/Only/startFlinkAlluxioTeraBenchmark.sh _part4
sh Flink/Alluxio/Only/startFlinkAlluxioTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Only TF-IDF"
echo "#"

sh Flink/Alluxio/Only/startFlinkAlluxioTfidfBenchmark.sh _part1
sh Flink/Alluxio/Only/startFlinkAlluxioTfidfBenchmark.sh _part2
sh Flink/Alluxio/Only/startFlinkAlluxioTfidfBenchmark.sh _part3
#sh Flink/Alluxio/Only/startFlinkAlluxioTfidfBenchmark.sh _part4
#sh Flink/Alluxio/Only/startFlinkAlluxioTfidfBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Only Grouping"
echo "#"

sh Flink/Alluxio/Only/startFlinkAlluxioGroupingBenchmark.sh _part1
sh Flink/Alluxio/Only/startFlinkAlluxioGroupingBenchmark.sh _part2
sh Flink/Alluxio/Only/startFlinkAlluxioGroupingBenchmark.sh _part3
sh Flink/Alluxio/Only/startFlinkAlluxioGroupingBenchmark.sh _part4
sh Flink/Alluxio/Only/startFlinkAlluxioGroupingBenchmark.sh _part5