package com.mgm.benchmark_evaluator.models;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultAtomElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;
import com.opencsv.CSVWriter;

public class CsvRatioIT3 {

	private final String resultFolderPath;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(CsvRatioIT3.class.getName());

	public CsvRatioIT3(String benchmarkName, String resultFolderPath, String iterationName, String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileName = fileName.split("_")[0];
		this.computerName = computerName;
		this.iterationName = iterationName;
		this.benchmarkName = benchmarkName;
	}

	public void writeCSV(Iteration3ResultsElement results) {

		createFolder();
		CSVWriter csvWriter;

		try {
			csvWriter = new CSVWriter(new FileWriter(getOutputPath()), DefaultValues.CSV_SEPERATOR,CSVWriter.NO_QUOTE_CHARACTER);
			List<String[]> data = computeImprovement(results.getResults());
			csvWriter.writeAll(data);
			csvWriter.close();
		} catch (IOException e) {
			LOGGER.error(StringTemplates.outputCantWriteFile(getOutputPath()), e);
			e.printStackTrace();
		}
	}

	private String getOutputPath() {
		String scenario = "_"+getTitleScenario().replace(" ", "_");
		return Paths.get(getOutputFolderPath(), fileName + DefaultValues.RATIO_FILES +scenario+ DefaultValues.CSV_ENDING).toString();
	}
	
	private String getTitleScenario() {
		
		String[] benchmarkNameSplits = benchmarkName.split("_");
		String title = StringTemplates.getUpperFirstChar(benchmarkNameSplits[0])
				+" "+StringTemplates.getAlgorithm(benchmarkNameSplits[1])+"_";
		if(computerName.equals(DefaultValues.COMPUTER_SUMMARYNAME)){
			String chartName = getSummaryScenarioTitle();
			return title+chartName;
		}
		return title+computerName;
	}
	
	private String getSummaryScenarioTitle() {
		String chartName = "";
		if(fileName.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.TIME_CHARTTITLE;
		}else if(fileName.equals(Integer.toString(DefaultValues.PERFORMANCE_DEFAULTINDX))){
			chartName=DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}

	private String getOutputFolderPath() {
		return Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
	}

	private void createFolder() {
		FileFolderOperations.createFolderIfNotExist(getOutputFolderPath());
	}

	private List<String[]> computeImprovement(Map<Long, Iteration3ResultElement> elements) {
		Map<Long, Iteration3ResultElement> ref = getRef(elements);
		Map<Long, Iteration3ResultElement> toCompare = getToCompareElements(elements);
		List<String> technologies = getTechnologies(elements);
		List<String[]> csvContent = getHeadLine(technologies);
		if (ref.size() >= toCompare.size()) {
			csvContent.addAll(computeImprovementRefWithToCompare(ref, toCompare,technologies));
		} else {
			csvContent.addAll(computeImprovementToCompareWithRef(ref, toCompare,technologies));
		}
		return csvContent;
	}
	
	private List<String[]> getHeadLine(List<String> technologies){
		List<String[]> headLine = new ArrayList<String[]>();
		String[] headLineStrings = new String[technologies.size()+1];
		headLineStrings[0] = DefaultValues.R_SCALEFACTOR_TITLE;
		for(int index = 0; index < technologies.size(); index++){
			headLineStrings[(index+1)]=StringTemplates.getTestCaseTitle(technologies.get(index));
		}
		headLine.add(headLineStrings);
		return headLine;
	}
	private List<String[]> computeImprovementRefWithToCompare(Map<Long, Iteration3ResultElement> ref,Map<Long, Iteration3ResultElement> toCompare, List<String> technologies){
		List<String[]> cells = new ArrayList<String[]>();
		for(Long refKey : getSortedScaleFactor(ref)){
			
			String[] row = new String[technologies.size()+1];
			row[0] = Long.toString(refKey);
			String t_refElementKey = new ArrayList<String>(ref.get(refKey).getElements().keySet()).get(0);
			Iteration3ResultAtomElement t_refElement = ref.get(refKey).getElements().get(t_refElementKey);
			if(toCompare.containsKey(refKey)){
				Iteration3ResultElement toCompareElement = toCompare.get(refKey);
				
				for(int index = 0; index < technologies.size() ; index++){
					if(toCompareElement.getElements().containsKey(technologies.get(index))){
						row[index+1] = computeRatrio(t_refElement,toCompareElement.getElements().get(technologies.get(index)));
					}else{
						row[index+1] = "-";
					}
				}
			}else{
				for(int index = 0; index < technologies.size() ; index++){
					row[index+1] = "-";
				}
			}
			cells.add(row);
		}
		
		return cells;
	}

	private List<String[]> computeImprovementToCompareWithRef(Map<Long, Iteration3ResultElement> ref, Map<Long, Iteration3ResultElement> toCompare,List<String> technologies) {
		List<String[]> cells = new ArrayList<String[]>();
		for(Long toCompareKey : getSortedScaleFactor(toCompare)){
			
			String[] row = new String[technologies.size()+1];
			row[0] = Long.toString(toCompareKey);
			Iteration3ResultElement toCompareElement = toCompare.get(toCompareKey);
			if(ref.containsKey(toCompareKey)){
				Iteration3ResultElement refElement = ref.get(toCompareKey);
				String t_refElementKey = new ArrayList<String>(refElement.getElements().keySet()).get(0);
				Iteration3ResultAtomElement t_refElement = refElement.getElements().get(t_refElementKey);
				for(int index = 0; index < technologies.size() ; index++){
					if(toCompareElement.getElements().containsKey(technologies.get(index))){
						row[index+1] = computeRatrio(t_refElement,toCompareElement.getElements().get(technologies.get(index)));
					}else{
						row[index+1] = "-";
					}
				}
			}
			cells.add(row);
		}
		return cells;
	}
	
	private String computeRatrio(Iteration3ResultAtomElement ref, Iteration3ResultAtomElement toCompare){
		return String.format(Locale.GERMAN, "%1$,.4f", toCompare.getMean()/ref.getMean());
	}
	
	private List<Long> getSortedScaleFactor(Map<Long, Iteration3ResultElement> elements){
		List<Long> sortedKeys = new ArrayList<Long>(elements.keySet());
		Collections.sort(sortedKeys);
		return sortedKeys;
	}
	
	private Map<Long, Iteration3ResultElement> getRef(Map<Long, Iteration3ResultElement> elements) {
		Map<Long, Iteration3ResultElement> ref = new HashMap<Long, Iteration3ResultElement>();

		for (Entry<Long, Iteration3ResultElement> element : elements.entrySet()) {
			Iteration3ResultElement refElement = new Iteration3ResultElement();
			for (Entry<String, Iteration3ResultAtomElement> t_element : element.getValue().getElements().entrySet()) {
				if (t_element.getKey().equals(DefaultValues.REF_TECHNOLOGY)) {
					refElement.addElement(t_element.getKey(), t_element.getValue());
					break;
				}
			}
			if (!refElement.getElements().isEmpty()) {
				ref.put(element.getKey(), refElement);
			}
		}
		return ref;
	}

	private Map<Long, Iteration3ResultElement> getToCompareElements(Map<Long, Iteration3ResultElement> elements) {
		Map<Long, Iteration3ResultElement> toCompareElements = new HashMap<Long, Iteration3ResultElement>();
		for (Entry<Long, Iteration3ResultElement> element : elements.entrySet()) {
			Iteration3ResultElement compareElement = new Iteration3ResultElement();
			for (Entry<String, Iteration3ResultAtomElement> t_element : element.getValue().getElements().entrySet()) {
				
				if (!t_element.getKey().equals(DefaultValues.REF_TECHNOLOGY)) {
					compareElement.addElement(t_element.getKey(), t_element.getValue());
				}
			}
			if (!compareElement.getElements().isEmpty()) {
				toCompareElements.put(element.getKey(), compareElement);
			}
		}
		return toCompareElements;
	}
	
	private List<String> getTechnologies(Map<Long, Iteration3ResultElement> elements) {
		List<String> technologies = new ArrayList<String>();
		for (Entry<Long, Iteration3ResultElement> element : elements.entrySet()) {
			for (Entry<String, Iteration3ResultAtomElement> t_element : element.getValue().getElements().entrySet()) {
				if (!t_element.getKey().equals(DefaultValues.REF_TECHNOLOGY)) {
					if(!technologies.contains(t_element.getKey())){
						technologies.add(t_element.getKey());
					}
				}
			}
		}
		return technologies;
	}
}