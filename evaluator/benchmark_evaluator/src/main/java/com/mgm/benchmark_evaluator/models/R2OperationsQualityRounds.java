package com.mgm.benchmark_evaluator.models;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;

public class R2OperationsQualityRounds {
	
	private final String resultFolderPath;
	private final String fileNameNumber;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(R2OperationsQualityRounds.class.getName()); 
	public static boolean WITH_COLOR = true;

	public R2OperationsQualityRounds(String benchmarkName,String resultFolderPath,String iterationName,String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileNameNumber = fileName.split("_")[0];
		this.fileName = fileName.split("_")[1].replaceAll("[^a-zA-Z0-9 ]","").replace(" ","_");
		this.computerName = computerName;
		this.iterationName = iterationName;	
		this.benchmarkName = benchmarkName;
	}

	public void write(Iteration1ResultsElement results){
		if(!results.isEmpty()){
			try {
				writeR(results);
				writeSVG();
			} catch (IOException e) {
				LOGGER.error(e);
			}
		}
	}
	
	private void writeR(Iteration1ResultsElement results) throws IOException{
		List<String> lines;
		if(WITH_COLOR){
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT3COLOR_TEMPLATE_PATH);
		}else{
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT3_TEMPLATE_PATH);
		}
		List<String> outputLines = new ArrayList<String>();
		for(String line: lines){
			line = line
					.replace(StringTemplates.R2_OURTPUTPATH_KEY, getSvgOutputPath(benchmarkName))
					.replace(StringTemplates.R2_TITLEBOTTOM_KEY, getTitleBottom())
					.replace(StringTemplates.R2_YTITLE_KEY, getYTitle(results))
					.replace(StringTemplates.R2_WITHSTD_KEY, getWithStd())
					.replace(StringTemplates.R2_TITLETOP_KEY, getTitleTop())
					.replace(StringTemplates.R2_GRAPHNAMES_KEY , getGraphNames(results))
					.replace(StringTemplates.R2_GRAPHVALUES_KEY, getGraphValues(results))
					.replace(StringTemplates.R2_STDVALUES_KEY, getSDValues(results))
					;
			outputLines.add(line);
		}
		FileFolderOperations.createFolderIfNotExist(FileFolderOperations.getFolderPathFromFilePath(getRFilePath(benchmarkName)));
		FileFolderOperations.writeFile(getRFilePath(benchmarkName), outputLines);
	}
	
	private String getRFilePath(String benchmarkName){
		String folderPath = Paths.get(resultFolderPath,iterationName, benchmarkName,computerName).toString();
		return Paths.get(folderPath,DefaultValues.R_TEMPFOLDERNAME,fileNameNumber+DefaultValues.QUALITYROUND_FILES+ DefaultValues.R_ENDING).toString();
	}
	
	private String getGraphNames(Iteration1ResultsElement results){
		String retunValue = "\""+DefaultValues.R_SCALEFACTOR_TITLE+"\",";
		int numberRounds = 0;
		for(Iteration1ResultElement result: results.getResults()){
			if(numberRounds < result.getElements().size()){
				numberRounds = result.getElements().size();
			}
		}
		
		for(int roundIndex = 1; roundIndex <= numberRounds; roundIndex++){
			retunValue += "\" Round: "+Integer.toString(roundIndex)+"\",";
		}
		
		if(retunValue.endsWith(",")){
			retunValue = retunValue.substring(0, retunValue.length()-1);
		}
		
		return retunValue;
	}
	
	private String getTitleTop() {
		
		String[] benchmarkNameSplits = benchmarkName.split("_");
		String title = StringTemplates.getTestCaseTitle(benchmarkNameSplits[0])
				+" "+StringTemplates.getUpperFirstChar(benchmarkNameSplits[1])
				+" "+StringTemplates.getAlgorithm(benchmarkNameSplits[2])+"\\n";
		if(computerName.equals(DefaultValues.COMPUTER_SUMMARYNAME)){
			String chartName = getSummaryChartTitle();
			return title+chartName;
		}
		return title+computerName+" ("+DefaultValues.QUALITY_PLOT_TITLE+")";
	}
	
	private String getSummaryChartTitle() {
		String chartName = "";
		if(fileNameNumber.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.TIME_CHARTTITLE;
		}else if(fileNameNumber.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}
	
	private String getWithStd(){
		return "FALSE";
	}
	
	private String getTitleBottom(){
		return "";
	}
	
	private String getYTitle(Iteration1ResultsElement results){
		if(results.getName().indexOf("_") < 0){
			return results.getName()+" ["+DefaultValues.QUALITY_PLOT_UNIT+"]";
		}
		return results.getName().substring(results.getName().indexOf("_"), results.getName().length())+"["+DefaultValues.QUALITY_PLOT_UNIT+"]";
	}
	
	private String getSvgOutputPath(String benchmarkName){
		String scenario = "_"+getTitleTop().replace(" ", "_").replace("\\n", "_");
		String folderPath = Paths.get(resultFolderPath,iterationName, benchmarkName,computerName).toString();
		return Paths.get(folderPath,fileNameNumber+"_"+fileName+"_"+DefaultValues.QUALITYROUND_FILES+scenario+ DefaultValues.PDF_ENDING).toString();
	}
	
	private String getGraphValues(Iteration1ResultsElement results){
		
		String returnScalFactorString = "sf=c(";
		List<String> roundsString = new ArrayList<String>();
		
		int numberRounds = 0;
		for(Iteration1ResultElement result: results.getResults()){
			if(numberRounds < result.getQualitiesInPercent().size()){
				numberRounds = result.getQualitiesInPercent().size();
			}
		}
		for(int roundIndex = 1; roundIndex <= numberRounds; roundIndex++){
			roundsString.add("round"+roundIndex+"=c(");
		}
		
		
		for(Iteration1ResultElement result : results.getResults()){
			for(int roundIndex = 0; roundIndex < numberRounds; roundIndex++){
				if(roundIndex < result.getQualitiesInPercentAsStrings().size()){
					roundsString.set(roundIndex,  roundsString.get(roundIndex)+result.getQualitiesInPercentAsStrings().get(roundIndex).replace(".", "").replace(",", ".")+",");
				}else{
					roundsString.set(roundIndex,roundsString.get(roundIndex)+"NA,");
				}
			}
			returnScalFactorString += result.getScaleFactorAsString().replace(".", "").replace(",", ".")+",";
		}
		if(returnScalFactorString.endsWith(",")){
			for(int roundIndex = 0; roundIndex < numberRounds; roundIndex++){
				roundsString.set(roundIndex, roundsString.get(roundIndex).substring(0, roundsString.get(roundIndex).length()-1));
			}
			returnScalFactorString = returnScalFactorString.substring(0, returnScalFactorString.length()-1);
		}
		for(int roundIndex = 0; roundIndex < numberRounds; roundIndex++){
			roundsString.set(roundIndex, roundsString.get(roundIndex)+")");
		}
		returnScalFactorString +=")";
		
		for(String roundString : roundsString){
			returnScalFactorString += ", "+roundString;
		}
		
		return returnScalFactorString;
	}
	
	private String getSDValues(Iteration1ResultsElement results){
		String returnString = "sd=c(NA)";
		return returnString;
	}
	
	private void writeSVG(){
		RscriptCommandLine rsciptCommand = new RscriptCommandLine(getRFilePath(benchmarkName));
		rsciptCommand.run();
	}
}
