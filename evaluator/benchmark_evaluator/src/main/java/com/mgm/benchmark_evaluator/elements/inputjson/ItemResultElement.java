package com.mgm.benchmark_evaluator.elements.inputjson;

/**
 * contain one result from one item 
 * @author bugge
 * @since 11.02.2016
 */
public class ItemResultElement implements Comparable<ItemResultElement>{
	private Integer itemID;
	private Long clock;
	private Integer dataFormat;
	private Float valueFloat = null;
	private String valueString = null;
	private Long valueLong = null;
	private Long nanoseconds;
	public Integer getItemID() {
		return itemID;
	}
	public void setItemID(Integer itemID) {
		this.itemID = itemID;
	}
	public Long getClock() {
		return clock;
	}
	public void setClock(Long clock) {
		this.clock = clock;
	}
	public Integer getDataFormat() {
		return dataFormat;
	}
	public void setDataFormat(Integer dataFormat) {
		this.dataFormat = dataFormat;
	}
	public Long getNanoseconds() {
		return nanoseconds;
	}
	public void setNanoseconds(Long nanoseconds) {
		this.nanoseconds = nanoseconds;
	}
	public Float getValueFloat() {
		return valueFloat;
	}
	public void setValueFloat(Float valueFloat) {
		this.valueFloat = valueFloat;
	}
	public String getValueString() {
		return valueString;
	}
	public void setValueString(String valueString) {
		this.valueString = valueString;
	}
	public Long getValueLong() {
		return valueLong;
	}
	public void setValueLong(Long valueLong) {
		this.valueLong = valueLong;
	}
	@Override
	public String toString() {
		return "ItemResultElement [itemID=" + itemID + ", clock=" + clock + ", dataFormat=" + dataFormat
				+ ", valueFloat=" + valueFloat + ", valueString=" + valueString + ", valueLong=" + valueLong
				+ ", nanoseconds=" + nanoseconds + "]";
	}
	@Override
	public int compareTo(ItemResultElement arg0) {
		if(this.getClock() < arg0.getClock()){
			return -1;
		}else {
			return 1;
		}
	}
}
