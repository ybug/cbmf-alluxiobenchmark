package com.mgm.spark_terasort;

import com.github.jankroken.commandline.annotations.LongSwitch;
import com.github.jankroken.commandline.annotations.Option;
import com.github.jankroken.commandline.annotations.Required;
import com.github.jankroken.commandline.annotations.ShortSwitch;
import com.github.jankroken.commandline.annotations.SingleArgument;
import com.github.jankroken.commandline.annotations.Toggle;

public class Arguments {
	private String inputPath = null;
	private String outputPath = null;
	private String hdfs = "";
	private int partitionNumber = 1;
	private boolean offHeap = false;
	private String fsUser = "spark";

	public String getFsUser() {
		return fsUser;
	}

	@Option
	@LongSwitch("fsUser")
	@ShortSwitch("fsu")
	@SingleArgument
	public void setFsUser(String fsUser) {
		this.fsUser = fsUser;
	}

	public String getInputPath() {
		return inputPath;
	}

	@Option
	@LongSwitch("input")
	@ShortSwitch("i")
	@SingleArgument
	@Required
	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
	}

	public String getOutputPath() {
		return outputPath;
	}

	@Option
	@LongSwitch("output")
	@ShortSwitch("o")
	@SingleArgument
	@Required
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public String getHdfs() {
		return hdfs;
	}
	
	@Option
	@LongSwitch("filesystem")
	@ShortSwitch("fs")
	@SingleArgument
	public void setHdfs(String hdfs) {
		this.hdfs = hdfs;
	}

	public int getPartitionNumber() {
		return partitionNumber;
	}

	@Option
	@LongSwitch("partitionnumber")
	@ShortSwitch("pn")
	@SingleArgument
	public void setPartitionNumber(String partitionNumber) {
		try{
			this.partitionNumber = Integer.parseInt(partitionNumber);
		}catch(NumberFormatException ex){
			this.partitionNumber = 1;
		}
	}
	
	public boolean isOffHeap() {
		return offHeap;
	}
	
	@Option
    @LongSwitch("offheap")
    @ShortSwitch("oh")
    @Toggle(true)
	public void setOffHeap(boolean offHeap) {
		this.offHeap = offHeap;
	}
}
