package com.mgm.benchmark_evaluator.elements.iteration3;

import java.util.HashMap;
import java.util.Map;

public class Iteration3ComputerResultsElement {

	private final String name;
	//itemid
	private Map<Integer,Iteration3ResultsElement> results = new HashMap<Integer,Iteration3ResultsElement>();
	
	public Iteration3ComputerResultsElement(String name) {
		this.name = name;
	}
	
	public void setResults(Map<Integer,Iteration3ResultsElement> results){
		this.results = results;
	}

	public String getName() {
		return name;
	}

	public Map<Integer, Iteration3ResultsElement> getResults() {
		return results;
	}

	@Override
	public String toString() {
		return "Iteration3ComputerResultsElement [name=" + name + ", results=" + results + "]";
	}
}
