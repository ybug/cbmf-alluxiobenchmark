package com.mgm.benchmark_evaluator.elements.inputjson;

import java.util.HashMap;
import java.util.Map;

/**
 * contains the items from one host
 * @author bugge
 * @since 11.02.2016
 */
public class HostResultElement {

	private Integer id;
	private Map<Integer, ItemResultsElement> items = new HashMap<Integer,ItemResultsElement>();
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Map<Integer, ItemResultsElement> getItems() {
		return items;
	}
	public void setItems(Map<Integer, ItemResultsElement> items) {
		this.items = items;
	}
	@Override
	public String toString() {
		return "HostResultElement [id=" + id + ", items=" + items + "]";
	}
}
