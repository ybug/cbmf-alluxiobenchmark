package com.mgm.benchmark_evaluator.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.inputjson.ItemInformationElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ComputerResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ScenarioResultsElement;
import com.mgm.benchmark_evaluator.models.DefaultValues;
import com.mgm.benchmark_evaluator.models.MathOperations;

/**
 * merge same benchmarks with different scale factors 
 * summary all worker 
 * @author bugge
 *
 */
public class Iteration2 {

	private final Iteration1BenchmarkResultsElement benchmarksResultsIt1;
	private final Map<Integer,ItemInformationElement> itemsInformation;
	private final static Logger LOGGER = Logger.getLogger(Iteration2.class.getName());
	
	public Iteration2(Iteration1BenchmarkResultsElement benchmarksResultsIt1,Map<Integer,ItemInformationElement> itemsInformation) {
		this.benchmarksResultsIt1 = benchmarksResultsIt1;
		this.itemsInformation = itemsInformation;
	}

	public Iteration1BenchmarkResultsElement compute() {
		return computeSummarys(mergeBenchmarks(getMatchingBenchmarksName()));
	}

	private Iteration1BenchmarkResultsElement mergeBenchmarks(Map<String, List<String>> matchResultsNames) {
		Iteration1BenchmarkResultsElement benchmarksResultsIt2 = new Iteration1BenchmarkResultsElement();
		for (Entry<String, List<String>> matchResultsName : matchResultsNames.entrySet()) {
			for (String benchmarkName : matchResultsName.getValue()) {
				if (benchmarksResultsIt2.getBenchmarks().containsKey(matchResultsName.getKey())) {
					mergeBenchmark(benchmarksResultsIt2.getBenchmarks().get(matchResultsName.getKey()),
							benchmarksResultsIt1.getBenchmarks().get(benchmarkName));
				} else {
					Iteration1ScenarioResultsElement newBenchmark = benchmarksResultsIt1.getBenchmarks().get(benchmarkName);
					newBenchmark.setName(matchResultsName.getKey());
					benchmarksResultsIt2.getBenchmarks().
					put(matchResultsName.getKey(), newBenchmark);
				}
			}
		}
		return benchmarksResultsIt2;
	}
	
	private Iteration1BenchmarkResultsElement computeSummarys(Iteration1BenchmarkResultsElement orig){
		orig = mergeWorker(orig,getMatchingWorkerName(orig));
		writeOut(orig);
		return orig;
	}
	
	private Iteration1BenchmarkResultsElement mergeWorker(Iteration1BenchmarkResultsElement orig,Map<String, List<String>> matchResultsNames) {

		for (Entry<String, List<String>> matchResultsName : matchResultsNames.entrySet()) {
			if(matchResultsName.getValue().size()>1){
				for(Entry<String,Iteration1ScenarioResultsElement> benchmark: orig.getBenchmarks().entrySet()){
					Iteration1ComputerResultsElement worker = new Iteration1ComputerResultsElement(matchResultsName.getKey());
					Map<Integer,Iteration1ResultsElement> itemsName = getWorkerItemKeys(benchmark.getValue(),matchResultsName.getValue());
					for(Entry<Integer,Iteration1ResultsElement> itemName : itemsName.entrySet()){
						Map<Long,MathOperations> maths = new HashMap<Long,MathOperations>();
						Map<Long,MathOperations> mathsQualities = new HashMap<Long,MathOperations>();
						for (String workerName : matchResultsName.getValue()) {
							Iteration1ComputerResultsElement oneWorker = benchmark.getValue().getComputer().get(workerName);
							for(Integer oldItemKey : getItemKeysFromTemplateKey(itemName.getKey())){
								if(oneWorker.getResults().containsKey(oldItemKey)){
									Iteration1ResultsElement items = oneWorker.getResults().get(oldItemKey);
									for(Iteration1ResultElement item : items.getResults()){
										if(maths.containsKey(item.getScaleFactor())){
											maths.get(item.getScaleFactor()).addDoubleValue(item.getMean());
											mathsQualities.get(item.getScaleFactor()).addDoubleValue(item.computMeanQualityInPercentByElements());
										}else{
											MathOperations math = new MathOperations();
											math.addDoubleValue(item.getMean());
											maths.put(item.getScaleFactor(), math);
											MathOperations mathQuality = new MathOperations();
											mathQuality.addDoubleValue(item.computMeanQualityInPercentByElements());
											mathsQualities.put(item.getScaleFactor(), mathQuality);
										}
									}
								}
							}
						}
						//Iteration1ResultElement
						//set item 
						for(Entry<Long,MathOperations> math : maths.entrySet()){
							Iteration1ResultElement newItem = new Iteration1ResultElement();
							newItem.setMax(math.getValue().getMax());
							newItem.setMean(math.getValue().getMean());
							newItem.setMin(math.getValue().getMin());
							newItem.setStandardDeviation(math.getValue().getStandardDeviation());
							newItem.setScaleFactor(math.getKey());
							newItem.setQualityInPercent(mathsQualities.get(math.getKey()).getMean());
							newItem.setQualityInPercentSTD(mathsQualities.get(math.getKey()).getStandardDeviation());
							newItem.setElements(math.getValue().getElements());
							newItem.setQualitiesInPercent(mathsQualities.get(math.getKey()).getElements());
							itemName.getValue().addResult(newItem);
						}
					}	
					worker.setResults(itemsName);
					benchmark.getValue().getComputer().put(worker.getName(), worker);
				}
			}
		}
		return orig;
	}
	
	private List<Integer> getItemKeysFromTemplateKey(Integer templateKey){
		List<Integer> itemKey = new ArrayList<Integer>();
		for(Entry<Integer,ItemInformationElement> itemInformation : itemsInformation.entrySet()){
			if(itemInformation.getValue().getTemplateId().equals(templateKey)){
				itemKey.add(itemInformation.getKey());
			}
		}
		
		return itemKey;
	}
	
	private Map<Integer,Iteration1ResultsElement> getWorkerItemKeys(Iteration1ScenarioResultsElement benchmark, List<String> workerNames){
		
		Map<Integer,Iteration1ResultsElement> itemKeys = new HashMap<Integer,Iteration1ResultsElement>();
		for(String workerName : workerNames){
			if(benchmark.getComputer().containsKey(workerName)){
				Iteration1ComputerResultsElement worker = benchmark.getComputer().get(workerName);
				for(Entry<Integer,Iteration1ResultsElement> itemName:worker.getResults().entrySet()){
					if(!itemKeys.containsKey(itemName.getKey())){
						Iteration1ResultsElement item = new Iteration1ResultsElement();
						item.setName(itemName.getValue().getName());
						item.setUnit(itemName.getValue().getUnit());
						int newKey = this.itemsInformation.get(itemName.getKey()).getTemplateId();
						item.setItemId(newKey);
						itemKeys.put(newKey,item);
					}
				}
			}
		}
		return itemKeys;
	}
	
	private void mergeBenchmark(Iteration1ScenarioResultsElement merge, Iteration1ScenarioResultsElement orig){
		for(Entry<String,Iteration1ComputerResultsElement> computer : merge.getComputer().entrySet()){
			Iteration1ComputerResultsElement origComp = orig.getComputer().get(computer.getKey());
			for(Entry<Integer,Iteration1ResultsElement> item: computer.getValue().getResults().entrySet()){
				if(!origComp.getResults().containsKey(item.getKey())){
					System.out.println(item.getKey() +"-> not found" );
					System.out.println(item.getValue().getName());
					
					
					System.out.println();
					continue;
				}
				for(Iteration1ResultElement origItemSeg : origComp.getResults().get(item.getKey()).getResults()){
					item.getValue().addResult(origItemSeg);
				}
			}
		}
	}

	private Map<String, List<String>> getMatchingWorkerName(Iteration1BenchmarkResultsElement orig) {
		Map<String, List<String>> returnValue = new HashMap<String, List<String>>();
		for (Entry<String, Iteration1ScenarioResultsElement> benchmark : orig.getBenchmarks().entrySet()) {
			for(Entry<String,Iteration1ComputerResultsElement> server : benchmark.getValue().getComputer().entrySet()){
				String serverName = server.getKey();
				if (serverName.contains(DefaultValues.WORKER_NAME)) {
					addToResultValue(DefaultValues.WORKER_NAME, serverName, returnValue);
				} 
			}
		}
		return returnValue;
	}
	
	private Map<String, List<String>> getMatchingBenchmarksName() {
		Map<String, List<String>> returnValue = new HashMap<String, List<String>>();
		for (Entry<String, Iteration1ScenarioResultsElement> benchmark : benchmarksResultsIt1.getBenchmarks().entrySet()) {
			String benchmarkName = benchmark.getKey();
			if (benchmarkName.contains(DefaultValues.BENCHMARK_PART_NAME1)) {
				benchmarkName = getSummaryBenchmarkName(DefaultValues.BENCHMARK_PART_NAME1, benchmarkName);
			} else if (benchmarkName.contains(DefaultValues.BENCHMARK_PART_NAME2)) {
				benchmarkName = getSummaryBenchmarkName(DefaultValues.BENCHMARK_PART_NAME2, benchmarkName);
			}
			if (benchmarkName.startsWith("_")) {
				benchmarkName = benchmarkName.substring(1, benchmarkName.length());
			}
			addToResultValue(benchmarkName, benchmark.getKey(), returnValue);
		}
		return returnValue;
	}

	private String getSummaryBenchmarkName(String pattern, String benchmarkName) {

		String[] benchmarkNameParts = benchmarkName.split("_");
		String benchmarkNewName = "";
		for (String benchmarkNamePart : benchmarkNameParts) {
			if (benchmarkNamePart.contains(pattern)) {
				continue;
			}
			benchmarkNewName += benchmarkNamePart + "_";
		}
		if (benchmarkNewName.endsWith("_")) {
			benchmarkNewName = benchmarkNewName.substring(0, benchmarkNewName.length() - 1);
		}
		return benchmarkNewName;
	}

	private void addToResultValue(String summaryBenchmarkName, String benchmarkName, Map<String, List<String>> returnValue) {
		if (returnValue.containsKey(summaryBenchmarkName)) {
			if(!returnValue.get(summaryBenchmarkName).contains(benchmarkName)){
				returnValue.get(summaryBenchmarkName).add(benchmarkName);
			}
		} else {
			List<String> origBenchmarkNames = new ArrayList<String>();
			origBenchmarkNames.add(benchmarkName);
			returnValue.put(summaryBenchmarkName, origBenchmarkNames);
		}
	}
	
	private void writeOut(Iteration1BenchmarkResultsElement benchmarks) {
		long counter = 1;
		for(Entry<String,Iteration1ScenarioResultsElement> benchmark : benchmarks.getBenchmarks().entrySet()){
			for (Entry<String, Iteration1ComputerResultsElement> computer : benchmark.getValue().getComputer().entrySet()) {
				for (Entry<Integer, Iteration1ResultsElement> results : computer.getValue().getResults().entrySet()) {
					OutputController writeOperations = new OutputController(benchmark.getKey(), DefaultValues.ITERATION2_NAME, computer.getKey(),
							results.getKey()+"_"+results.getValue().getName());
					writeOperations.printResults(results.getValue());
				}
			}
			LOGGER.info(benchmark.getKey()+" | "+counter +" \\ "+benchmarks.getBenchmarks().size());
			counter++;
			
		}
	}
}
