package com.mgm.spark_terasort.extern.eastcirclek.total_order_partitioner;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.hadoop.io.Text;

/**
 * A leaf trie node that does string compares to figure out where the given
 * key belongs between lower..upper.
 */
public class LeafTrieNode extends TrieNode {
    int lower;
    int upper;
    Text[] splitPoints;
    public LeafTrieNode(int level, Text[] splitPoints, int lower, int upper) {
        super(level);
        this.splitPoints = splitPoints;
        this.lower = lower;
        this.upper = upper;
    }
    public int findPartition(Text key) {
        for(int i=lower; i<upper; ++i) {
            if (splitPoints[i].compareTo(key) >= 0) {
                return i;
            }
        }
        return upper;
    }
    public void print(PrintStream strm) throws IOException {
        for(int i = 0; i < 2*getLevel(); ++i) {
            strm.print(' ');
        }
        strm.print(lower);
        strm.print(", ");
        strm.println(upper);
    }
}
