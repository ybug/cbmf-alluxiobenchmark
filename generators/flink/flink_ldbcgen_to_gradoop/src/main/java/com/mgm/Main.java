package com.mgm;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.utils.ParameterTool;
import org.gradoop.common.model.impl.properties.PropertyList;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.io.impl.json.JSONDataSink;
import org.gradoop.flink.model.impl.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import org.s1ck.ldbc.LDBCToFlink;
import org.s1ck.ldbc.tuples.LDBCEdge;
import org.s1ck.ldbc.tuples.LDBCVertex;

import com.google.common.collect.Maps;

public class Main {

	/**
	 * File containing EPGM vertices.
	 */
	private static final String VERTICES_JSON = "nodes.json";
	/**
	 * File containing EPGM edges.
	 */
	private static final String EDGES_JSON = "edges.json";
	/**
	 * File containing EPGM graph heads.
	 */
	private static final String GRAPHS_JSON = "graphs.json";
	/**
	 * Flink execution environment.
	 */
	private static ExecutionEnvironment ENV;

	public static void main(String[] args) throws Exception {

		final ParameterTool params = ParameterTool.fromArgs(args);
		String inputPath = params.get("input", null);
		String outputFolderPath = params.get("output", null);

		if (inputPath == null || outputFolderPath == null) {
			System.err.println("input or output not set in the arguments!");
			return;
		}
		
		if (!outputFolderPath.endsWith("/")) {
			outputFolderPath += "/";
		}
		
		LDBCToFlink ldbcToFlink = new LDBCToFlink(inputPath, getExecutionEnvironment());
		
		DataSet<ImportVertex<Long>> importVertices = ldbcToFlink.getVertices()
				.map(new MapFunction<LDBCVertex, ImportVertex<Long>>(){

					private static final long serialVersionUID = 1L;

					@Override
					public ImportVertex<Long> map(LDBCVertex value) throws Exception {
						Map<String, Object> newProperties = getConvertedProperties(value.getProperties());
						return new ImportVertex<>(value.getVertexId(), value.getLabel(), PropertyList.createFromMap(newProperties));
					}
				});
		
		DataSet<ImportEdge<Long>> importEdges = ldbcToFlink.getEdges()
				.map(new MapFunction<LDBCEdge, ImportEdge<Long>>() {

					private static final long serialVersionUID = 1L;

					@Override
					public ImportEdge<Long> map(LDBCEdge value) throws Exception {
						Map<String, Object> newProperties = getConvertedProperties(value.getProperties());
						return new ImportEdge<>(value.getEdgeId(), value.getSourceVertexId(),
								value.getTargetVertexId(), value.getLabel(), PropertyList.createFromMap(newProperties));
					}
				});
		
		GraphDataSource<Long> dataSource = new GraphDataSource<>(
			      importVertices, importEdges, GradoopFlinkConfig.createConfig(getExecutionEnvironment()));
		
		writeLogicalGraph(dataSource.getLogicalGraph(),outputFolderPath);
		
	}
	
	private static Map<String, Object> getConvertedProperties(Map<String, Object> value) {
		Map<String, Object> newProperties = Maps.newHashMap();
		 
	    for(Entry<String, Object> property : value.entrySet()){
	    	if(property.getValue() instanceof Date){
	    		newProperties.put(property.getKey(), ((Date) property.getValue()).getTime());
	    	}else if(property.getValue() instanceof ArrayList){
	    	}else{
	    		newProperties.put(property.getKey(), property.getValue());
	    	}
	    }
		return newProperties;
	}		
	/**
	 * Returns a Flink execution environment.
	 *
	 * @return Flink execution environment
	 */
	protected static ExecutionEnvironment getExecutionEnvironment() {
		if (ENV == null) {
			ENV = ExecutionEnvironment.getExecutionEnvironment();
		}
		return ENV;
	}

	/**
	 * Writes a logical graph into a given directory.
	 *
	 * @param graph
	 *            logical graph
	 * @param directory
	 *            output path
	 * @throws Exception
	 */
	protected static void writeLogicalGraph(LogicalGraph graph, String directory) throws Exception {
		directory = appendSeparator(directory);
		graph.writeTo(new JSONDataSink(directory + GRAPHS_JSON, directory + VERTICES_JSON, directory + EDGES_JSON, graph.getConfig()));

		getExecutionEnvironment().execute("Flink LDBC Generator To Gradoop");
	}

	/**
	 * Appends a file separator to the given directory (if not already existing).
	 *
	 * @param directory
	 *            directory
	 * @return directory with OS specific file separator
	 */
	private static String appendSeparator(final String directory) {
		final String fileSeparator = System.getProperty("file.separator");
		String result = directory;
		if (!directory.endsWith(fileSeparator)) {
			result = directory + fileSeparator;
		}
		return result;
	}
}
