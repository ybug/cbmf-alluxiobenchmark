package com.mgm.text_extractor;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.java.DataSet;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.mahout.text.wikipedia.XmlInputFormat;

/**
 * Skeleton for a Flink Job.
 *
 * For a full example of a Flink Job, see the WordCountJob.java file in the
 * same package/directory or have a look at the website.
 *
 * You can also generate a .jar file that you can submit on your Flink
 * cluster.
 * Just type
 * 		mvn clean package
 * in the projects root directory.
 * You will find the jar in
 * 		target/flink-quickstart-0.1-SNAPSHOT-Sample.jar
 *
 */
public class Main {

	public static void main(String[] args) throws Exception {
		// set up the execution environment
		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		final ParameterTool params = ParameterTool.fromArgs(args);
		
		env.getConfig().setGlobalJobParameters(params);
		org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
		Configuration flinkConf = new Configuration();
		conf.set("xmlinput.start", "<text xml:space=\"preserve\">");
		conf.set("xmlinput.end", "</text>");
		Job job = Job.getInstance(conf);
		String inputPath = params.get("input", "hdfs://0.0.0.0/user/root/dataset/wikismall/enwiki-latest-pages-articles1.xml-p000000010p000030302");
		String outputPath = params.get("output","hdfs://0.0.0.0/user/root/results/flink_wiki_text_extractor/wikismall.txt");
		flinkConf.setInteger("scale", params.getInt("scale", 1));
		
		
		DataSet<Tuple2<LongWritable, Text>> input =
			    env.readHadoopFile(new XmlInputFormat(), LongWritable.class, Text.class, inputPath,job);
		
		input.flatMap(new RichFlatMapFunction<Tuple2<LongWritable, Text>, String>() {
			private static final long serialVersionUID = 1L;
			
			private int scaleFactor = 1;
			
			@Override
		    public void open(Configuration parameters) throws Exception {
				scaleFactor = parameters.getInteger("scale", 1);
		    }

			@Override
			public void flatMap(Tuple2<LongWritable, Text> input, Collector<String> output) {
				String origText;
				try {
					origText = WikiParser.run(input.f1.toString()).replaceAll("[^a-zA-Z0-9\\n .,!?]","");
					String returnString = "";
					for(int i = 0; i < scaleFactor; i++ ){
						returnString += origText.replaceAll("\n", "<"+origText.hashCode()+"_"+Integer.toString(i)+">\n");
						if(!returnString.endsWith("\n")){
							returnString += "<"+origText.hashCode()+"_"+Integer.toString(i)+">\n";
						}
					}
					if(returnString.endsWith("\n")){
						returnString  = returnString.substring(0, returnString.length()-1);
					}
					output.collect(returnString);
				} catch (Exception e) {
				}
				
			}
		}).withParameters(flinkConf).writeAsText(outputPath);
		
		
		// execute program
		env.execute("Flink wiki text extractor");
	}
}
