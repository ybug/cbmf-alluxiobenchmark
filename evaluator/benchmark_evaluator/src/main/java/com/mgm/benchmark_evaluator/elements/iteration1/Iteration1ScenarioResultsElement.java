package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.HashMap;
import java.util.Map;

public class Iteration1ScenarioResultsElement {
	
	private String name;
	
	private final Map<String,Iteration1ComputerResultsElement> computer = new HashMap<String,Iteration1ComputerResultsElement>();

	public void addResultsToComputer(String computerName, String metricName, String metricUnit,Integer keyId, Iteration1ResultElement result){
		if(computer.containsKey(computerName)){
			computer.get(computerName).addResult(metricName, metricUnit,keyId, result);
		}else{
			Iteration1ComputerResultsElement element = new Iteration1ComputerResultsElement(computerName);
			element.addResult(metricName, metricUnit,keyId, result);
			computer.put(computerName, element);
		}
	}
	
	public Iteration1ScenarioResultsElement(String name) {
		this.name = name;
	}
	public Map<String, Iteration1ComputerResultsElement> getComputer() {
		return computer;
	}

	public void setName(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Iteration1ScenarioResultsElement [name=" + name + ", computer=" + computer + "]";
	}
	
}
