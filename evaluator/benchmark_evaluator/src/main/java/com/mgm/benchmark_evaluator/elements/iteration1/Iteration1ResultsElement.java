package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Iteration1ResultsElement {

	private String name;
	private Integer itemId;
	private String unit;
	private List<Iteration1ResultElement> results = new ArrayList<Iteration1ResultElement>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public List<Iteration1ResultElement> getResults() {
		Collections.sort(results);
		return results;
	}
	public void setResults(List<Iteration1ResultElement> results) {
		this.results = results;
	}
	public void addResult(Iteration1ResultElement result){
		this.results.add(result);
	}
	public boolean isEmpty(){
		if(results.isEmpty()){
			return true;
		}
		for(Iteration1ResultElement result : results){
			if(!result.isEmpty()){
				return false;
			}
		}
		return true;
	}
	@Override
	public String toString() {
		return "Iteration1ResultsElement [name=" + name + ", itemId=" + itemId + ", unit=" + unit + ", results=" + results.size() + ", empty="+isEmpty()+"]";
	}
}
