package com.mgm.benchmark_evaluator.elements;

import com.github.jankroken.commandline.annotations.LongSwitch;
import com.github.jankroken.commandline.annotations.Option;
import com.github.jankroken.commandline.annotations.ShortSwitch;
import com.github.jankroken.commandline.annotations.SingleArgument;
import com.mgm.benchmark_evaluator.models.DefaultValues;
import com.mgm.benchmark_evaluator.models.StringTemplates;

public class Arguments {
	
	private String inputPath = DefaultValues.INPUT_PATH;
	private static String outputPath = DefaultValues.OUTPUT_PATH;
	private boolean withColor = false;

	public boolean isWithColor() {
		return withColor;
	}

	@Option
	@LongSwitch(StringTemplates.ARGUMENTS_COLOR_KEY_LONG)
	@ShortSwitch(StringTemplates.ARGUMENTS_COLOR_KEY_SHORT)    
	@SingleArgument
	public void setWithColor(String withColor) {
		this.withColor = Boolean.parseBoolean(withColor);
	}

	public String getInputPath() {
		return inputPath;
	}

	@Option
	@LongSwitch(StringTemplates.ARGUMENTS_INPUT_PATH_KEY_LONG)
	@ShortSwitch(StringTemplates.ARGUMENTS_INPUT_PATH_KEY_SHORT)
	@SingleArgument
	public void setInputPath(String configPath) {
		this.inputPath = configPath;
	}

	public static String getOutputPath() {
		return outputPath;
	}

	@Option
	@LongSwitch(StringTemplates.ARGUMENTS_OUTPUT_PATH_KEY_LONG)
	@ShortSwitch(StringTemplates.ARGUMENTS_OUTPUT_PATH_KEY_SHORT)
	@SingleArgument
	public void setOutputPath(String outputPath) {
		Arguments.outputPath = outputPath;
	}

	@Override
	public String toString() {
		return "Arguments [inputPath=" + inputPath + ", outputPath=" + outputPath + "]";
	}
}
