package com.mgm.terasort.extern.eastcirclek;

import org.apache.flink.api.common.functions.Partitioner;

public class OptimizedFlinkTeraPartitioner implements Partitioner<OptimizedText> {
	
	private static final long serialVersionUID = 1L;
	
	private TotalOrderPartitioner underlying;
	
	public OptimizedFlinkTeraPartitioner(TotalOrderPartitioner underlying){
		this.underlying = underlying;
	}

	@Override
	public int partition(OptimizedText key, int numPartitions) {
		return underlying.getPartition(key.getText());
	}	
}
