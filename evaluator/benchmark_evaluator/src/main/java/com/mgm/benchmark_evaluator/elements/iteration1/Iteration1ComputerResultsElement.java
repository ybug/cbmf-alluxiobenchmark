package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.HashMap;
import java.util.Map;

public class Iteration1ComputerResultsElement {

	private final String name;
	private Map<Integer,Iteration1ResultsElement> results = new HashMap<Integer,Iteration1ResultsElement>();
	
	public Iteration1ComputerResultsElement(String name) {
		this.name = name;
	}
	
	public void setResults(Map<Integer,Iteration1ResultsElement> results){
		this.results = results;
	}
	
	public void addResult(Integer key, Iteration1ResultsElement value){
		results.put(key, value);
	}
	//TODO
	public void addResult(String metricName, String metricUnit,Integer keyId, Iteration1ResultElement result){
		if(results.containsKey(metricName)){
			results.get(keyId).addResult(result);
		}else{
			Iteration1ResultsElement element = new Iteration1ResultsElement();
			element.setName(metricName);
			element.setUnit(metricUnit);
			element.setItemId(keyId);
			element.addResult(result);
			results.put(keyId, element);
		}
	}

	public String getName() {
		return name;
	}

	public Map<Integer, Iteration1ResultsElement> getResults() {
		return results;
	}

	@Override
	public String toString() {
		return "Iteration1ComputerResultsElement [name=" + name + ", results=" + results + "]";
	}
	
}
