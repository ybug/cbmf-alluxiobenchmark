package com.mgm.benchmark_evaluator.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ComputerResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ScenarioResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ComputerResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultAtomElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3ScenarioResultsElement;
import com.mgm.benchmark_evaluator.models.DefaultValues;
/**
 * Summary Storage systems 
 * @author bugge
 *
 */
public class Iteration3 {
	
	private final Iteration1BenchmarkResultsElement benchmarksResultsIt2;
	
	private final static Logger LOGGER = Logger.getLogger(Iteration3.class.getName());
	
	public Iteration3(Iteration1BenchmarkResultsElement benchmarksResultsIt2) {
		this.benchmarksResultsIt2 = benchmarksResultsIt2;
	}

	public Iteration3BenchmarkResultsElement compute() {
		return mergeBenchmarks();
	}
	
	private Iteration3BenchmarkResultsElement mergeBenchmarks(){
		Iteration3BenchmarkResultsElement merge = new Iteration3BenchmarkResultsElement();
		for(Entry<String,List<Iteration1ScenarioResultsElement>> newBenchmarkEntry : getToMergeBenchmarks().entrySet()){
			Iteration3ScenarioResultsElement newBenchmark = new Iteration3ScenarioResultsElement(newBenchmarkEntry.getKey());
			for(Iteration1ScenarioResultsElement oldBenchmark : newBenchmarkEntry.getValue()){
				addToNewBenchmark(newBenchmark,oldBenchmark,oldBenchmark.getName());
			}
			merge.getBenchmarks().put(newBenchmarkEntry.getKey(), newBenchmark);
		}
		writeOut(merge);
		return merge;
	}
	
	private void addToNewBenchmark(Iteration3ScenarioResultsElement newBenchmark,Iteration1ScenarioResultsElement oldBenchmark,String name){
		for(Entry<String, Iteration1ComputerResultsElement> oldComputer : oldBenchmark.getComputer().entrySet()){
			if(newBenchmark.getComputer().containsKey(oldComputer.getKey())){
				addNewComputer(newBenchmark.getComputer().get(oldComputer.getKey()),oldComputer.getValue(),getTechnologyName(name),name);
			}else{
				
				Iteration3ComputerResultsElement newComputer = new Iteration3ComputerResultsElement(oldComputer.getValue().getName());
				newComputer.setResults(initNewComputer(oldComputer.getValue(),getTechnologyName(name)));
				newBenchmark.getComputer().put(oldComputer.getKey(), newComputer);
			}
		}
		
	}
	
	private String getTechnologyName(String name){
		String[] nameList = name.split("_");
		String returnString = "";
		if(nameList.length >= 3){
			for(int index = 0; index < nameList.length-2; index++){
				returnString += nameList[index]+" ";
			}
			return returnString.substring(0,returnString.length()-1);
		}
		return name;
	}
	
	private void addNewComputer(Iteration3ComputerResultsElement newComputer,Iteration1ComputerResultsElement computer, String technologyName, String name){
		for(Entry<Integer,Iteration1ResultsElement> oldResults : computer.getResults().entrySet()){
			if(!newComputer.getResults().containsKey(oldResults.getKey())){
				Iteration3ResultsElement newResults = new Iteration3ResultsElement();
				newResults.setItemId(oldResults.getValue().getItemId());
				newResults.setName(oldResults.getValue().getName());
				newResults.setUnit(oldResults.getValue().getUnit());
				
				for(Iteration1ResultElement oldResult : oldResults.getValue().getResults()){
					createTechnologyInNewScalFactor(technologyName, newResults, oldResult);
				}
				newComputer.getResults().put(oldResults.getKey(),newResults);
			}else{
				Iteration3ResultsElement newResults = newComputer.getResults().get(oldResults.getKey());
				
				for(Iteration1ResultElement oldResult : oldResults.getValue().getResults()){
					if(newResults.getResults().containsKey(oldResult.getScaleFactor())){
						addTechnologyInExistItem(technologyName, newResults, oldResult);
					}else{
						createTechnologyInNewScalFactor(technologyName, newResults, oldResult);
					}
				}
				newComputer.getResults().put(oldResults.getKey(),newResults);
			}
		}
	}

	private void createTechnologyInNewScalFactor(String technologyName, Iteration3ResultsElement newResults, Iteration1ResultElement oldResult) {
		Iteration3ResultElement newResult_t = new Iteration3ResultElement();
		newResult_t.setScaleFactor(oldResult.getScaleFactor());
			Map<String, Iteration3ResultAtomElement> newResultAtoms = new HashMap<String, Iteration3ResultAtomElement>();
			
			copyResultAtom(technologyName, oldResult, newResultAtoms);
		newResult_t.setElements(newResultAtoms);
		newResults.getResults().put(oldResult.getScaleFactor(), newResult_t);
	}

	private void addTechnologyInExistItem(String technologyName, Iteration3ResultsElement newResults, Iteration1ResultElement oldResult) {
		Iteration3ResultElement newResult = newResults.getResults().get(oldResult.getScaleFactor());
		if (newResult.getElements().containsKey(technologyName)) {
			LOGGER.error("Technologie exist: " + technologyName + " " + newResults.getName() + " SF:" + oldResult.getScaleFactor());
		} else {
			copyResultAtom(technologyName, oldResult, newResults.getResults().get(oldResult.getScaleFactor()).getElements());
		}
	}
	
	private Map<Integer,Iteration3ResultsElement> initNewComputer(Iteration1ComputerResultsElement computer, String technologyName){
		
		Map<Integer,Iteration3ResultsElement> newResults = new HashMap<Integer,Iteration3ResultsElement>();
		for(Entry<Integer,Iteration1ResultsElement> oldResults : computer.getResults().entrySet()){
			Iteration3ResultsElement newResults_t = new Iteration3ResultsElement();
			newResults_t.setItemId(oldResults.getValue().getItemId());
			newResults_t.setName(oldResults.getValue().getName());
			newResults_t.setUnit(oldResults.getValue().getUnit());
			Map<Long,Iteration3ResultElement> newResult = new HashMap<Long,Iteration3ResultElement>();
			for(Iteration1ResultElement oldResult : oldResults.getValue().getResults()){
				Iteration3ResultElement newResult_t = new Iteration3ResultElement();
				newResult_t.setScaleFactor(oldResult.getScaleFactor());
					Map<String, Iteration3ResultAtomElement> newResultAtoms = new HashMap<String, Iteration3ResultAtomElement>();
					copyResultAtom(technologyName, oldResult, newResultAtoms);
					
				newResult_t.setElements(newResultAtoms);
				newResult.put(oldResult.getScaleFactor(), newResult_t);
			}
			newResults_t.setResults(newResult);
			newResults.put(oldResults.getKey(), newResults_t);
		}
		return newResults;
	}

	private void copyResultAtom(String technologyName, Iteration1ResultElement oldResult, Map<String, Iteration3ResultAtomElement> newResultAtoms) {
		Iteration3ResultAtomElement newResultAtom  = new Iteration3ResultAtomElement();
		newResultAtom.setElements(oldResult.getElements());
		newResultAtom.setMax(oldResult.getMax());
		newResultAtom.setMean(oldResult.getMean());
		newResultAtom.setMin(oldResult.getMin());
		newResultAtom.setStandardDeviation(oldResult.getStandardDeviation());
		newResultAtom.setQualityInPercent(oldResult.getQualityInPercent());
		newResultAtom.setQualityInPercentSTD(oldResult.getQualityInPercentSTD());
		newResultAtom.setTechnologyName(technologyName);
		newResultAtoms.put(technologyName, newResultAtom);
	}
	
	private Map<String,List<Iteration1ScenarioResultsElement>> getToMergeBenchmarks(){
		Map<String,List<Iteration1ScenarioResultsElement>> mergeBenchmarkNames = new HashMap<String,List<Iteration1ScenarioResultsElement>>();
		
		for(Entry<String,Iteration1ScenarioResultsElement> benchmark : benchmarksResultsIt2.getBenchmarks().entrySet()){
			String[] nameList = benchmark.getKey().split("_");
			String mergeBenchmarkName = "";
			if(nameList.length >= 3){
				for(int index = 1; index < nameList.length; index++){
					mergeBenchmarkName += nameList[index]+"_";
				}
				mergeBenchmarkName = mergeBenchmarkName.substring(0,mergeBenchmarkName.length()-1);
			}else{
				mergeBenchmarkName = benchmark.getKey();
			}
			
			if(mergeBenchmarkNames.containsKey(mergeBenchmarkName)){
				mergeBenchmarkNames.get(mergeBenchmarkName).add(benchmarksResultsIt2.getBenchmarks().get(benchmark.getKey()));
			}else{
				List<Iteration1ScenarioResultsElement> benchmarkNames = new ArrayList<Iteration1ScenarioResultsElement>();
				benchmarkNames.add(benchmarksResultsIt2.getBenchmarks().get(benchmark.getKey()));
				mergeBenchmarkNames.put(mergeBenchmarkName, benchmarkNames);
			}
		}
		return mergeBenchmarkNames;
	}
	
	private void writeOut(Iteration3BenchmarkResultsElement benchmarks) {
		long counter = 1;
		for(Entry<String,Iteration3ScenarioResultsElement> benchmark : benchmarks.getBenchmarks().entrySet()){
			for (Entry<String, Iteration3ComputerResultsElement> computer : benchmark.getValue().getComputer().entrySet()) {
				for (Entry<Integer, Iteration3ResultsElement> results : computer.getValue().getResults().entrySet()) {
					OutputController writeOperations = new OutputController(benchmark.getKey(), DefaultValues.ITERATION3_NAME, computer.getKey(),
							results.getKey()+"_"+results.getValue().getName());
					writeOperations.printResultsIt3(results.getValue());
				}
			}
			LOGGER.info(benchmark.getKey()+" | "+counter+" \\ "+benchmarks.getBenchmarks().size());
			counter++;
		}
	}
}
