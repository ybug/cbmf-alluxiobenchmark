#!/bin/bash

SOURCE_DIR=$2
TARGET_DIR=$3

if [[ "$SOURCE_DIR" == */ ]]
then
   SOURCE_DIR=${SOURCE_DIR%?}

fi

if [[ "$TARGET_DIR" == */ ]]
then
   TARGET_DIR=${TARGET_DIR%?}

fi

SOURCE_DIR_SEP_POS_temp=$(echo $SOURCE_DIR | grep -b -o / | tail -1 | cut -d'/' -f 1)
SOURCE_DIR_SEP_POS=$((${SOURCE_DIR_SEP_POS_temp%?}+1))

TARGET_DIR_SEP_POS_temp=$(echo $TARGET_DIR | grep -b -o / | tail -1| cut -d'/' -f 1)
TARGET_DIR_SEP_POS=$((${TARGET_DIR_SEP_POS_temp%?}+1))

SOURCE_DIR_NAME=$(echo $SOURCE_DIR | cut -c$SOURCE_DIR_SEP_POS- | cut -c2-)
SOURCE_DIR_PARENT=$(echo $SOURCE_DIR | cut -c-$SOURCE_DIR_SEP_POS) 

TARGET_DIR_NAME=$(echo $TARGET_DIR | cut -c$TARGET_DIR_SEP_POS- | cut -c2-)
TARGET_DIR_PARENT=$(echo $TARGET_DIR | cut -c-$TARGET_DIR_SEP_POS)

echo "mkdir: "$TARGET_DIR_PARENT
HADOOP_USER_NAME=$1 hdfs dfs -mkdir -p $TARGET_DIR_PARENT
echo ""
echo "cp: "$SOURCE_DIR" to-> "$TARGET_DIR_PARENT
HADOOP_USER_NAME=$1 hdfs dfs -cp $SOURCE_DIR $TARGET_DIR_PARENT
echo ""
echo "mv: "$TARGET_DIR_PARENT$SOURCE_DIR_NAME" -> "$TARGET_DIR
HADOOP_USER_NAME=$1 hdfs dfs -mv $TARGET_DIR_PARENT$SOURCE_DIR_NAME $TARGET_DIR

