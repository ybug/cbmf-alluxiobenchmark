package com.mgm.benchmark_evaluator.elements.inputjson;

import java.util.HashMap;
import java.util.Map;

/**
 * contains the results from the benchmark / times and zabbix results
 * @author bugge
 * @since 04.02.2016
 */
public class BenchmarkResultsElement {

	private String benchmarkName = "";
	private HostsGroupInformationElement groupInformation = new HostsGroupInformationElement();
	private Map<Integer,HostInformationElement> hostsInformation = new HashMap<Integer,HostInformationElement>();
	private Map<Integer,ItemInformationElement> itemsInformation = new HashMap<Integer,ItemInformationElement>();
	private Map<String,ScenarioResultElement> scenarios = new HashMap<String,ScenarioResultElement>();
	private boolean zabbixWasConnected = false;
	
	public String getBenchmarkName() {
		return benchmarkName;
	}
	public void setBenchmarkName(String benchmarkName) {
		this.benchmarkName = benchmarkName;
	}
	public HostsGroupInformationElement getGroupInformation() {
		return groupInformation;
	}
	public void setGroupInformation(HostsGroupInformationElement groupInformation) {
		this.groupInformation = groupInformation;
	}
	public Map<Integer, HostInformationElement> getHostsInformation() {
		return hostsInformation;
	}
	public void setHostsInformation(Map<Integer, HostInformationElement> hostsInformation) {
		this.hostsInformation = hostsInformation;
	}
	public Map<Integer, ItemInformationElement> getItemsInformation() {
		return itemsInformation;
	}
	public void setItemsInformation(Map<Integer, ItemInformationElement> itemsInformation) {
		this.itemsInformation = itemsInformation;
	}
	public Map<String,ScenarioResultElement> getScenarios() {
		return scenarios;
	}
	public void setScenarios(Map<String,ScenarioResultElement> scenarios) {
		this.scenarios = scenarios;
	}
	
	public boolean isZabbixWasConnected() {
		return zabbixWasConnected;
	}
	public void setZabbixWasConnected(boolean zabbixWasConnected) {
		this.zabbixWasConnected = zabbixWasConnected;
	}
	@Override
	public String toString() {
		return "BenchmarkResultsElement [benchmarkName=" + benchmarkName + ", groupInformation=" + groupInformation + ", hostsInformation="
				+ hostsInformation + ", itemsInformation=" + itemsInformation + ", scenarios=" + scenarios + ", zabbixWasConnected="
				+ zabbixWasConnected + "]";
	}
}
