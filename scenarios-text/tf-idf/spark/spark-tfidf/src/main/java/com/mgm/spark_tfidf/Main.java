package com.mgm.spark_tfidf;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.storage.StorageLevel;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.google.common.base.Optional;

import scala.Tuple2;
import scala.Tuple3;

/**
 * tfidf implement in spark
 * 
 * @author bugge
 * @since 07.06.2016
 */
public class Main {
	private static final String APP_NAME = "com.mgm.spark-tfidf";
	private static JavaSparkContext context;

	public static void main(String[] args) throws IllegalAccessException, InstantiationException, InvocationTargetException {
		Arguments arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
		
		StorageLevel storageLevel = StorageLevel.MEMORY_ONLY();
		if(arguments.isOffHeap()){
			storageLevel = StorageLevel.OFF_HEAP();
    	}
		
		SparkConf conf = new SparkConf().setAppName(APP_NAME);
		context = new JavaSparkContext(conf);
		
		JavaRDD<String> text = context.textFile(arguments.getInputPath()).persist(storageLevel);

		// split text -> (document, word, 1)
		JavaRDD<Tuple3<String, String, Integer>> docWordNumbers = text.flatMap(new FlatMapFunction<String, Tuple3<String, String, Integer>>() {

			private static final long serialVersionUID = 1L;

			public Iterable<Tuple3<String, String, Integer>> call(String line) throws Exception {
				// document name is on the end of the line in this format -> <document name>
				String[] lineElements = line.split("<");
				List<Tuple3<String, String, Integer>> out = new ArrayList<Tuple3<String, String, Integer>>();
				if (lineElements.length >= 2 && !lineElements[0].equals("")) {
					String docID = lineElements[1].replace(">", ""); // clean the document name
					String docContent = lineElements[0].toLowerCase().replaceAll("[^a-zA-Z ]", "");// clean the text and remove unneeded charts
					for (String word : docContent.split(" ")) { // split text to words
						if (!word.equals("")) {
							out.add(new Tuple3<String, String, Integer>(docID, word, 1));
						}
					}
				}
				return out;
			}
		}).persist(storageLevel);

		// number of documents -> for idf (number of documents)
		Long docNumber = docWordNumbers.map(new Function<Tuple3<String, String, Integer>, String>() {

			private static final long serialVersionUID = 1L;

			public String call(Tuple3<String, String, Integer> input) throws Exception {
				return input._1();
			}
		}).distinct().count();

		// number of words in document -> to normalize tf (document, number of words)
		JavaPairRDD<String, Integer> numberOfWordsInDocuments = docWordNumbers
				.mapToPair(new PairFunction<Tuple3<String, String, Integer>, String, Integer>() {

					private static final long serialVersionUID = 1L;

					public Tuple2<String, Integer> call(Tuple3<String, String, Integer> input) throws Exception {
						return new Tuple2<String, Integer>(input._1(), input._3());
					}

				})
				.persist(storageLevel)
				.reduceByKey(new Function2<Integer, Integer, Integer>() {

					private static final long serialVersionUID = 1L;

					public Integer call(Integer v1, Integer v2) throws Exception {
						return v1 + v2;
					}
				}).persist(storageLevel);

		// tf -> (word, (document, tf)) tf = (number of terms in document) / (number of all words in document)
		JavaPairRDD<String, Tuple2<String, Double>> tf = docWordNumbers
				.mapToPair(new PairFunction<Tuple3<String, String, Integer>, Tuple2<String, String>, Integer>() {

					private static final long serialVersionUID = 1L;

					public Tuple2<Tuple2<String, String>, Integer> call(Tuple3<String, String, Integer> input) throws Exception {
						return new Tuple2<Tuple2<String, String>, Integer>(new Tuple2<String, String>(input._1(), input._2()), input._3());
					}

				}).persist(storageLevel)
				.reduceByKey(new Function2<Integer, Integer, Integer>() {

					private static final long serialVersionUID = 1L;

					public Integer call(Integer v1, Integer v2) throws Exception {
						return v1 + v2;
					}
				}).persist(storageLevel)
				.mapToPair(new PairFunction<Tuple2<Tuple2<String, String>, Integer>, String, Tuple2<String, Integer>>() {

					private static final long serialVersionUID = 1L;

					// key is documentID and value is word with number
					public Tuple2<String, Tuple2<String, Integer>> call(Tuple2<Tuple2<String, String>, Integer> input) throws Exception {
						String outputKey = input._1()._1();
						Tuple2<String, Integer> outputValue = new Tuple2<String, Integer>(input._1()._2(), input._2());
						return new Tuple2<String, Tuple2<String, Integer>>(outputKey, outputValue);
					}

				})
				.persist(storageLevel).join(numberOfWordsInDocuments) // key == documentID
				.mapToPair(new PairFunction<Tuple2<String, Tuple2<Tuple2<String, Integer>, Integer>>, String, Tuple2<String, Double>>() {

					private static final long serialVersionUID = 1L;

					public Tuple2<String, Tuple2<String, Double>> call(Tuple2<String, Tuple2<Tuple2<String, Integer>, Integer>> input)
							throws Exception {
						String documentID = input._1();
						Integer numberOfWordsIndDocument = input._2()._2();
						Tuple2<String, Integer> wordsWithNumber = input._2()._1();
						Double tf = ((double) wordsWithNumber._2() / (double) numberOfWordsIndDocument);

						String outputKey = wordsWithNumber._1();
						Tuple2<String, Double> outputValue = new Tuple2<String, Double>(documentID, tf);

						return new Tuple2<String, Tuple2<String, Double>>(outputKey, outputValue);
					}
				}).persist(storageLevel);

		// idf -> (word, idf) idf = 1+log_e( document number in corpus / number of documents contains word)
		JavaPairRDD<String, Double> idf = docWordNumbers.map(new Function<Tuple3<String, String, Integer>, Tuple2<String, String>>() {

			private static final long serialVersionUID = 1L;

			public Tuple2<String, String> call(Tuple3<String, String, Integer> input) throws Exception {
				// (docId,word)
				return new Tuple2<String, String>(input._1(), input._2());
			}
		}).persist(storageLevel)
				.distinct().mapToPair(new PairFunction<Tuple2<String, String>, String, Integer>() {

			private static final long serialVersionUID = 1L;

			public Tuple2<String, Integer> call(Tuple2<String, String> input) throws Exception {
				// (word,1)
				return new Tuple2<String, Integer>(input._2(), 1);
			}

		}).persist(storageLevel).reduceByKey(new Function2<Integer, Integer, Integer>() {

			private static final long serialVersionUID = 1L;

			public Integer call(Integer v1, Integer v2) throws Exception {
				return v1 + v2;
			}

		}).persist(storageLevel).mapToPair(new IdfComputer(docNumber)).persist(storageLevel);

		// tf-idf -> (document, word, tf-idf) -> tf-idf = tf * idf
		JavaRDD<Tuple3<String, String, Double>> tfidf = tf.leftOuterJoin(idf)
				.flatMap(new FlatMapFunction<Tuple2<String,Tuple2<Tuple2<String,Double>,Optional<Double>>>, Tuple3<String, String, Double>>() {

					private static final long serialVersionUID = 1L;

					public Iterable<Tuple3<String, String, Double>> call(Tuple2<String, Tuple2<Tuple2<String, Double>, Optional<Double>>> input)
							throws Exception {
						List<Tuple3<String, String, Double>> outputCollection = new ArrayList<Tuple3<String, String, Double>>();
						String word = input._1();
						String document = input._2()._1()._1();
						Double tf = input._2()._1()._2();
						Optional<Double> idf = input._2()._2();
						if(idf.isPresent()){
							Tuple3<String, String, Double> output = new Tuple3<String, String, Double>(document, word, tf * idf.get());
							outputCollection.add(output);
						}
						return outputCollection;
					}
		}).persist(storageLevel);

		// write out
		tfidf.map(new Function<Tuple3<String, String, Double>, String>() {

			private static final long serialVersionUID = 1L;

			public String call(Tuple3<String, String, Double> input) throws Exception {
				return input._1() + ", " + input._2() + ", " + Double.toString(input._3());
			}

		}).saveAsTextFile(arguments.getOutputPath());
	}

	/**
	 * compute idf
	 * 
	 * @author bugge
	 * @since 10.05.2016
	 */
	public static class IdfComputer implements PairFunction<Tuple2<String, Integer>, String, Double> {
		private static final long serialVersionUID = 1L;

		private Long numberOfDocuments = 0L;

		public IdfComputer() {

		}

		/**
		 * 
		 * @param numberOfDocuments
		 *            ->is the number of documents in corpus
		 */
		public IdfComputer(Long numberOfDocuments) {
			this.numberOfDocuments = numberOfDocuments;
		}

		public Tuple2<String, Double> call(Tuple2<String, Integer> input) throws Exception {
			if (numberOfDocuments == 0L) {
				return new Tuple2<String, Double>(input._1(), 0.0);
			}
			return new Tuple2<String, Double>(input._1(), idf(input._2(), numberOfDocuments));
		}

		/**
		 * compute idf like 1+log_e( document number in corpus / number of documents contains word)
		 * 
		 * @param numberOFDocumentsContainsWord
		 *            -> document number in corpus
		 * @param maxNumberDocuments
		 *            -> number of documents contains word
		 * @return idf
		 */
		private Double idf(Integer numberOFDocumentsContainsWord, Long maxNumberDocuments) {
			if (numberOFDocumentsContainsWord > 0) {
				return new Double(1 + Math.log((double) maxNumberDocuments / (double) numberOFDocumentsContainsWord));
			} else {
				return new Double(1);
			}

		}
	}
}
