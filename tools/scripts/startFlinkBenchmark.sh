#!/bin/bash

echo ""
echo ""
echo "#####################"
echo "### Flink HDFS ######"
echo "#####################"
echo ""
echo "setup and enable HDFS"
echo "press any key to START BENCHMARK!"
read

sh Flink/HDFS/startFlinkHDFSBenchmark.sh

echo ""
echo ""
echo "#############################"
echo "### Flink Alluxio Through ###"
echo "#############################"
echo ""
echo "setup and enable Alluxio! disable HDFS"
echo "press any key to START BENCHMARK!"
read

sh Flink/Alluxio/startFlinkAlluxioThroughBenchmark.sh

echo ""
echo ""
echo "################################"
echo "### Flink Alluxio Must Cache ###"
echo "################################"
echo ""
echo "setup and restart Alluxio!"
echo "press any key to START BENCHMARK!"
read
sh Flink/Alluxio/startFlinkAlluxioMustCacheBenchmark.sh

echo ""
echo ""
echo "################################"
echo "### Flink Alluxio Only ###"
echo "################################"
echo ""
echo "setup and restart Alluxio!, Disable HDFS"
echo "press any key to START BENCHMARK!"
read
sh Flink/Alluxio/startFlinkAlluxioBenchmark.sh
