package com.mgm.benchmark_evaluator.models;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ResultsElement;

public class R2OperationsMinMax {

	private final String resultFolderPath;
	private final String fileNameNumber;
	private final String fileName;
	private final String computerName;
	private final String iterationName;
	private final String benchmarkName;
	private final static Logger LOGGER = Logger.getLogger(R2OperationsMinMax.class.getName());
	public static boolean WITH_COLOR = true;

	public R2OperationsMinMax(String benchmarkName, String resultFolderPath, String iterationName, String computerName, String fileName) {
		this.resultFolderPath = resultFolderPath;
		this.fileNameNumber = fileName.split("_")[0];
		this.fileName = fileName.split("_")[1].replaceAll("[^a-zA-Z0-9 ]","").replace(" ","_");
		this.computerName = computerName;
		this.iterationName = iterationName;
		this.benchmarkName = benchmarkName;
	}

	public void write(Iteration1ResultsElement results) {

		try {
			writeR(results);
			writeSVG();
		} catch (IOException e) {
			LOGGER.error(e);
		}
	}

	private void writeR(Iteration1ResultsElement results) throws IOException {
		List<String> lines;
		if(WITH_COLOR){
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT3COLOR_TEMPLATE_PATH);
		}else{
			lines = FileFolderOperations.readFile(DefaultValues.R_PLOT3_TEMPLATE_PATH);
		}
		List<String> outputLines = new ArrayList<String>();
		for (String line : lines) {
			line = line.replace(StringTemplates.R2_OURTPUTPATH_KEY, getSvgOutputPath(benchmarkName))
					.replace(StringTemplates.R2_TITLEBOTTOM_KEY, getTitleBottom()).replace(StringTemplates.R2_YTITLE_KEY, getYTitle(results))
					.replace(StringTemplates.R2_WITHSTD_KEY, getWithStd()).replace(StringTemplates.R2_TITLETOP_KEY, getTitleTop())
					.replace(StringTemplates.R2_GRAPHNAMES_KEY, getGraphNames(results))
					.replace(StringTemplates.R2_GRAPHVALUES_KEY, getGraphValues(results))
					.replace(StringTemplates.R2_STDVALUES_KEY, getSDValues(results));
			outputLines.add(line);
		}
		FileFolderOperations.createFolderIfNotExist(FileFolderOperations.getFolderPathFromFilePath(getRFilePath(benchmarkName)));
		FileFolderOperations.writeFile(getRFilePath(benchmarkName), outputLines);
	}

	private String getRFilePath(String benchmarkName) {
		String folderPath = Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
		return Paths.get(folderPath, DefaultValues.R_TEMPFOLDERNAME, fileNameNumber + DefaultValues.MIN_MAX_FILES + DefaultValues.R_ENDING).toString();
	}

	private String getGraphNames(Iteration1ResultsElement results) {
		String retunValue = "\"" + DefaultValues.R_SCALEFACTOR_TITLE + "\",";
		retunValue += "\"" + DefaultValues.R_MAX_TITLE + "\",";
		retunValue += "\"" + DefaultValues.R_MEAN_TITLE + "\",";
		retunValue += "\"" + DefaultValues.R_MIN_TITLE + "\"";
		return retunValue;
	}

	private String getTitleTop() {
		
		String[] benchmarkNameSplits = benchmarkName.split("_");
		String title = StringTemplates.getTestCaseTitle(benchmarkNameSplits[0])
				+" "+StringTemplates.getUpperFirstChar(benchmarkNameSplits[1])
				+" "+StringTemplates.getAlgorithm(benchmarkNameSplits[2])+"\\n";
		if(computerName.equals(DefaultValues.COMPUTER_SUMMARYNAME)){
			String chartName = getSummaryChartTitle();
			return title+chartName;
		}
		return title+computerName;
	}
	
	private String getSummaryChartTitle() {
		String chartName = "";
		if(fileNameNumber.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.TIME_CHARTTITLE;
		}else if(fileNameNumber.equals(Integer.toString(DefaultValues.TIME_DEFAULTINDX))){
			chartName=DefaultValues.PERFORMANCE_CHARTTITLE;
		}
		return chartName;
	}
	
	private String getWithStd() {
		return "FALSE";
	}

	private String getTitleBottom() {
		return "";
	}

	private String getYTitle(Iteration1ResultsElement results) {
		String unit = "";
		if (!results.getUnit().equals("")) {
			unit = " [" + results.getUnit() + "]";
		}
		if (results.getName().indexOf("_") < 0) {
			return results.getName() + unit;
		}
		return results.getName().substring(results.getName().indexOf("_"), results.getName().length()) + unit;
	}

	private String getSvgOutputPath(String benchmarkName) {
		String scenario = "_"+getTitleTop().replace(" ", "_").replace("\\n", "_");
		String folderPath = Paths.get(resultFolderPath, iterationName, benchmarkName, computerName).toString();
		return Paths.get(folderPath, fileNameNumber +"_"+fileName+"_"+ DefaultValues.MIN_MAX_FILES +scenario+ DefaultValues.PDF_ENDING).toString();
	}

	private String getGraphValues(Iteration1ResultsElement results) {

		String returnScalFactorString = "sf=c(";
		String returnMinString = "min=c(";
		String returnMeanString = "mean=c(";
		String returnMaxString = "max=c(";

		for (Iteration1ResultElement result : results.getResults()) {
			returnMaxString += result.getMaxAsString().replace(".", "").replace(",", ".") + ",";
			returnMeanString += result.getMeanAsString().replace(".", "").replace(",", ".") + ",";
			returnMinString += result.getMinAsString().replace(".", "").replace(",", ".") + ",";
			returnScalFactorString += result.getScaleFactorAsString().replace(".", "").replace(",", ".") + ",";
		}
		if (returnScalFactorString.endsWith(",")) {
			returnMaxString = returnMaxString.substring(0, returnMaxString.length() - 1);
			returnMeanString = returnMeanString.substring(0, returnMeanString.length() - 1);
			returnMinString = returnMinString.substring(0, returnMinString.length() - 1);
			returnScalFactorString = returnScalFactorString.substring(0, returnScalFactorString.length() - 1);
		}
		returnMaxString += ")";
		returnMeanString += ")";
		returnMinString += ")";
		returnScalFactorString += ")";

		return returnScalFactorString + ", " + returnMaxString + ", " + returnMeanString + ", " + returnMinString;
	}

	private String getSDValues(Iteration1ResultsElement results) {
		String returnString = "sd=c(NA)";
		return returnString;
	}

	private void writeSVG() {
		RscriptCommandLine rsciptCommand = new RscriptCommandLine(getRFilePath(benchmarkName));
		rsciptCommand.run();
	}
}
