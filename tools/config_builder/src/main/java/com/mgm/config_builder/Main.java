package com.mgm.config_builder;

import java.nio.file.Paths;

import com.mgm.config_builder.controller.ConfigManager;
import com.mgm.config_builder.controller.PropertiesManager;
import com.mgm.config_builder.model.DefaultValues;

public class Main 
{
    public static void main( String[] args )
    {
    	boolean error = false;
    	String propertiesPath = Paths.get(DefaultValues.PROPERTIES_FILE_PATH).toAbsolutePath().toString();
    	PropertiesManager properties = new PropertiesManager(propertiesPath,args);
    	error = properties.loadAndConvertPropertiesFromFile();
    	if(!error){
    		ConfigManager config = new ConfigManager(properties.getProperties());
    		config.build();
    		
    	}
    }
}
