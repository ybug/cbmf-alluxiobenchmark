package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Iteration1RoundHostsElement {
	
	private Map<Integer,Boolean> error = new HashMap<Integer,Boolean>();
	private Map<Integer,List<Double>> items = new HashMap<Integer,List<Double>>();
	private Map<Integer,List<Long>> measurementPointNumbers = new HashMap<Integer,List<Long>>();

	public Map<Integer, List<Double>> getItems() {
		return items;
	}

	public void setItems(Map<Integer, List<Double>> items) {
		this.items = items;
	}
	
	public void addItem(Integer itemID,Double averageValue){
		if(items.containsKey(itemID)){
			items.get(itemID).add(averageValue);
			
		}else {
			List<Double> averageValues = new ArrayList<Double>();
			averageValues.add(averageValue);
			items.put(itemID, averageValues);
		}
	}
	
	public boolean isError(Integer itemID) {
		if(error.containsKey(itemID)){
			return true;
		}else {
			return false;
		}
	}
	
	public void addMesurmentPointNumber(Integer itemID,Long mesurmentPointNumber){
		if(items.containsKey(itemID)){
			measurementPointNumbers.get(itemID).add(mesurmentPointNumber);
			
		}else {
			List<Long> mesurmentPointNumberValues = new ArrayList<Long>();
			mesurmentPointNumberValues.add(mesurmentPointNumber);
			measurementPointNumbers.put(itemID, mesurmentPointNumberValues);
		}
	}
	
	public Map<Integer, List<Long>> getMesurmentPointNumbers() {
		return measurementPointNumbers;
	}
	
	public void setError(Integer itemID) {
		error.put(itemID, true);
	}

	public int getLength(Integer itemID){
		return items.get(itemID).size();
	}
}
