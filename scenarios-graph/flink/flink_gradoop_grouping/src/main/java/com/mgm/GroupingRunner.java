/*
 * This file is part of Gradoop.
 *
 * Gradoop is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gradoop is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Gradoop. If not, see <http://www.gnu.org/licenses/>.
 */

package com.mgm;

import org.apache.flink.api.common.ProgramDescription;
import org.gradoop.flink.model.impl.operators.grouping.Grouping;
import org.gradoop.flink.model.impl.operators.grouping.GroupingStrategy;
import org.gradoop.flink.model.impl.operators.grouping.functions.aggregation.CountAggregator;

/**
 * A dedicated program for parametrized graph grouping.
 */
public class GroupingRunner extends AbstractRunner implements
  ProgramDescription {

  /**
   * Returns the grouping operator implementation based on the given strategy.
   *
   * @param useVertexLabels       use vertex label for grouping, true/false
   * @param useEdgeLabels         use edge label for grouping, true/false
   * @return grouping operator implementation
   */
  public static Grouping getOperator(boolean useVertexLabels, boolean useEdgeLabels) {
    return new Grouping.GroupingBuilder()
      .setStrategy(GroupingStrategy.GROUP_REDUCE)
      .useVertexLabel(useVertexLabels)
      .useEdgeLabel(useEdgeLabels)
      .addVertexAggregator(new CountAggregator())
      .addEdgeAggregator(new CountAggregator())
      .build();
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String getDescription() {
    return GroupingRunner.class.getName();
  }
}
