package com.mgm.benchmark_evaluator.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.mgm.benchmark_evaluator.elements.inputjson.BenchmarkElement;
import com.mgm.benchmark_evaluator.elements.inputjson.ItemInformationElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration1.Iteration1ScenarioResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration3.Iteration3BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.elements.iteration4.Iteration4BenchmarkResultsElement;
import com.mgm.benchmark_evaluator.models.StringTemplates;

public class CompressorController {

	private final static Logger LOGGER = Logger.getLogger(CompressorController.class.getName());
	
	private final InputController input;
	private final Map<Integer,ItemInformationElement> itemsInformation = new HashMap<Integer,ItemInformationElement>(); 
	
	public CompressorController(InputController input) {
		this.input = input;
	}
	
	public void compress(){
		iteration4(iteration3(iteration2(iteration1())));
	}
	
	private Iteration1BenchmarkResultsElement iteration1(){
		LOGGER.info(StringTemplates.outputStartFirstIteration);
		Iteration1BenchmarkResultsElement benchmarkResults = new Iteration1BenchmarkResultsElement();
		for(Entry<String,String> inputFile : input.getInputFilePaths().entrySet()){
			BenchmarkElement benchmark = input.getResult(inputFile);
			if(benchmark.isValid()){
				addItemInformations(benchmark);
				Iteration1 iteratino1 = new Iteration1(benchmark,itemsInformation);
				Iteration1ScenarioResultsElement it1 = iteratino1.compute();
				if(it1.getComputer().keySet().size() > 0){
					benchmarkResults.getBenchmarks().put(inputFile.getKey(),it1);
				}
			}
		}
		return benchmarkResults;
	}
	
	private Iteration1BenchmarkResultsElement iteration2(Iteration1BenchmarkResultsElement benchmarks){
		LOGGER.info(StringTemplates.outputStartIteration2);
		Iteration2 iteration2 = new Iteration2(benchmarks,itemsInformation);
		return iteration2.compute();
	}
	
	private Iteration3BenchmarkResultsElement iteration3(Iteration1BenchmarkResultsElement benchmarks){
		LOGGER.info(StringTemplates.outputStartIteration3);
		Iteration3 iteration3 = new Iteration3(benchmarks);
		return iteration3.compute();
	}
	
	private Iteration4BenchmarkResultsElement iteration4(Iteration3BenchmarkResultsElement benchmarks){
		LOGGER.info(StringTemplates.outputStartIteration4);
		Iteration4 iteration4 = new Iteration4(benchmarks);
		return iteration4.compute();
	}
	
	
	private void addItemInformations(BenchmarkElement benchmark){
		for(Entry<Integer, ItemInformationElement> itemInformation : benchmark.getBenchmarkResults().getItemsInformation().entrySet()){
			if(!itemsInformation.containsKey(itemInformation.getKey())){
				itemsInformation.put(itemInformation.getKey(), itemInformation.getValue());
			}
		}
	}
}
