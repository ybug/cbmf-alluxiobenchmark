package com.mgm.config_builder.model;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

/**
 * This class load the properties from file
 * @author bugge
 * @since 21.01.2016
 */
public class PropertiesOperations {
	
	private final static Logger LOGGER = Logger.getLogger(PropertiesOperations.class.getName());
	
	private final Properties properties = new Properties();
	private InputStream input = null;
	private String propertiesPath = null;
	
	/**
	 * @param propertiesPath -> is the path to the properties file
	 */
	public PropertiesOperations(String propertiesPath){
		this.propertiesPath = propertiesPath;
	}
	
	/**
	 * 
	 * @return return all keys
	 */
	public Set<Object> getAllKeys(){
		return this.properties.keySet();
	}
	
	/**
	 * return the property for key
	 * @param key -> is the key to select the value from the properties
	 * @param defaultValue -> if don't exist key, then will return defaultValue
	 * @return the value from the key or defaultValue
	 */
	public String getProperty(String key, String defaultValue){
		return properties.getProperty(key, defaultValue);
	}
	
	/**
	 * load the properties from file 
	 * @return if occur a error, then return true 
	 */
	public boolean loadProperties(){
		try {
			this.input = new FileInputStream(this.propertiesPath);
			this.properties.load(input);
			
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
			return true;
		} finally {
			if (this.input != null) {
				try {
					this.input.close();
				} catch (IOException e) {
					LOGGER.error(e.getMessage(), e);
					return true;
				}
			}
		}
		return false;
	}
}
