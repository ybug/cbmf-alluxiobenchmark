package com.mgm.comparator_wrapper.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

public class HDFSOperations {
	
	private final static Logger LOGGER = Logger.getLogger(HDFSOperations.class.getName()); 

	private final FileSystem hdfs;
	private static final int MAX_READ_FILE_LINES_PER_FILE = 10;
	
	public static List<String> getFolderContentAndCleanup(String fsUri,String folderUri){
		Configuration configuration = new Configuration();
		try {
			LOGGER.info("Read from Hdfs compare result-> "+folderUri);
			HDFSOperations hdfsOperations = new HDFSOperations(configuration,fsUri);
			List<String> lines = hdfsOperations.readFolder(folderUri);
			//hdfsOperations.deleteFolder(folderUri);
			hdfsOperations.close();
			return lines;
		} catch (IOException e) {
			LOGGER.error(e.getMessage(),e);
		} catch (URISyntaxException e) {
			LOGGER.error(e.getMessage(),e);
		} 
		
		return null;
	}
	
	public HDFSOperations(Configuration configuration, String fsUri) throws IOException, URISyntaxException {
		this.hdfs = FileSystem.get(new URI(fsUri),configuration);
	}

	
	public List<String> readFolder(String folderUri) throws IllegalArgumentException, IOException{
		List<String> folderContent = new ArrayList<String>();
		Path folderPath = new Path(folderUri);
		if(hdfs.isDirectory(folderPath)){
			FileStatus[] folderStatus = hdfs.listStatus(folderPath);
			for(Path filePath : getImportantFile(folderStatus)){
				folderContent.addAll(readFile(filePath));
			}
			return folderContent;
		}
		return folderContent;
	}
	
	public void deleteFolder(String folderUri) throws IllegalArgumentException, IOException {
		hdfs.delete(new Path(folderUri), true);
	}
	
	private List<Path> getImportantFile(FileStatus[] folderStatus){
		List<Path> importantFilePaths = new ArrayList<Path>();
		for(FileStatus fileStatus : folderStatus){
			Path filePath = fileStatus.getPath();
			if(isImportantFile(filePath.getName())){
				importantFilePaths.add(filePath);
			}
			
		}
		return importantFilePaths;
	}
	
	private boolean isImportantFile(String fileName){
		boolean important = false;
		//flink important files are 1-n
		try{
			Integer.parseInt(fileName);
			important=true;
		}catch(NumberFormatException e){
		}
		//spark important file -> part-00000
		if(fileName.startsWith("part-")){
			important=true;
		}
		return important;
	}
	
	private List<String> readFile(Path filePath) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(hdfs.open(filePath)));
        List<String> fileLines = new ArrayList<String>();
        String line = "";
        int counter = 0;
        LOGGER.info("read lines");
        line=br.readLine();
        while (line != null && counter < MAX_READ_FILE_LINES_PER_FILE){
        	if(!line.equals("")){
        		counter++;
        		LOGGER.info(line);
                fileLines.add(line);
        	}
        	line=br.readLine();
        }
        return fileLines;
	}
	
	
	public void close() throws IOException{
		hdfs.close();
	}
}
