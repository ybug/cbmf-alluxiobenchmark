package com.mgm.benchmark_evaluator.elements.iteration1;

import java.util.HashMap;
import java.util.Map;

public class Iteration1RoundsResultElement {
	
	private final int roundNumber;
	private Map<Integer,Iteration1RoundHostsElement> hosts = new HashMap<Integer,Iteration1RoundHostsElement>();
	
	public Iteration1RoundsResultElement(int roundNumber) {
		this.roundNumber = roundNumber;
	}

	public Map<Integer, Iteration1RoundHostsElement> getHosts() {
		return hosts;
	}

	public void setHosts(Map<Integer, Iteration1RoundHostsElement> hosts) {
		this.hosts = hosts;
	}
	
	public void addAverageValue(Integer hostID, Integer itemID, Double averageValue, long numberOfMeasurmendPoints){
		if(hosts.containsKey(hostID)){
			hosts.get(hostID).addMesurmentPointNumber(itemID, numberOfMeasurmendPoints);
			hosts.get(hostID).addItem(itemID, averageValue);
		}else{
			Iteration1RoundHostsElement host = new Iteration1RoundHostsElement();
			host.addMesurmentPointNumber(itemID, numberOfMeasurmendPoints);
			host.addItem(itemID, averageValue);
			hosts.put(hostID, host);
		}
	}
	
	public void addError(Integer hostID, Integer itemID){
		if(hosts.containsKey(hostID)){
			hosts.get(hostID).setError(itemID);
		}else{
			Iteration1RoundHostsElement host = new Iteration1RoundHostsElement();
			host.setError(itemID);
			hosts.put(hostID, host);
		}
	}

	public int getRoundNumber() {
		return roundNumber;
	}
}
