package com.mgm.benchmark_evaluator.elements.iteration3;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Iteration3ResultAtomElement{

	private double min;
	private double max;
	private double mean;
	private double standardDeviation;
	private String technologyName;
	private double qualityInPercent;
	private double qualityInPercentSTD;
	private List<Double> elements = new ArrayList<Double>();
	
	public double getQualityInPercentSTD() {
		return qualityInPercentSTD;
	}
	public String getQualityInPercentSTDAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", qualityInPercentSTD);
	}
	public void setQualityInPercentSTD(double qualityInPercentSTD) {
		this.qualityInPercentSTD = qualityInPercentSTD;
	}
	public double getQualityInPercent() {
		return qualityInPercent;
	}
	public String getQualityInPercentAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", qualityInPercent);
	}
	public void setQualityInPercent(double qualityInPercent) {
		this.qualityInPercent = qualityInPercent;
	}
	public double getMin() {
		return min;
	}
	public String getMinAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", min);
	}
	
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public String getMaxAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", max);
	}
	public void setMax(double max) {
		this.max = max;
	}
	public double getMean() {
		return mean;
	}
	public String getMeanAsString(){
		return String.format(Locale.GERMAN, "%1$,.4f", mean);
	}
	public void setMean(double mean) {
		this.mean = mean;
	}
	public double getStandardDeviation() {
		return standardDeviation;
	}
	public String getStandardDeviationAsString() {
		return String.format(Locale.GERMAN, "%1$,.4f", standardDeviation);
	}
	public String getStandardDeviationAsStringInPercent(){
		return String.format(Locale.GERMAN, "%1$,.4f",(((double) 100 / this.mean)*this.standardDeviation));
	}
	public void setStandardDeviation(double standardDeviation) {
		this.standardDeviation = standardDeviation;
	}
	
	public List<Double> getElements() {
		return elements;
	}
	
	public List<String> getElementsAsStrings(){
		List<String> elementsAsString = new ArrayList<String>();
		
		for(Double element: elements){
			elementsAsString.add(String.format(Locale.GERMAN, "%1$,.4f", element));
		}
		
		return elementsAsString;
	} 
	
	public String getTechnologyName() {
		return technologyName;
	}
	public void setTechnologyName(String technologyName) {
		this.technologyName = technologyName;
	}
	public void setElements(List<Double> elements) {
		this.elements = elements;
	}
	public boolean isEmpty(){
		return elements.isEmpty();
	}
	@Override
	public String toString() {
		return "Iteration3ResultAtomElement [mean=" + mean + ", technologyName=" + technologyName + "]";
	}
}
