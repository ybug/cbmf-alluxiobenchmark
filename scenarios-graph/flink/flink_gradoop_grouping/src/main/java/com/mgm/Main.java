package com.mgm;


/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.flink.api.java.utils.ParameterTool;
import org.gradoop.flink.model.impl.LogicalGraph;
import org.gradoop.flink.model.impl.operators.grouping.Grouping;

public class Main {
	
	public static void main(String[] args) throws Exception {
		
		final ParameterTool params = ParameterTool.fromArgs(args);
		String inputPath = params.get("input",null);
		String outputPath = params.get("output", null);
		
		if (inputPath == null || outputPath == null) {
			System.err.println("input or output not set in the arguments!");
			return;
		}
		
		if(!inputPath.endsWith("/")){
			inputPath+="/";
		}
		if(!outputPath.endsWith("/")){
			outputPath+="/";
		}

	    boolean useVertexLabels = true;
	    boolean useEdgeLabels = true;

	    // initialize EPGM database
	    LogicalGraph graphDatabase = GroupingRunner.readLogicalGraph(inputPath, false);

	    // initialize grouping method
	    Grouping grouping = GroupingRunner.getOperator(useVertexLabels, useEdgeLabels);
	    // call grouping on whole database graph
	    LogicalGraph summarizedGraph = graphDatabase.callForGraph(grouping);

	    if (summarizedGraph != null) {
	    	GroupingRunner.writeLogicalGraph(summarizedGraph, outputPath);
	    } else {
	      System.err.println("wrong parameter constellation");
	    }
	}
}
