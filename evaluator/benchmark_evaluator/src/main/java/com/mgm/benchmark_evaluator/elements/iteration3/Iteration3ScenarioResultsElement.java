package com.mgm.benchmark_evaluator.elements.iteration3;

import java.util.HashMap;
import java.util.Map;

public class Iteration3ScenarioResultsElement {
	
	private String name;
	
	private final Map<String,Iteration3ComputerResultsElement> computer = new HashMap<String,Iteration3ComputerResultsElement>();
	
	public Iteration3ScenarioResultsElement(String name) {
		this.name = name;
	}
	public Map<String, Iteration3ComputerResultsElement> getComputer() {
		return computer;
	}

	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "Iteration3ScenarioResultsElement [name=" + name + ", computer=" + computer + "]";
	}
	
}
