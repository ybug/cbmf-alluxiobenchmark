package com.mgm.spark_terasort.extern.eastcirclek.total_order_partitioner;

import java.io.IOException;
import java.io.PrintStream;

import org.apache.hadoop.io.Text;

public abstract class TrieNode {
	private int level;

	public TrieNode(int level) {
		this.level = level;
	}

	public abstract int findPartition(Text key);

	public abstract void print(PrintStream strm) throws IOException;

	public int getLevel() {
		return level;
	}
}