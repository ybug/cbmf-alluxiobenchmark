package com.mgm.benchmark_evaluator.elements.inputjson;

public class BenchmarkElement {
	private String name = null;
	private BenchmarkResultsElement benchmarkResults = null;
	
	public BenchmarkElement(String name, BenchmarkResultsElement bechmarkResults) {
		this.name = name;
		this.benchmarkResults = bechmarkResults;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BenchmarkResultsElement getBenchmarkResults() {
		return benchmarkResults;
	}
	public void setBenchmarkResults(BenchmarkResultsElement benchmarkResults) {
		this.benchmarkResults = benchmarkResults;
	}
	
	public boolean isValid() {
		return benchmarkResults != null;
	}
	
	@Override
	public String toString() {
		return "BenchmarkElement [name=" + name + "]";
	}
}
