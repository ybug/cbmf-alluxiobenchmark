package com.mgm.spark_sorted_evaluation;

import java.lang.reflect.InvocationTargetException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.google.common.base.Optional;

import scala.Tuple2;

/**
 * Hello world!
 *
 */
public class Main 
{
	private static final String APP_NAME = "com.mgm.spark-sorted-evaluation";
	private static JavaSparkContext context;
	
	public final static boolean EQUAL = true;
	public final static boolean NOT_EQUAL = false;
	public final static String EQUAL_TEXT = "Equal:";
	public final static String NOT_EQUAL_TEXT = "Not Equal:";
    public static void main( String[] args ) throws IllegalAccessException, InstantiationException, InvocationTargetException
    {
    	Arguments arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
    	SparkConf conf = new SparkConf().setAppName(APP_NAME).setMaster(arguments.getMasterHost());
    	JobConf mapRedConf = new JobConf();
    	context = new JavaSparkContext(conf);
    	
    	JavaPairRDD<LongWritable,String> referenceDataSet= context
    	.newAPIHadoopFile(arguments.getInputRefPath(),TextInputFormat.class,LongWritable.class, Text.class,mapRedConf)
    	.mapToPair(new ConvertTextToString());
    	
    	JavaPairRDD<LongWritable,String> checkDataSet= context
    	    	.newAPIHadoopFile(arguments.getInputCheckPath(),TextInputFormat.class,LongWritable.class, Text.class,mapRedConf)
    	    	.mapToPair(new ConvertTextToString());
    	
    	referenceDataSet.fullOuterJoin(checkDataSet)
    	.mapToPair(new PairFunction<Tuple2<LongWritable,Tuple2<Optional<String>,Optional<String>>>, Boolean,Long>() {

			private static final long serialVersionUID = 1L;

			public Tuple2<Boolean,Long> call(Tuple2<LongWritable, Tuple2<Optional<String>, Optional<String>>> v1) throws Exception {
				
				Boolean isEqual = EQUAL;
				String first = v1._2._1.get();
				String second = v1._2._1.get();
				if(second == null || first == null){
					isEqual = NOT_EQUAL;
				}
				if (!first.equals(second)) {
					isEqual =  NOT_EQUAL;
				}
				return new Tuple2<Boolean,Long>(isEqual,1L);
			}
		})
    	.reduceByKey(new CompressDataSetLong())
    	.map(new Function<Tuple2<Boolean,Long>,String>(){

			private static final long serialVersionUID = 1L;

			public String call(Tuple2<Boolean, Long> value) throws Exception {
				if(value._1() == EQUAL){
					return EQUAL_TEXT+Long.toString(value._2());
				}else {
					return NOT_EQUAL_TEXT+Long.toString(value._2());
				}
			}
    		
    	})
    	.saveAsTextFile(arguments.getOutputPath());
    	
    	
    }
    
    public static class ConvertTextToString implements PairFunction<Tuple2<LongWritable,Text>,LongWritable,String>{
    	private static final long serialVersionUID = 1L;
		//convert Tuple2<String,Integer> to JavaPairRDD<String,Integer> 
		public Tuple2<LongWritable, String> call(Tuple2<LongWritable,Text> value) throws Exception {
			return new Tuple2<LongWritable, String>(value._1(), value._2().toString());
		}
    }
    
    public static class CompressDataSetLong implements Function2<Long,Long,Long>{
    	private static final long serialVersionUID = 1L;
		//add all occurrence from each word (add all values from same key)
		public Long call(Long v1, Long v2) throws Exception {
			return v1+v2;
		}
    }
}
