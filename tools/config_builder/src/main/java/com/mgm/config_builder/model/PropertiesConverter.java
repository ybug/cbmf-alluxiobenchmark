package com.mgm.config_builder.model;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.github.jankroken.commandline.CommandLineParser;
import com.github.jankroken.commandline.OptionStyle;
import com.mgm.config_builder.elements.Arguments;
import com.mgm.config_builder.elements.PropertiesElement;

/**
 * Convert the properties to PropertiesElement format
 * 
 * @author bugge
 * @since 21.01.2016
 */
public class PropertiesConverter {

	private final static Logger LOGGER = Logger.getLogger(PropertiesConverter.class.getName());

	private Map<String, String> propertiesMap = new HashMap<String, String>();
	private String propertiesPath;
	private PropertiesElement propertiesElement;

	/**
	 * 
	 * @param propertiesPath
	 *            -> is the file path to the properties
	 */
	public PropertiesConverter(String propertiesPath) {
		this.propertiesPath = propertiesPath;
		this.propertiesElement = new PropertiesElement(this.propertiesPath);
	}

	/**
	 * 
	 * @param propertiesMap
	 *            -> is the key value map in with key is name of the property and value is the content from the property
	 */
	public void setPropertiesMap(Map<String, String> propertiesMap) {
		this.propertiesMap = propertiesMap;
	}

	/**
	 * convert from map to PropertiesElement
	 */
	public void convert(String[] args) {
		this.propertiesElement = new PropertiesElement(this.propertiesPath);

		for (Entry<String, String> entry : this.propertiesMap.entrySet()) {
			if(entry.getKey().equals(StringTemplates.COPIE_NUMBER_KEY)){
				this.propertiesElement.setNumberOfCopies(Integer.parseInt(entry.getValue()));
			}else if(entry.getKey().equals(StringTemplates.REPLACE_COPIE_NUMBER_KEY)){
				this.propertiesElement.setReplaceCopyNumberString(entry.getValue());
			}else{
				this.propertiesElement.addProperty(entry.getKey(), entry.getValue());
			}
		}
		setArgumentsInProperties(args);
	}

	private void setArgumentsInProperties(String[] args){
		Arguments arguments = new Arguments();
		try {
			arguments = CommandLineParser.parse(Arguments.class, args, OptionStyle.SIMPLE);
		} catch (IllegalAccessException e) {
			LOGGER.error(e);
		} catch (InstantiationException e) {
			LOGGER.error(e);
		} catch (InvocationTargetException e) {
			LOGGER.error(e);
		}
		this.propertiesElement.setTemplateFolderPath(arguments.getTemplateFolderPath());
		this.propertiesElement.setTargetFolderPath(arguments.getTargetFolderPath());
	}
	/**
	 * 
	 * @return the converted PropertiesElememt
	 */
	public PropertiesElement getPropertiesElement() {
		return this.propertiesElement;
	}
}
