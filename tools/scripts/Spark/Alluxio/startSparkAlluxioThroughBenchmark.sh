#!/bin/bash

echo "#"
echo "### Spark AlluxioThrough Wordcount"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughWordcountBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughWordcountBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughWordcountBenchmark.sh _part3
sh Spark/Alluxio/Through/startSparkAlluxioThroughWordcountBenchmark.sh _part4
sh Spark/Alluxio/Through/startSparkAlluxioThroughWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioThrough Copy"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughCopyBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughCopyBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughCopyBenchmark.sh _part3
sh Spark/Alluxio/Through/startSparkAlluxioThroughCopyBenchmark.sh _part4
sh Spark/Alluxio/Through/startSparkAlluxioThroughCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioThrough Terasort"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughTeraBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughTeraBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughTeraBenchmark.sh _part3
sh Spark/Alluxio/Through/startSparkAlluxioThroughTeraBenchmark.sh _part4
sh Spark/Alluxio/Through/startSparkAlluxioThroughTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark AlluxioThrough TF-IDF"
echo "#"

sh Spark/Alluxio/Through/startSparkAlluxioThroughTfidfBenchmark.sh _part1
sh Spark/Alluxio/Through/startSparkAlluxioThroughTfidfBenchmark.sh _part2
sh Spark/Alluxio/Through/startSparkAlluxioThroughTfidfBenchmark.sh _part3
#sh Spark/Alluxio/Through/startSparkAlluxioThroughTfidfBenchmark.sh _part4
#sh Spark/Alluxio/Through/startSparkAlluxioThroughTfidfBenchmark.sh _part5
