package com.mgm.benchmark_evaluator.models;

import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import com.google.common.primitives.Doubles;

public class MathOperations {

	private DescriptiveStatistics stats = new DescriptiveStatistics();
	//private List<Double> elements = new ArrayList<Double>();
	
	public void addDoubleValue(double value){
		stats.addValue(value);
	}
	
	public void addFloatValue(float value){
		stats.addValue((double)value);
	}
	
	public void addLongValue(long value){
		stats.addValue((double)value);
	}
	
	public double getMin(){
		return stats.getMin();
	}
	
	public double getStandardDeviation(){
		return stats.getStandardDeviation();
	}
	
	public double getMax(){
		return stats.getMax();
	}
	
	public double getMean(){
		return stats.getMean();
	}
	
	public long getNumberElements(){
		return stats.getN();
	}
	
	public void reset(){
		stats.clear();
	}
	
	public List<Double> getElements() {
		return Doubles.asList(stats.getValues());
	}
}
