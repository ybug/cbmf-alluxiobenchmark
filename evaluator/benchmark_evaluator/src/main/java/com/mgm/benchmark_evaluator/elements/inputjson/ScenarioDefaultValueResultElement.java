package com.mgm.benchmark_evaluator.elements.inputjson;

/**
 * Save one default value for one scenario (like data set space)
 * @author bugge
 * @since 04.05.2016
 */
public class ScenarioDefaultValueResultElement {

	private String id = null;
	private String name = null;
	private String valueString = null;
	private Long valueLong = null;
	private Double valueDouble = null;
	private String unit = null;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValueString() {
		return valueString;
	}
	public void setValueString(String valueString) {
		this.valueString = valueString;
	}
	public Long getValueLong() {
		return valueLong;
	}
	public void setValueLong(Long valueLong) {
		this.valueLong = valueLong;
	}
	public Double getValueDouble() {
		return valueDouble;
	}
	public void setValueDouble(Double valueDouble) {
		this.valueDouble = valueDouble;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public boolean existValue(){
		if(valueString!=null){
			return true;
		}else if( valueLong != null){
			return true;
		}else if( valueDouble != null){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public String toString() {
		return "ScenarioDefaultValueResultElement [id=" + id + ", name=" + name
				+ ", valueString=" + valueString + ", valueLong=" + valueLong + ", valueDouble=" + valueDouble
				+ ", unit=" + unit + "]";
	}	
}
