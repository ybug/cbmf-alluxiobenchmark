package com.mgm.comparator_wrapper.model;

public class ArgumentsOperations {

	private final String[] args;
	private ArgumentsElement result;
	
	public enum ARGS_POSITION{
		REFPATH ("refPath",0),
		REPORTPATH ("reportPath",1),
		RESULTPATH ("resultPath",2),
		HDFS_URI ("hdfsUri",3),
		HDFS_REPORTPATH ("hdfsReportPath",4),
		COMMAND ("command",5);
		
		private final String name;
		private final int position;
		ARGS_POSITION(String name, int position){
			this.name = name;
			this.position = position;
		}
		public int getPosition(){
			return this.position;
		}
		
		public String toString(){
			return this.name+"-> args["+this.position+"]";
		}
	}
	
	
	public static ArgumentsElement convertArgsToArgumentsElement(String[] args){
		ArgumentsOperations converter = new ArgumentsOperations(args);
		if(converter.convert()){
			System.err.println("Not enough Arguments!");
			for(ARGS_POSITION argPos : ARGS_POSITION.values()){
				System.err.println(argPos.toString());
			}
			return new ArgumentsElement();
		}
		return converter.getArguments();
	}
	
	public ArgumentsOperations(String[] args) {
		this.args = args;
	}
	
	/**
	 * @return true if occur error
	 */
	public boolean convert(){
		this.result = new ArgumentsElement();
		if(!isValidNumberOfArgs()){
			return true;
		}
		this.result.setRefPath(extractRefPath());
		this.result.setReportPath(extractReportPath());
		this.result.setResultPath(extractResultPath());
		this.result.setCommand(extractCommand());
		this.result.setHdfsUri(extractHdfsUri());
		this.result.setHdfsReportPath(extractHdfsReportPath());
		return !this.result.isValid();
	}
	
	public ArgumentsElement getArguments(){
		return this.result;
	}
	
	private boolean isValidNumberOfArgs(){
		if(this.args.length < ArgumentsElement.MIN_ARGS_NUMBER){
			return false;
		}
		return true;
	}
	
	private String extractRefPath(){
		return this.args[ARGS_POSITION.REFPATH.getPosition()];
	}
	
	private String extractResultPath(){
		return this.args[ARGS_POSITION.RESULTPATH.getPosition()];
	}
	
	private String extractReportPath(){
		return this.args[ARGS_POSITION.REPORTPATH.getPosition()];
	}
	
	private String extractHdfsUri(){
		return this.args[ARGS_POSITION.HDFS_URI.getPosition()];
	}
	
	private String extractHdfsReportPath(){
		return this.args[ARGS_POSITION.HDFS_REPORTPATH.getPosition()];
	}
	
	private String extractCommand(){
		String command = "";
		for(int argPos = ARGS_POSITION.COMMAND.getPosition(); argPos < this.args.length; argPos++){
			command += this.args[argPos]+" ";
		}
		if(command.endsWith(" ")){
			command = command.substring(0, command.length()-1);
		}
		return command;
	}
	
}
