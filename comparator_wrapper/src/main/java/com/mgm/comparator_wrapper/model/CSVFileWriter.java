package com.mgm.comparator_wrapper.model;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.base.Joiner;

/**
 * 
 * @author bugge
 * @since 28.02.2016
 */
public class CSVFileWriter {

	private final static Logger LOGGER = Logger.getLogger(CSVFileWriter.class.getName());
	
	public static final String ERROR_PATTERN = "Not Equal:";
	public static final String SUCCESSFUL_PATTERN = "Equal:";
	public static final String CSV_SEPERATOR = ",";
	public static final String NOT_FOUND = "not found!";
	
	private final String filePath;

	public static boolean writeReport(ArgumentsElement args,List<String> lines){
		LOGGER.info("write csv-> "+args.getReportPath());
		CSVFileWriter csvWriter = new CSVFileWriter(args.getReportPath());
		return csvWriter.write(csvWriter.buildRow(lines, args), CSV_SEPERATOR);
	}
	
	public CSVFileWriter(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * 
	 * @param seperator
	 * @param columns
	 * @return true if occur error
	 */
	public boolean write(List<String> columns,String seperator) {
		String line = Joiner.on(seperator).join(columns);
		List<String> lines = new ArrayList<String>();
		lines.add(line);
		Path file = Paths.get(filePath);
		LOGGER.info("write out: "+line);
		creatFolder(filePath);
		try {
			if (!Files.exists(file)){
			    Files.createFile(file);
			}
			Files.write(file, lines, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
			return true;
		}
		return false;
	}
	
	public List<String> buildRow(List<String> lines,ArgumentsElement args){
		List<String> row = new ArrayList<String>();
		row.add(extractSuccessful(lines));
		row.add(extractError(lines));
		row.add(args.getRefPath());
		row.add(args.getResultPath());
		return row;
	}
	
	private String extractError(List<String> lines){
		for(String line: lines){
			if(line.contains(ERROR_PATTERN)){
				return line;
			}
		}
		
		return ERROR_PATTERN+NOT_FOUND;
	}
	
	private String extractSuccessful(List<String> lines){
		for(String line: lines){
			if(line.contains(SUCCESSFUL_PATTERN)){
				return line;
			}
		}
		
		return SUCCESSFUL_PATTERN+NOT_FOUND;
	}
	
	private void creatFolder(String filePath){
		if(!existFileOrFolder(getFolderPathFromFile(filePath))){
			File resultFolder = new File(getFolderPathFromFile(filePath));
			resultFolder.mkdirs();
		}
	}
	
	private boolean existFileOrFolder(String folderPath){
		File folder = new File(folderPath);
		return folder.exists();
	}
	
	private String getFolderPathFromFile(String filePath){
		File file = new File(filePath);
		return new File(file.getParent()).getAbsolutePath();
	}
}
