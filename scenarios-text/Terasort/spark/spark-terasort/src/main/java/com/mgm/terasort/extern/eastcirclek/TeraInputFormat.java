/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mgm.terasort.extern.eastcirclek;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.TaskAttemptID;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.task.TaskAttemptContextImpl;
import org.apache.hadoop.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mgm.spark_terasort.KeyCompressor;
import com.mgm.spark_terasort.extern.eastcirclek.tera_input_format.SamplerThreadGroup;
import com.mgm.spark_terasort.extern.eastcirclek.tera_input_format.TeraLineRecordReader;
import com.mgm.spark_terasort.extern.eastcirclek.tera_input_format.TextSampler;

/**
 * An input format that reads the first 10 characters of each line as the key and the rest of the line as the value. Both key and value are
 * represented as Text.
 */
public class TeraInputFormat extends FileInputFormat<Text, Text> {
	
	Logger LOG = LoggerFactory.getLogger(TeraInputFormat.class);

	public static final String PARTITION_FILENAME = "_partition.lst";
	private static final String NUM_PARTITIONS_CONFIG_KEY = "mapreduce.terasort.num.partitions";
	private static final String SAMPLE_SIZE_CONFIG_KEY = "mapreduce.terasort.partitions.sample";
//	private static MRJobConfig lastContext = null;
//	private static List<InputSplit> lastResult = null;

	/**
	 * Use the input splits to take samples of the input and generate sample keys. By default reads 100,000 keys from 10 locations in the input, sorts
	 * them and picks N-1 keys to generate N equally sized partitions.
	 * 
	 * @param job
	 *            the job to sample
	 * @param partFile
	 *            where to write the output file to
	 * @throws Throwable
	 *             if something goes wrong
	 */
	public static void writePartitionFile(final JobContext job, Path partFile) throws Throwable {
		long t1 = System.currentTimeMillis();
		Configuration conf = job.getConfiguration();
		
		// call his self (can run now own methods)
		final TeraInputFormat inFormat = new TeraInputFormat();
		// contains all records (keys) with operations
		final TextSampler sampler = new TextSampler();
		// set in main as args
		int partitions = job.getNumReduceTasks();

		// default 100000 (max key number) !!!!!!
		long sampleSize = conf.getLong(SAMPLE_SIZE_CONFIG_KEY, 100000); 
		//long sampleSize = conf.getLong(SAMPLE_SIZE_CONFIG_KEY, Long.MAX_VALUE);//Maximal number of rows that reads from hdfs (braucht man so viele?
		
		final List<InputSplit> splits = inFormat.getSplits(job);

		long t2 = System.currentTimeMillis();
		System.out.println("Computing input splits took " + (t2 - t1) + "ms");
		int samples = Math.min(conf.getInt(NUM_PARTITIONS_CONFIG_KEY, 10), splits.size());
		System.out.println("Sampling " + samples + " splits of " + splits.size());
		final long recordsPerSample = sampleSize / samples; // how many lines read ohne thread 
		final int sampleStep = splits.size() / samples;
		Thread[] samplerReader = new Thread[samples];
		
		SamplerThreadGroup threadGroup = new SamplerThreadGroup("Sampler Reader Thread Group");
		// take N samples from different parts of the input
		for (int i = 0; i < samples; ++i) {
			final int idx = i;
			samplerReader[i] = new Thread(threadGroup, "Sampler Reader " + idx) {
				{
					setDaemon(true);
				}

				public void run() {
					inFormat.samplerReaderThreadRunner(job, inFormat, sampler, splits, recordsPerSample, sampleStep, idx);
				}
			};
			samplerReader[i].start();
		}
		//Output File System
		FileSystem outFs = partFile.getFileSystem(conf);
		// Path, overwrite, buffer size, replication, block size 
		DataOutputStream writer = outFs.create(partFile, true, 64 * 1024, (short) 10, outFs.getDefaultBlockSize(partFile));
		for (int i = 0; i < samples; i++) { // wait until threads terminated 
			try {
				samplerReader[i].join();
				if (threadGroup.getThrowable() != null) {
					throw threadGroup.getThrowable();
				}
			} catch (InterruptedException e) {
			}
		}
		//create partitions and write out to the HDFS
		for (Text split : sampler.createPartitions(partitions)) {
			split.write(writer);
		}
		writer.close();
		long t3 = System.currentTimeMillis();
		System.out.println("Computing parititions took " + (t3 - t2) + "ms");
	}

	/**
	 * read a part of the input file and save the keys in sampler
	 * @param job
	 * @param inFormat
	 * @param sampler
	 * @param splits
	 * @param recordsPerSample
	 * @param sampleStep
	 * @param idx
	 */
	private void samplerReaderThreadRunner(final JobContext job, final TeraInputFormat inFormat, final TextSampler sampler,
			final List<InputSplit> splits, final long recordsPerSample, final int sampleStep, final int idx) {
		long records = 0;
		try {
			// in Thread read File
			TaskAttemptContext context = new TaskAttemptContextImpl(job.getConfiguration(), new TaskAttemptID());
			RecordReader<Text, Text> reader = inFormat.createRecordReader(splits.get(sampleStep * idx), context);
			reader.initialize(splits.get(sampleStep * idx), context);
			Text lastKey = null;
			while (reader.nextKeyValue()) {
				Text currentKey = new Text(reader.getCurrentKey());
				if(KeyCompressor.sameDocumentType(lastKey, currentKey)){
					continue;
				}
				lastKey = currentKey; 
				
				sampler.addKey(currentKey);
				records += 1;
				if (recordsPerSample <= records) {
					break;
				}
			}
		} catch (IOException ie) {
			System.err.println("Got an exception while reading splits " + StringUtils.stringifyException(ie));
			throw new RuntimeException(ie);
		} catch (InterruptedException e) {

		}
	}

	@Override
	public RecordReader<Text, Text> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException {
		return new TeraLineRecordReader();
	}
}
