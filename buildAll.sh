#!/bin/bash

echo "####################################"
echo "############# Tools ################"
echo "####################################"
echo ""
echo "#### configuration builder ####"
echo ""
mvn -f tools/config_builder/pom.xml clean package
echo ""
echo "#### evaluation builder ####"
echo ""
mvn -f evaluator/benchmark_evaluator/pom.xml clean package

echo "####################################"
echo "############# Generator ############"
echo "####################################"
echo ""
echo "#### Build Flink wiki extractor ####"
echo ""
mvn -f generators/flink/flink_wiki_text_extractor/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Flink ldbc graph to gradoop graph ####"
echo ""
mvn -f generators/flink/flink_ldbcgen_to_gradoop/pom.xml clean package -Pbuild-jar

echo "####################################"
echo "############# Alluxio ##############"
echo "####################################"
echo ""
echo "#### Alluxio Lineage API Wrapper ####"
echo "execute java -jar target/alluxio_lineage_api-0.0.1-SNAPSHOT-jar-with-dependencies.jar test test2"
echo ""
mvn -f alluxio_lineage_api/pom.xml clean install

echo "####################################"
echo "############ Evaluation ############"
echo "####################################"

echo ""
echo "#### Build Evaluator Wrapper ####"
echo ""
mvn -f comparator_wrapper/pom.xml clean package

echo ""
echo "#### Build Flink unsorted evaluator ####"
echo ""
mvn -f scenarios-text/Evaluation/unsorted/flink/flink-unsorted-evaluation/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Flink sorted evaluator ####"
echo ""
mvn -f scenarios-text/Evaluation/sorted/flink/flink-sorted-evaluation/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Spark unsorted evaluator ####"
echo ""
mvn -f scenarios-text/Evaluation/unsorted/spark/spark-unsorted-evaluation/pom.xml clean package

echo ""
echo "#### Build Spark sorted evaluator ####"
echo ""
mvn -f scenarios-text/Evaluation/sorted/spark/spark-sorted-evaluation/pom.xml clean package

echo "####################################"
echo "###### Build Flink Test cases ######"
echo "####################################"
echo ""
echo "#### Build Flink Copy ####"
echo ""
mvn -f scenarios-text/Copy/flink/flink_copy/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Flink Terasort ####"
echo ""
mvn -f scenarios-text/Terasort/flink/flink_terasort/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Flink TF-IDF ####"
echo ""
mvn -f scenarios-text/tf-idf/flink/flink-tfidf/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Flink Word Counter ####"
echo ""
mvn -f scenarios-text/WordCount/flink/flink-wordcount/pom.xml clean package -Pbuild-jar

echo ""
echo "#### Build Flink Gradoop Grouping ####"
echo ""
mvn -f scenarios-graph/flink/flink_gradoop_grouping/pom.xml clean package -Pbuild-jar

echo "####################################"
echo "###### Build Spark Test cases ######"
echo "####################################"
echo ""
echo "#### Build Spark Word Counter ####"
echo ""
mvn -f scenarios-text/WordCount/spark/spark-wordcount/pom.xml clean package

echo ""
echo "#### Build Spark Copy ####"
echo ""
mvn -f scenarios-text/Copy/spark/spark-cp/pom.xml clean package

echo ""
echo "#### Build Spark TF-IDF ####"
echo ""
mvn -f scenarios-text/tf-idf/spark/spark-tfidf/pom.xml clean package

echo ""
echo "#### Build Spark Terasort ####"
echo ""
mvn -f scenarios-text/Terasort/spark/spark-terasort/pom.xml clean package
