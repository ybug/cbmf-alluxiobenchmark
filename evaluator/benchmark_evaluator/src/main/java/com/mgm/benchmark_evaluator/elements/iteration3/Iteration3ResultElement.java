package com.mgm.benchmark_evaluator.elements.iteration3;

import java.util.HashMap;
import java.util.Map;

public class Iteration3ResultElement implements Comparable<Iteration3ResultElement>{

	private long scaleFactor;
	//technology Name
	private Map<String, Iteration3ResultAtomElement> elements = new HashMap<String, Iteration3ResultAtomElement>();
	
	public long getScaleFactor() {
		return scaleFactor;
	}
	public String getScaleFactorAsString() {
		return Long.toString(scaleFactor);
	}
	public void setScaleFactor(long scaleFactor) {
		this.scaleFactor = scaleFactor;
	}
	
	public Map<String, Iteration3ResultAtomElement> getElements() {
		return elements;
	}
	public void setElements(Map<String, Iteration3ResultAtomElement> elements) {
		this.elements = elements;
	}
	public void addElement(String technology, Iteration3ResultAtomElement element){
		this.elements.put(technology, element);
	}
	@Override
    public int compareTo(Iteration3ResultElement another) {
        if (this.getScaleFactor()<another.getScaleFactor()){
            return -1;
        }else{
            return 1;
        }
    }
	@Override
	public String toString() {
		return "Iteration3ResultElement [scaleFactor=" + scaleFactor + ", elements=" + elements + "]";
	}
	
	
}
