package com.mgm.config_builder.model;

public class StringTemplates {
	
	public static final String TEMPLATE_FOLDER_ARGUMENT_KEY_LONG = "input";
	public static final String TEMPLATE_FOLDER_ARGUMENT_KEY_SHORT = "i";
	public static final String OUTPUT_FOLDER_ARGUMENT_KEY_LONG = "output";
	public static final String OUTPUT_FOLDER_ARGUMENT_KEY_SHORT = "o";
	
	public static final String ABSOLUT_PATH_KEY="absolutPath";
	public static final String COPIE_NUMBER_KEY = "copy_number";
	public static final String REPLACE_COPIE_NUMBER_KEY = "replace_copy_number";

}
