package com.mgm.terasort.extern.eastcirclek.total_order_partitioner;

import java.io.DataInputStream;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.MRJobConfig;

import com.mgm.terasort.extern.eastcirclek.total_order_partitioner.InnerTrieNode;
import com.mgm.terasort.extern.eastcirclek.total_order_partitioner.TrieNode;
import com.mgm.terasort.extern.eastcirclek.total_order_partitioner.LeafTrieNode;

public class TrieBuilder {

	/**
     * Given a sorted set of cut points, build a trie that will find the correct
     * partition quickly.
     * @param splits the list of cut points
     * @param lower the lower bound of partitions 0..numPartitions-1
     * @param upper the upper bound of partitions 0..numPartitions-1
     * @param prefix the prefix that we have already checked against
     * @param maxDepth the maximum depth we will build a trie for
     * @return the trie node that will divide the splits correctly
     */
    public TrieNode buildTrie(Text[] splits, int lower, int upper, Text prefix, int maxDepth) {
        int depth = prefix.getLength();
        if (depth >= maxDepth || lower == upper) {
            return new LeafTrieNode(depth, splits, lower, upper);
        }
        InnerTrieNode result = new InnerTrieNode(depth);
        Text trial = new Text(prefix);
        // append an extra byte on to the prefix
        trial.append(new byte[1], 0, 1);
        int currentBound = lower;
        for(int ch = 0; ch < 255; ++ch) {
            trial.getBytes()[depth] = (byte) (ch + 1);
            lower = currentBound;
            while (currentBound < upper) {
                if (splits[currentBound].compareTo(trial) >= 0) { // compare  1. byte
                    break;
                }
                currentBound += 1;
            }
			trial.getBytes()[depth] = (byte) ch;
            result.getChildren()[ch] = buildTrie(splits, lower, currentBound, trial, 
                                         maxDepth);
        }
        // pick up the rest
        trial.getBytes()[depth] = (byte) 255;
        result.getChildren()[255] = buildTrie(splits, currentBound, upper, trial,
                                      maxDepth);
        return result;
    }
    
    /**
     * Read the cut points from the given sequence file.
     * @param fs the file system
     * @param p the path to read
     * @param job the job config
     * @return the strings to split the partitions on
     * @throws IOException
     */
    public Text[] readPartitions(FileSystem fs, Path p, Configuration conf) throws IOException {
        int reduces = conf.getInt(MRJobConfig.NUM_REDUCES, 1);
        Text[] result = new Text[reduces - 1];
        DataInputStream reader = fs.open(p);
        for(int i=0; i < reduces - 1; ++i) {
            result[i] = new Text();
            result[i].readFields(reader);
        }
        reader.close();
        return result;
    }

    

    // eastcirclek
    public TrieNode buildTrieFromHDFS(Configuration conf, Path hdfsPath) throws IOException{
        FileSystem fs = hdfsPath.getFileSystem(conf);
        Text[] splitPoints = readPartitions(fs, hdfsPath, conf);
        
        return buildTrie(splitPoints, 0, splitPoints.length, new Text(), 2);
    }
}
