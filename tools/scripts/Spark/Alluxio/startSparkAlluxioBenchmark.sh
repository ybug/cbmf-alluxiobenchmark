#!/bin/bash

echo "#"
echo "### Spark Alluxio Only Wordcount"
echo "#"

sh Spark/Alluxio/Only/startSparkAlluxioWordcountBenchmark.sh _part1
sh Spark/Alluxio/Only/startSparkAlluxioWordcountBenchmark.sh _part2
sh Spark/Alluxio/Only/startSparkAlluxioWordcountBenchmark.sh _part3
sh Spark/Alluxio/Only/startSparkAlluxioWordcountBenchmark.sh _part4
sh Spark/Alluxio/Only/startSparkAlluxioWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark Alluxio Only Copy"
echo "#"

sh Spark/Alluxio/Only/startSparkAlluxioCopyBenchmark.sh _part1
sh Spark/Alluxio/Only/startSparkAlluxioCopyBenchmark.sh _part2
sh Spark/Alluxio/Only/startSparkAlluxioCopyBenchmark.sh _part3
sh Spark/Alluxio/Only/startSparkAlluxioCopyBenchmark.sh _part4
sh Spark/Alluxio/Only/startSparkAlluxioCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark Alluxio Only Terasort"
echo "#"

sh Spark/Alluxio/Only/startSparkAlluxioTeraBenchmark.sh _part1
sh Spark/Alluxio/Only/startSparkAlluxioTeraBenchmark.sh _part2
sh Spark/Alluxio/Only/startSparkAlluxioTeraBenchmark.sh _part3
sh Spark/Alluxio/Only/startSparkAlluxioTeraBenchmark.sh _part4
sh Spark/Alluxio/Only/startSparkAlluxioTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Spark Alluxio Only TF-IDF"
echo "#"

sh Spark/Alluxio/Only/startSparkAlluxioTfidfBenchmark.sh _part1
sh Spark/Alluxio/Only/startSparkAlluxioTfidfBenchmark.sh _part2
sh Spark/Alluxio/Only/startSparkAlluxioTfidfBenchmark.sh _part3
#sh Spark/Alluxio/Only/startSparkAlluxioTfidfBenchmark.sh _part4
#sh Spark/Alluxio/Only/startSparkAlluxioTfidfBenchmark.sh _part5
