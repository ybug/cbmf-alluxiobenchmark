package com.mgm.spark_sorted_evaluation;

import com.github.jankroken.commandline.annotations.LongSwitch;
import com.github.jankroken.commandline.annotations.Option;
import com.github.jankroken.commandline.annotations.Required;
import com.github.jankroken.commandline.annotations.ShortSwitch;
import com.github.jankroken.commandline.annotations.SingleArgument;

public class Arguments {
	private String inputRefPath = null;
	private String inputCheckPath = null;
	private String outputPath = null;
	private String masterHost = "local";

	public String getInputRefPath() {
		return inputRefPath;
	}

	@Option
	@LongSwitch("inputRef")
	@ShortSwitch("ir")
	@SingleArgument
	@Required
	public void setInputRefPath(String inputPath) {
		this.inputRefPath = inputPath;
	}

	public String getOutputPath() {
		return outputPath;
	}

	@Option
	@LongSwitch("output")
	@ShortSwitch("o")
	@SingleArgument
	@Required
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
	}

	public String getMasterHost() {
		return masterHost;
	}

	@Option
	@LongSwitch("master")
	@ShortSwitch("m")
	@SingleArgument
	public void setMasterHost(String masterHost) {
		this.masterHost = masterHost;
	}

	public String getInputCheckPath() {
		return inputCheckPath;
	}
	
	@Option
	@LongSwitch("inputCheck")
	@ShortSwitch("ic")
	@SingleArgument
	@Required
	public void setInputCheckPath(String inputCheckPath) {
		this.inputCheckPath = inputCheckPath;
	}
}
