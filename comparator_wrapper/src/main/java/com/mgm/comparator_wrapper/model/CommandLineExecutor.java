package com.mgm.comparator_wrapper.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

public class CommandLineExecutor {

	private final static Logger LOGGER = Logger.getLogger(CommandLineExecutor.class.getName()); 
	
	public final String command;

	public CommandLineExecutor( String command) {
		this.command = command;
	}

	public boolean run() {
		Process process;
		boolean executeInCommandError = false;
		int exitValue = 0;

		try {
			LOGGER.info("Execute -> "+command);
			process = Runtime.getRuntime().exec(command);
			logOutputFromCommandInputStream(process);
			logOutputFromCommandErrorStream(process);
			process.waitFor();
			
			//executeInCommandError = logOutputFromCommandErrorStream(process);
			
			if (process.exitValue() != 0) {
				exitValue = process.exitValue();
				executeInCommandError = true;
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			return true;
		}
		if (executeInCommandError) {
			LOGGER.error("Command line error! exitValue: "+exitValue);
			return true;
		}
		return executeInCommandError;
	}

	/**
	 * write in the log the output from the execute command (only input stream)
	 * 
	 * @param process
	 *            -> is the process from the execute command
	 * @throws IOException
	 */
	private void logOutputFromCommandInputStream(Process process) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = "";
		while ((line = reader.readLine()) != null) {
			LOGGER.info(line);
		}
	}

	/**
	 * write in the log the output from the execute command (only error stream)
	 * 
	 * @param process
	 *            -> is the process from the execute command
	 * @return if true, then occur error
	 * @throws IOException
	 */
	private boolean logOutputFromCommandErrorStream(Process process) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		String line = "";
		boolean error = false;
		while ((line = reader.readLine()) != null) {
			LOGGER.error(line);
			error = true;
		}
		return error;
	}
}
