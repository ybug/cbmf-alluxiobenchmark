#!/bin/bash

echo "#"
echo "### Flink Alluxio Through Wordcount"
echo "#"

sh Flink/Alluxio/Through/startFlinkAlluxioThroughWordcountBenchmark.sh _part1
sh Flink/Alluxio/Through/startFlinkAlluxioThroughWordcountBenchmark.sh _part2
sh Flink/Alluxio/Through/startFlinkAlluxioThroughWordcountBenchmark.sh _part3
sh Flink/Alluxio/Through/startFlinkAlluxioThroughWordcountBenchmark.sh _part4
sh Flink/Alluxio/Through/startFlinkAlluxioThroughWordcountBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Through Copy"
echo "#"

sh Flink/Alluxio/Through/startFlinkAlluxioThroughCopyBenchmark.sh _part1
sh Flink/Alluxio/Through/startFlinkAlluxioThroughCopyBenchmark.sh _part2
sh Flink/Alluxio/Through/startFlinkAlluxioThroughCopyBenchmark.sh _part3
sh Flink/Alluxio/Through/startFlinkAlluxioThroughCopyBenchmark.sh _part4
sh Flink/Alluxio/Through/startFlinkAlluxioThroughCopyBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Through Terasort"
echo "#"

sh Flink/Alluxio/Through/startFlinkAlluxioThroughTeraBenchmark.sh _part1
sh Flink/Alluxio/Through/startFlinkAlluxioThroughTeraBenchmark.sh _part2
sh Flink/Alluxio/Through/startFlinkAlluxioThroughTeraBenchmark.sh _part3
sh Flink/Alluxio/Through/startFlinkAlluxioThroughTeraBenchmark.sh _part4
sh Flink/Alluxio/Through/startFlinkAlluxioThroughTeraBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Through TF-IDF"
echo "#"

sh Flink/Alluxio/Through/startFlinkAlluxioThroughTfidfBenchmark.sh _part1
sh Flink/Alluxio/Through/startFlinkAlluxioThroughTfidfBenchmark.sh _part2
sh Flink/Alluxio/Through/startFlinkAlluxioThroughTfidfBenchmark.sh _part3
#sh Flink/Alluxio/Through/startFlinkAlluxioThroughTfidfBenchmark.sh _part4
#sh Flink/Alluxio/Through/startFlinkAlluxioThroughTfidfBenchmark.sh _part5

echo "press any key to continue!"
read
echo "#"
echo "### Flink Alluxio Through Grouping"
echo "#"
sh Flink/Alluxio/Through/startFlinkAlluxioThroughGroupingBenchmark.sh _part1
sh Flink/Alluxio/Through/startFlinkAlluxioThroughGroupingBenchmark.sh _part2
sh Flink/Alluxio/Through/startFlinkAlluxioThroughGroupingBenchmark.sh _part3
sh Flink/Alluxio/Through/startFlinkAlluxioThroughGroupingBenchmark.sh _part4
sh Flink/Alluxio/Through/startFlinkAlluxioThroughGroupingBenchmark.sh _part5