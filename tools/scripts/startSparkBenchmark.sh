#!/bin/bash

echo ""
echo ""
echo "#####################"
echo "### Spark HDFS ######"
echo "#####################"
echo ""
echo "setup and enable HDFS"
echo "press any key to START BENCHMARK!"
read

sh Spark/HDFS/startSparkHDFSBenchmark.sh

echo ""
echo ""
echo "#############################"
echo "### Spark Alluxio Through ###"
echo "#############################"
echo ""
echo "setup and enable Alluxio! disable HDFS"
echo "press any key to START BENCHMARK!"
read

sh Spark/Alluxio/startSparkAlluxioThroughBenchmark.sh

echo ""
echo ""
echo "################################"
echo "### Spark Alluxio Must Cache ###"
echo "################################"
echo ""
echo "setup and restart Alluxio!"
echo "press any key to START BENCHMARK!"
read
sh Spark/Alluxio/startSparkAlluxioMustCacheBenchmark.sh

echo ""
echo ""
echo "##########################################"
echo "### Spark Alluxio Through RDD Off Heap ###"
echo "##########################################"
echo ""
echo "setup and enable Alluxio! disable HDFS"
echo "press any key to START BENCHMARK!"
read

sh Spark/Alluxio/startSparkAlluxioThroughRDDOffHeapBenchmark.sh

echo ""
echo ""
echo "################################"
echo "### Spark Alluxio Must Cache ###"
echo "################################"
echo ""
echo "setup and restart Alluxio!"
echo "press any key to START BENCHMARK!"
read
sh Spark/Alluxio/startSparkAlluxioMustCacheRDDOffHeapBenchmark.sh

echo ""
echo ""
echo "################################"
echo "### Spark Alluxio Only ###"
echo "################################"
echo ""
echo "setup and restart Alluxio!, disbale HDFS"
echo "press any key to START BENCHMARK!"
read
sh Spark/Alluxio/startSparkAlluxioBenchmark.sh